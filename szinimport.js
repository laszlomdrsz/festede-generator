const xlsx = require('xlsx');
const fs = require('fs');

let workbook;
let importRows;
try {
	workbook = xlsx.readFile('szinek.xlsx');
	importRows = xlsx.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
} catch (error) {
	console.log('szinek.xlsx hiányzik');
	importRows = [];
}

const colors = [];
const colorAliases = {};

for (const importRow of importRows) {
	const rgb = [
		importRow.r,
		importRow.g,
		importRow.b
	];
	colors.push(rgb);
	const aliasName = importRow.n;
	if (aliasName || aliasName === 0) {
		colorAliases[aliasName] = rgb;
	}
}


const colorsData = JSON.stringify(colors, null, 2);
fs.writeFileSync('src/szinek.json', colorsData);

const colorAliasesData = JSON.stringify(colorAliases, null, 2);
fs.writeFileSync('src/szinnevek.json', colorAliasesData);