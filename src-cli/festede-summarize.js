const xlsx = require('xlsx');
const fs = require('fs');

const colorExcelOutputDir = './excel-summaries';
const imageExcelInputDir = './image-excel';

const clearOutput = () => {
  const outputFilenames = fs.readdirSync(colorExcelOutputDir).filter(filename => filename !== '.gitkeep');
  try {
    for (const outputFilename of outputFilenames) {
      fs.unlinkSync(`${colorExcelOutputDir}/${outputFilename}`);
    }
  } catch (error) {
    console.log('Hiba történt a fájlok törlése során. Töröld ki a fájlokat az excel-summaries mappából!');
    process.exit(0);
  }
}

/**
   * Átalakít egy tömböt XLSX bufferré
   * @param array Tömb
   * @param wsName Munkalap neve
   * @returns Buffer
   */
 const arrayToExcel = (array, wsName) => {
  const xlsxDataSheet = xlsx.utils.json_to_sheet(array);
  const workBook = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(workBook, xlsxDataSheet, wsName);
  const buffer = xlsx.write(workBook, {type: 'buffer'});
  return buffer;
};

const run = async() => {
  clearOutput();
  const inputImageExcelFilenames = fs.readdirSync(imageExcelInputDir).filter(filename => filename !== '.gitkeep' && filename.endsWith('.xlsx'));
  const colorDataMap = new Map(); // color, {imageCount, areaPercentage}
  
  let totalImageCount = 0;
  const imageExcelSumRows = [];
  for (const filename of inputImageExcelFilenames) {
    const filePath = `${imageExcelInputDir}/${filename}`;
    const workbook = xlsx.readFile(filePath);
    const imageRows = xlsx.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
    imageExcelSumRows.push(...imageRows);
    for (const imageRow of imageRows) {
      totalImageCount++;
      let colorCounter = 1;
      while (imageRow[`Szín ${colorCounter}`]) {
        const color = imageRow[`Szín ${colorCounter}`];
        const areaPercentage = parseFloat(imageRow[`Százalék ${colorCounter}`]);
        if (colorDataMap.has(color)) {
          const oldImageCount = colorDataMap.get(color).imageCount;
          const oldAreaPercentage = colorDataMap.get(color).areaPercentage;
          colorDataMap.set(color, {imageCount: oldImageCount + 1, areaPercentage: areaPercentage + oldAreaPercentage});
        } else {
          colorDataMap.set(color, {imageCount: 1, areaPercentage})
        }
        colorCounter++;
      }
    }
  }
  
  let colorExcelRows = [];
  for (const [colorName, usageData] of colorDataMap) {
    const row = {
      'Szín': colorName,
      'Képek száma': usageData.imageCount,
      'Százalék': Math.round(usageData.areaPercentage / totalImageCount * 100) / 100
    }
    colorExcelRows.push(row);
  }
  colorExcelRows = colorExcelRows.sort((a, b) => b['Százalék'] - a['Százalék']);


  const colorExcelBuffer = arrayToExcel(colorExcelRows, 'Színek');
  fs.writeFileSync(`${colorExcelOutputDir}/szín-összesítő.xlsx`, colorExcelBuffer);
  const imageExcelBuffer = arrayToExcel(imageExcelSumRows, 'Képek');
  fs.writeFileSync(`${colorExcelOutputDir}/kép-összesítő.xlsx`, imageExcelBuffer)
}

(async () => {
  await run();
  console.log('\nFolyamat kész! Bezárhatod ezt az ablakot');
})()