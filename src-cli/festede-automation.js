const { main } = require("./main");
const fs = require('fs');
const xlsx = require('xlsx');
const moment = require('moment');

// Ezek akkor működnek, ha egy szinttel kijjebbről indítjuk
const inputDir = './input';
const outputDir = './output';
const imageExcelOutputDir = './image-excel'

const clearOutput = () => {
  const outputFilenames = fs.readdirSync(outputDir).filter(filename => filename !== '.gitkeep');
  try {
    for (const outputFilename of outputFilenames) {
      fs.unlinkSync(`${outputDir}/${outputFilename}`);
    }
  } catch (error) {
    console.log('Hiba történt a fájlok törlése során. Töröld ki a fájlokat az output mappából!');
    process.exit(0);
  }
}

/**
   * Átalakít egy tömböt XLSX bufferré
   * @param array Tömb
   * @param wsName Munkalap neve
   * @returns Buffer
   */
const arrayToExcel = (array, wsName) => {
  const xlsxDataSheet = xlsx.utils.json_to_sheet(array);
  const workBook = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(workBook, xlsxDataSheet, wsName);
  const buffer = xlsx.write(workBook, {type: 'buffer'});
  return buffer;
};



const filenameWithoutExtension = filename => {
  const filenameArray = filename.split('.');
  filenameArray.pop();
  return filenameArray.join('.');
};

const setArgv = (filename) => {
  process.argv[2] = '-i';
  process.argv[3] = `${inputDir}/${filename}`;
  process.argv[4] = '-o';
  process.argv[5] = `${outputDir}/${filenameWithoutExtension(filename)}`;
}

const run = async() => {
  clearOutput();
  const inputFilenames = fs.readdirSync(inputDir).filter(filename => filename !== '.gitkeep');

  for (const inputFilename of inputFilenames) {
    setArgv(inputFilename);
    await main();
    try {
      console.log("Finished: " + inputFilename);
    } catch (error) {
      console.error("Error: " + err.name + " " + err.message + " " + err.stack);
    }
  }
  
  const outputJsonFilenames = fs.readdirSync(outputDir).filter(filename => filename !== '.gitkeep' && filename.endsWith('.json'));
  const outputDataMap = new Map();
  
  for (const filename of outputJsonFilenames) {
    const colorData = JSON.parse(fs.readFileSync(`${outputDir}/${filename}`, 'utf-8'));
    outputDataMap.set(filenameWithoutExtension(filename), colorData);
  }
  
  const imageExcelRows = [];
  for (const [filename, colorData] of outputDataMap) {
    const row = {
      'Név': filename,
    };
    let colorCounter = 0;
    for (const color of colorData) {
      if (!color) {
        continue;
      }
      colorCounter++;
      row[`Szín ${colorCounter}`] = color.colorAlias || `${color.color[0]},${color.color[1]},${color.color[2]}`;
      row[`Százalék ${colorCounter}`] = Math.round(color.areaPercentage * 10000) / 100 + '%';
    }
    imageExcelRows.push(row);
  }
  const imageExcelBuffer = arrayToExcel(imageExcelRows, 'Képek');
  const date = moment().format('YYYY-MM-DD--HH-mm')
  fs.writeFileSync(`${imageExcelOutputDir}/kép-összesítő-${date}.xlsx`, imageExcelBuffer);
  
  /*
  const colorDataMap = new Map(); // KEY: array(3: r,g,b) VALUE: {imageCount, areaPercentage}
  for (const [filename, colorData] of outputDataMap) {
    for (const color of colorData) {
      if (!color) {
        continue;
      }
      const colorString = color.color.join(',');
      if (!colorDataMap.has(colorString)) {
        colorDataMap.set(colorString, {
          areaPercentage: color.areaPercentage, 
          imageCount: 1,
        })
      } else {
        const prevElem = colorDataMap.get(colorString);
        colorDataMap.set(colorString, {
          areaPercentage: prevElem.areaPercentage + color.areaPercentage, 
          imageCount: prevElem.imageCount + 1,
        })
      }
    }
  }
  const totalImageCount = outputDataMap.size;
  const colorExcelRows = [];
  for (const [colorName, usageData] of colorDataMap) {
    const colorArray = colorName.split(',');
    const row = {
      'Szín': `${colorArray[0]},${colorArray[1]},${colorArray[2]}`,
      'Képek száma': usageData.imageCount,
      'Százalék': Math.round(usageData.areaPercentage / totalImageCount * 10000) / 100 + '%'
    }
    colorExcelRows.push(row);
  }

  const colorExcelBuffer = arrayToExcel(colorExcelRows, 'Színek');
  fs.writeFileSync(`${outputDir}/szín-összesítő.xlsx`, colorExcelBuffer);
  */

}


(async () => {
  await run();
  console.log('\nFolyamat kész! Bezárhatod ezt az ablakot');
})()