var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
define("common", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.CancellationToken = exports.delay = void 0;
    function delay(ms) {
        return __awaiter(this, void 0, void 0, function* () {
            if (typeof window !== "undefined") {
                return new Promise((exec) => window.setTimeout(exec, ms));
            }
            else {
                return new Promise((exec) => exec());
            }
        });
    }
    exports.delay = delay;
    class CancellationToken {
        constructor() {
            this.isCancelled = false;
        }
    }
    exports.CancellationToken = CancellationToken;
});
define("random", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Random = void 0;
    class Random {
        constructor(seed) {
            if (typeof seed === "undefined") {
                this.seed = new Date().getTime();
            }
            else {
                this.seed = seed;
            }
        }
        next() {
            const x = Math.sin(this.seed++) * 10000;
            return x - Math.floor(x);
        }
    }
    exports.Random = Random;
});
define("lib/clustering", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.KMeans = exports.Vector = void 0;
    class Vector {
        constructor(values, weight = 1) {
            this.values = values;
            this.weight = weight;
        }
        distanceTo(p) {
            let sumSquares = 0;
            for (let i = 0; i < this.values.length; i++) {
                sumSquares += (p.values[i] - this.values[i]) * (p.values[i] - this.values[i]);
            }
            return Math.sqrt(sumSquares);
        }
        /**
         *  Calculates the weighted average of the given points
         */
        static average(pts) {
            if (pts.length === 0) {
                throw Error("Can't average 0 elements");
            }
            const dims = pts[0].values.length;
            const values = [];
            for (let i = 0; i < dims; i++) {
                values.push(0);
            }
            let weightSum = 0;
            for (const p of pts) {
                weightSum += p.weight;
                for (let i = 0; i < dims; i++) {
                    values[i] += p.weight * p.values[i];
                }
            }
            for (let i = 0; i < values.length; i++) {
                values[i] /= weightSum;
            }
            return new Vector(values);
        }
    }
    exports.Vector = Vector;
    class KMeans {
        constructor(points, k, random, centroids = null) {
            this.points = points;
            this.k = k;
            this.random = random;
            this.currentIteration = 0;
            this.pointsPerCategory = [];
            this.centroids = [];
            this.currentDeltaDistanceDifference = 0;
            if (centroids != null) {
                this.centroids = centroids;
                for (let i = 0; i < this.k; i++) {
                    this.pointsPerCategory.push([]);
                }
            }
            else {
                this.initCentroids();
            }
        }
        initCentroids() {
            for (let i = 0; i < this.k; i++) {
                this.centroids.push(this.points[Math.floor(this.points.length * this.random.next())]);
                this.pointsPerCategory.push([]);
            }
        }
        step() {
            // clear category
            for (let i = 0; i < this.k; i++) {
                this.pointsPerCategory[i] = [];
            }
            // calculate points per centroid
            for (const p of this.points) {
                let minDist = Number.MAX_VALUE;
                let centroidIndex = -1;
                for (let k = 0; k < this.k; k++) {
                    const dist = this.centroids[k].distanceTo(p);
                    if (dist < minDist) {
                        centroidIndex = k;
                        minDist = dist;
                    }
                }
                this.pointsPerCategory[centroidIndex].push(p);
            }
            let totalDistanceDiff = 0;
            // adjust centroids
            for (let k = 0; k < this.pointsPerCategory.length; k++) {
                const cat = this.pointsPerCategory[k];
                if (cat.length > 0) {
                    const avg = Vector.average(cat);
                    const dist = this.centroids[k].distanceTo(avg);
                    totalDistanceDiff += dist;
                    this.centroids[k] = avg;
                }
            }
            this.currentDeltaDistanceDifference = totalDistanceDiff;
            this.currentIteration++;
        }
    }
    exports.KMeans = KMeans;
});
// From https://stackoverflow.com/a/9493060/694640
define("lib/colorconversion", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.rgb2lab = exports.lab2rgb = exports.hslToRgb = exports.rgbToHsl = void 0;
    /**
      * Converts an RGB color value to HSL. Conversion formula
      * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
      * Assumes r, g, and b are contained in the set [0, 255] and
      * returns h, s, and l in the set [0, 1].
      *
      * @param   Number  r       The red color value
      * @param   Number  g       The green color value
      * @param   Number  b       The blue color value
      * @return  Array           The HSL representation
      */
    function rgbToHsl(r, g, b) {
        r /= 255, g /= 255, b /= 255;
        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);
        let h, s, l = (max + min) / 2;
        if (max === min) {
            h = s = 0; // achromatic
        }
        else {
            const d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch (max) {
                case r:
                    h = (g - b) / d + (g < b ? 6 : 0);
                    break;
                case g:
                    h = (b - r) / d + 2;
                    break;
                case b:
                    h = (r - g) / d + 4;
                    break;
                default: h = 0;
            }
            h /= 6;
        }
        return [h, s, l];
    }
    exports.rgbToHsl = rgbToHsl;
    /**
     * Converts an HSL color value to RGB. Conversion formula
     * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
     * Assumes h, s, and l are contained in the set [0, 1] and
     * returns r, g, and b in the set [0, 255].
     *
     * @param   Number  h       The hue
     * @param   Number  s       The saturation
     * @param   Number  l       The lightness
     * @return  Array           The RGB representation
     */
    function hslToRgb(h, s, l) {
        let r, g, b;
        if (s === 0) {
            r = g = b = l; // achromatic
        }
        else {
            const hue2rgb = (p, q, t) => {
                if (t < 0) {
                    t += 1;
                }
                if (t > 1) {
                    t -= 1;
                }
                if (t < 1 / 6) {
                    return p + (q - p) * 6 * t;
                }
                if (t < 1 / 2) {
                    return q;
                }
                if (t < 2 / 3) {
                    return p + (q - p) * (2 / 3 - t) * 6;
                }
                return p;
            };
            const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            const p = 2 * l - q;
            r = hue2rgb(p, q, h + 1 / 3);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1 / 3);
        }
        return [r * 255, g * 255, b * 255];
    }
    exports.hslToRgb = hslToRgb;
    // From https://github.com/antimatter15/rgb-lab/blob/master/color.js
    function lab2rgb(lab) {
        let y = (lab[0] + 16) / 116, x = lab[1] / 500 + y, z = y - lab[2] / 200, r, g, b;
        x = 0.95047 * ((x * x * x > 0.008856) ? x * x * x : (x - 16 / 116) / 7.787);
        y = 1.00000 * ((y * y * y > 0.008856) ? y * y * y : (y - 16 / 116) / 7.787);
        z = 1.08883 * ((z * z * z > 0.008856) ? z * z * z : (z - 16 / 116) / 7.787);
        r = x * 3.2406 + y * -1.5372 + z * -0.4986;
        g = x * -0.9689 + y * 1.8758 + z * 0.0415;
        b = x * 0.0557 + y * -0.2040 + z * 1.0570;
        r = (r > 0.0031308) ? (1.055 * Math.pow(r, 1 / 2.4) - 0.055) : 12.92 * r;
        g = (g > 0.0031308) ? (1.055 * Math.pow(g, 1 / 2.4) - 0.055) : 12.92 * g;
        b = (b > 0.0031308) ? (1.055 * Math.pow(b, 1 / 2.4) - 0.055) : 12.92 * b;
        return [Math.max(0, Math.min(1, r)) * 255,
            Math.max(0, Math.min(1, g)) * 255,
            Math.max(0, Math.min(1, b)) * 255];
    }
    exports.lab2rgb = lab2rgb;
    function rgb2lab(rgb) {
        let r = rgb[0] / 255, g = rgb[1] / 255, b = rgb[2] / 255, x, y, z;
        r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
        g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
        b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
        x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
        y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
        z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
        x = (x > 0.008856) ? Math.pow(x, 1 / 3) : (7.787 * x) + 16 / 116;
        y = (y > 0.008856) ? Math.pow(y, 1 / 3) : (7.787 * y) + 16 / 116;
        z = (z > 0.008856) ? Math.pow(z, 1 / 3) : (7.787 * z) + 16 / 116;
        return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)];
    }
    exports.rgb2lab = rgb2lab;
});
define("szinek", [], [
    [
        62,
        67,
        28
    ],
    [
        108,
        112,
        45
    ],
    [
        140,
        140,
        72
    ],
    [
        162,
        164,
        103
    ],
    [
        186,
        188,
        137
    ],
    [
        199,
        199,
        154
    ],
    [
        209,
        208,
        169
    ],
    [
        100,
        92,
        32
    ],
    [
        145,
        140,
        16
    ],
    [
        185,
        190,
        16
    ],
    [
        213,
        214,
        84
    ],
    [
        222,
        221,
        110
    ],
    [
        227,
        225,
        123
    ],
    [
        229,
        227,
        140
    ],
    [
        86,
        79,
        35
    ],
    [
        139,
        128,
        58
    ],
    [
        161,
        152,
        86
    ],
    [
        177,
        169,
        110
    ],
    [
        193,
        186,
        133
    ],
    [
        204,
        198,
        147
    ],
    [
        213,
        205,
        158
    ],
    [
        242,
        234,
        154
    ],
    [
        242,
        231,
        122
    ],
    [
        239,
        224,
        72
    ],
    [
        237,
        218,
        34
    ],
    [
        226,
        204,
        0
    ],
    [
        209,
        180,
        0
    ],
    [
        237,
        228,
        153
    ],
    [
        234,
        224,
        133
    ],
    [
        233,
        220,
        117
    ],
    [
        107,
        50,
        48
    ],
    [
        216,
        199,
        34
    ],
    [
        199,
        176,
        0
    ],
    [
        181,
        154,
        0
    ],
    [
        221,
        212,
        152
    ],
    [
        215,
        205,
        140
    ],
    [
        210,
        199,
        128
    ],
    [
        193,
        180,
        95
    ],
    [
        173,
        157,
        59
    ],
    [
        160,
        144,
        40
    ],
    [
        140,
        122,
        22
    ],
    [
        208,
        224,
        215
    ],
    [
        184,
        204,
        194
    ],
    [
        154,
        184,
        172
    ],
    [
        120,
        157,
        144
    ],
    [
        79,
        126,
        112
    ],
    [
        37,
        91,
        77
    ],
    [
        18,
        49,
        42
    ],
    [
        184,
        220,
        225
    ],
    [
        153,
        209,
        220
    ],
    [
        118,
        195,
        212
    ],
    [
        58,
        176,
        200
    ],
    [
        0,
        145,
        178
    ],
    [
        0,
        113,
        151
    ],
    [
        0,
        92,
        130
    ],
    [
        163,
        217,
        231
    ],
    [
        137,
        210,
        230
    ],
    [
        77,
        193,
        223
    ],
    [
        0,
        172,
        215
    ],
    [
        0,
        147,
        201
    ],
    [
        0,
        128,
        187
    ],
    [
        0,
        101,
        160
    ],
    [
        212,
        220,
        229
    ],
    [
        199,
        212,
        226
    ],
    [
        156,
        182,
        210
    ],
    [
        125,
        160,
        195
    ],
    [
        93,
        137,
        179
    ],
    [
        38,
        95,
        146
    ],
    [
        0,
        44,
        92
    ],
    [
        220,
        225,
        233
    ],
    [
        207,
        217,
        229
    ],
    [
        168,
        187,
        213
    ],
    [
        126,
        154,
        191
    ],
    [
        52,
        96,
        148
    ],
    [
        0,
        57,
        111
    ],
    [
        0,
        36,
        83
    ],
    [
        222,
        227,
        235
    ],
    [
        202,
        215,
        235
    ],
    [
        178,
        200,
        232
    ],
    [
        123,
        163,
        220
    ],
    [
        64,
        124,
        201
    ],
    [
        0,
        52,
        148
    ],
    [
        0,
        27,
        112
    ],
    [
        230,
        225,
        229
    ],
    [
        225,
        218,
        225
    ],
    [
        200,
        187,
        207
    ],
    [
        163,
        145,
        177
    ],
    [
        125,
        104,
        144
    ],
    [
        99,
        75,
        119
    ],
    [
        63,
        42,
        85
    ],
    [
        237,
        210,
        225
    ],
    [
        233,
        187,
        215
    ],
    [
        227,
        159,
        200
    ],
    [
        223,
        132,
        186
    ],
    [
        205,
        85,
        153
    ],
    [
        180,
        35,
        113
    ],
    [
        155,
        0,
        78
    ],
    [
        232,
        205,
        219
    ],
    [
        231,
        199,
        215
    ],
    [
        224,
        189,
        210
    ],
    [
        205,
        148,
        179
    ],
    [
        180,
        106,
        148
    ],
    [
        157,
        71,
        119
    ],
    [
        127,
        39,
        84
    ],
    [
        253,
        163,
        186
    ],
    [
        251,
        116,
        151
    ],
    [
        245,
        65,
        108
    ],
    [
        236,
        0,
        68
    ],
    [
        197,
        9,
        59
    ],
    [
        160,
        39,
        65
    ],
    [
        123,
        47,
        62
    ],
    [
        248,
        181,
        204
    ],
    [
        250,
        154,
        186
    ],
    [
        245,
        75,
        127
    ],
    [
        233,
        0,
        75
    ],
    [
        203,
        0,
        60
    ],
    [
        172,
        6,
        60
    ],
    [
        143,
        19,
        54
    ],
    [
        239,
        198,
        205
    ],
    [
        238,
        154,
        172
    ],
    [
        231,
        66,
        96
    ],
    [
        220,
        0,
        49
    ],
    [
        194,
        0,
        47
    ],
    [
        163,
        31,
        52
    ],
    [
        139,
        35,
        49
    ],
    [
        240,
        178,
        201
    ],
    [
        236,
        128,
        168
    ],
    [
        231,
        65,
        122
    ],
    [
        214,
        0,
        54
    ],
    [
        171,
        0,
        50
    ],
    [
        139,
        28,
        64
    ],
    [
        115,
        36,
        60
    ],
    [
        253,
        159,
        200
    ],
    [
        251,
        126,
        180
    ],
    [
        245,
        80,
        150
    ],
    [
        234,
        29,
        117
    ],
    [
        213,
        15,
        103
    ],
    [
        175,
        21,
        86
    ],
    [
        128,
        33,
        69
    ],
    [
        238,
        189,
        218
    ],
    [
        235,
        107,
        175
    ],
    [
        225,
        18,
        130
    ],
    [
        214,
        0,
        86
    ],
    [
        171,
        0,
        78
    ],
    [
        151,
        0,
        71
    ],
    [
        112,
        28,
        69
    ],
    [
        242,
        148,
        205
    ],
    [
        239,
        110,
        186
    ],
    [
        229,
        25,
        146
    ],
    [
        215,
        0,
        109
    ],
    [
        176,
        0,
        96
    ],
    [
        141,
        11,
        86
    ],
    [
        105,
        32,
        68
    ],
    [
        247,
        166,
        214
    ],
    [
        246,
        119,
        196
    ],
    [
        239,
        63,
        169
    ],
    [
        231,
        0,
        148
    ],
    [
        204,
        0,
        122
    ],
    [
        166,
        0,
        99
    ],
    [
        136,
        13,
        83
    ],
    [
        244,
        167,
        220
    ],
    [
        241,
        133,
        206
    ],
    [
        234,
        93,
        189
    ],
    [
        224,
        62,
        174
    ],
    [
        203,
        43,
        153
    ],
    [
        180,
        23,
        130
    ],
    [
        131,
        34,
        94
    ],
    [
        242,
        185,
        223
    ],
    [
        230,
        119,
        203
    ],
    [
        217,
        57,
        178
    ],
    [
        207,
        0,
        158
    ],
    [
        181,
        0,
        140
    ],
    [
        162,
        0,
        124
    ],
    [
        136,
        0,
        99
    ],
    [
        238,
        183,
        226
    ],
    [
        233,
        155,
        218
    ],
    [
        227,
        126,
        209
    ],
    [
        205,
        37,
        175
    ],
    [
        193,
        20,
        161
    ],
    [
        169,
        25,
        141
    ],
    [
        132,
        38,
        106
    ],
    [
        234,
        185,
        228
    ],
    [
        225,
        155,
        223
    ],
    [
        209,
        98,
        205
    ],
    [
        193,
        38,
        184
    ],
    [
        179,
        25,
        171
    ],
    [
        158,
        26,
        150
    ],
    [
        119,
        33,
        108
    ],
    [
        218,
        189,
        220
    ],
    [
        201,
        160,
        206
    ],
    [
        144,
        71,
        153
    ],
    [
        112,
        32,
        118
    ],
    [
        103,
        37,
        101
    ],
    [
        95,
        40,
        94
    ],
    [
        84,
        39,
        78
    ],
    [
        219,
        168,
        226
    ],
    [
        204,
        138,
        218
    ],
    [
        178,
        78,
        196
    ],
    [
        159,
        38,
        180
    ],
    [
        139,
        24,
        155
    ],
    [
        123,
        36,
        129
    ],
    [
        104,
        47,
        100
    ],
    [
        206,
        162,
        215
    ],
    [
        188,
        131,
        201
    ],
    [
        163,
        93,
        180
    ],
    [
        163,
        93,
        180
    ],
    [
        115,
        33,
        129
    ],
    [
        106,
        30,
        116
    ],
    [
        98,
        31,
        101
    ],
    [
        196,
        159,
        216
    ],
    [
        170,
        123,
        201
    ],
    [
        132,
        70,
        172
    ],
    [
        93,
        12,
        139
    ],
    [
        82,
        9,
        117
    ],
    [
        73,
        12,
        102
    ],
    [
        62,
        16,
        81
    ],
    [
        218,
        196,
        229
    ],
    [
        196,
        166,
        225
    ],
    [
        147,
        99,
        204
    ],
    [
        120,
        60,
        189
    ],
    [
        98,
        37,
        157
    ],
    [
        90,
        43,
        129
    ],
    [
        83,
        44,
        108
    ],
    [
        198,
        179,
        225
    ],
    [
        176,
        149,
        218
    ],
    [
        150,
        120,
        210
    ],
    [
        127,
        86,
        197
    ],
    [
        67,
        0,
        152
    ],
    [
        50,
        0,
        110
    ],
    [
        47,
        26,
        69
    ],
    [
        182,
        179,
        223
    ],
    [
        151,
        148,
        210
    ],
    [
        117,
        115,
        192
    ],
    [
        35,
        19,
        94
    ],
    [
        33,
        22,
        80
    ],
    [
        33,
        22,
        70
    ],
    [
        35,
        28,
        52
    ],
    [
        169,
        163,
        223
    ],
    [
        139,
        131,
        214
    ],
    [
        103,
        93,
        198
    ],
    [
        41,
        0,
        136
    ],
    [
        37,
        6,
        112
    ],
    [
        34,
        16,
        96
    ],
    [
        32,
        21,
        69
    ],
    [
        204,
        210,
        233
    ],
    [
        159,
        173,
        228
    ],
    [
        69,
        92,
        199
    ],
    [
        23,
        36,
        169
    ],
    [
        16,
        30,
        142
    ],
    [
        18,
        31,
        107
    ],
    [
        19,
        27,
        77
    ],
    [
        196,
        213,
        236
    ],
    [
        167,
        197,
        237
    ],
    [
        46,
        125,
        225
    ],
    [
        5,
        13,
        158
    ],
    [
        0,
        26,
        113
    ],
    [
        0,
        30,
        95
    ],
    [
        20,
        40,
        75
    ],
    [
        185,
        203,
        234
    ],
    [
        90,
        135,
        217
    ],
    [
        0,
        71,
        186
    ],
    [
        0,
        9,
        139
    ],
    [
        0,
        25,
        112
    ],
    [
        0,
        29,
        96
    ],
    [
        5,
        28,
        72
    ],
    [
        173,
        200,
        231
    ],
    [
        140,
        183,
        232
    ],
    [
        62,
        141,
        221
    ],
    [
        0,
        21,
        136
    ],
    [
        1,
        33,
        104
    ],
    [
        0,
        31,
        91
    ],
    [
        3,
        29,
        64
    ],
    [
        229,
        197,
        211
    ],
    [
        222,
        181,
        200
    ],
    [
        211,
        159,
        183
    ],
    [
        193,
        132,
        163
    ],
    [
        171,
        99,
        136
    ],
    [
        140,
        59,
        102
    ],
    [
        100,
        34,
        63
    ],
    [
        236,
        204,
        206
    ],
    [
        230,
        190,
        194
    ],
    [
        219,
        162,
        170
    ],
    [
        199,
        132,
        143
    ],
    [
        184,
        107,
        120
    ],
    [
        155,
        71,
        85
    ],
    [
        140,
        59,
        68
    ],
    [
        245,
        211,
        214
    ],
    [
        247,
        194,
        202
    ],
    [
        247,
        172,
        184
    ],
    [
        234,
        133,
        151
    ],
    [
        215,
        92,
        113
    ],
    [
        189,
        58,
        73
    ],
    [
        162,
        41,
        46
    ],
    [
        248,
        218,
        223
    ],
    [
        248,
        205,
        214
    ],
    [
        251,
        181,
        195
    ],
    [
        252,
        145,
        162
    ],
    [
        244,
        97,
        119
    ],
    [
        231,
        61,
        80
    ],
    [
        209,
        42,
        46
    ],
    [
        255,
        201,
        153
    ],
    [
        255,
        190,
        132
    ],
    [
        255,
        169,
        96
    ],
    [
        249,
        141,
        41
    ],
    [
        237,
        119,
        0
    ],
    [
        215,
        94,
        0
    ],
    [
        193,
        77,
        0
    ],
    [
        238,
        199,
        161
    ],
    [
        233,
        183,
        137
    ],
    [
        223,
        163,
        108
    ],
    [
        203,
        130,
        65
    ],
    [
        180,
        106,
        31
    ],
    [
        149,
        78,
        14
    ],
    [
        127,
        64,
        20
    ],
    [
        226,
        193,
        157
    ],
    [
        220,
        179,
        140
    ],
    [
        207,
        160,
        117
    ],
    [
        183,
        129,
        79
    ],
    [
        160,
        102,
        44
    ],
    [
        121,
        66,
        15
    ],
    [
        100,
        52,
        15
    ],
    [
        248,
        224,
        164
    ],
    [
        238,
        215,
        151
    ],
    [
        241,
        210,
        130
    ],
    [
        247,
        217,
        61
    ],
    [
        243,
        205,
        0
    ],
    [
        244,
        195,
        0
    ],
    [
        206,
        159,
        81
    ],
    [
        249,
        190,
        0
    ],
    [
        246,
        178,
        33
    ],
    [
        255,
        174,
        117
    ],
    [
        235,
        165,
        93
    ],
    [
        214,
        130,
        48
    ],
    [
        225,
        132,
        49
    ],
    [
        196,
        108,
        19
    ],
    [
        234,
        184,
        166
    ],
    [
        235,
        104,
        81
    ],
    [
        232,
        74,
        55
    ],
    [
        209,
        84,
        88
    ],
    [
        178,
        75,
        88
    ],
    [
        160,
        32,
        64
    ],
    [
        106,
        26,
        49
    ],
    [
        246,
        205,
        211
    ],
    [
        228,
        98,
        133
    ],
    [
        231,
        69,
        130
    ],
    [
        186,
        34,
        83
    ],
    [
        168,
        15,
        61
    ],
    [
        157,
        22,
        46
    ],
    [
        109,
        42,
        60
    ],
    [
        229,
        186,
        201
    ],
    [
        224,
        168,
        190
    ],
    [
        206,
        126,
        156
    ],
    [
        187,
        90,
        127
    ],
    [
        173,
        56,
        99
    ],
    [
        159,
        48,
        88
    ],
    [
        138,
        36,
        78
    ],
    [
        242,
        218,
        233
    ],
    [
        207,
        172,
        206
    ],
    [
        216,
        157,
        215
    ],
    [
        182,
        135,
        184
    ],
    [
        167,
        118,
        165
    ],
    [
        162,
        92,
        191
    ],
    [
        154,
        60,
        187
    ],
    [
        222,
        218,
        232
    ],
    [
        183,
        184,
        220
    ],
    [
        168,
        161,
        193
    ],
    [
        140,
        133,
        201
    ],
    [
        96,
        71,
        117
    ],
    [
        76,
        55,
        75
    ],
    [
        67,
        38,
        58
    ],
    [
        190,
        195,
        217
    ],
    [
        137,
        169,
        226
    ],
    [
        129,
        147,
        220
    ],
    [
        124,
        164,
        221
    ],
    [
        97,
        141,
        180
    ],
    [
        58,
        92,
        172
    ],
    [
        96,
        109,
        178
    ],
    [
        187,
        219,
        229
    ],
    [
        113,
        177,
        200
    ],
    [
        65,
        150,
        180
    ],
    [
        0,
        131,
        193
    ],
    [
        0,
        124,
        186
    ],
    [
        0,
        83,
        138
    ],
    [
        0,
        42,
        72
    ],
    [
        161,
        207,
        202
    ],
    [
        62,
        191,
        172
    ],
    [
        0,
        173,
        187
    ],
    [
        0,
        160,
        174
    ],
    [
        0,
        114,
        152
    ],
    [
        0,
        93,
        133
    ],
    [
        0,
        87,
        111
    ],
    [
        125,
        219,
        211
    ],
    [
        90,
        182,
        178
    ],
    [
        30,
        152,
        138
    ],
    [
        0,
        116,
        128
    ],
    [
        70,
        121,
        122
    ],
    [
        7,
        80,
        86
    ],
    [
        38,
        75,
        89
    ],
    [
        151,
        226,
        190
    ],
    [
        5,
        206,
        123
    ],
    [
        0,
        188,
        111
    ],
    [
        0,
        180,
        79
    ],
    [
        0,
        157,
        78
    ],
    [
        35,
        92,
        55
    ],
    [
        0,
        86,
        63
    ],
    [
        209,
        221,
        186
    ],
    [
        188,
        223,
        146
    ],
    [
        139,
        220,
        100
    ],
    [
        114,
        213,
        74
    ],
    [
        114,
        168,
        79
    ],
    [
        115,
        152,
        73
    ],
    [
        121,
        132,
        59
    ],
    [
        194,
        202,
        127
    ],
    [
        189,
        195,
        145
    ],
    [
        156,
        174,
        135
    ],
    [
        144,
        153,
        61
    ],
    [
        119,
        135,
        28
    ],
    [
        123,
        113,
        86
    ],
    [
        92,
        97,
        52
    ],
    [
        242,
        228,
        177
    ],
    [
        225,
        208,
        165
    ],
    [
        218,
        199,
        157
    ],
    [
        209,
        183,
        134
    ],
    [
        171,
        151,
        102
    ],
    [
        150,
        120,
        93
    ],
    [
        131,
        96,
        63
    ],
    [
        243,
        218,
        177
    ],
    [
        254,
        209,
        152
    ],
    [
        226,
        183,
        125
    ],
    [
        216,
        164,
        96
    ],
    [
        200,
        137,
        60
    ],
    [
        186,
        119,
        38
    ],
    [
        168,
        99,
        24
    ],
    [
        227,
        182,
        164
    ],
    [
        215,
        162,
        132
    ],
    [
        199,
        139,
        102
    ],
    [
        156,
        84,
        42
    ],
    [
        137,
        66,
        28
    ],
    [
        111,
        78,
        70
    ],
    [
        95,
        74,
        59
    ],
    [
        237,
        190,
        175
    ],
    [
        196,
        154,
        129
    ],
    [
        186,
        105,
        84
    ],
    [
        175,
        92,
        86
    ],
    [
        168,
        81,
        71
    ],
    [
        159,
        104,
        79
    ],
    [
        141,
        58,
        24
    ],
    [
        148,
        192,
        233
    ],
    [
        108,
        170,
        228
    ],
    [
        0,
        112,
        205
    ],
    [
        0,
        50,
        160
    ],
    [
        0,
        47,
        134
    ],
    [
        0,
        44,
        115
    ],
    [
        12,
        34,
        63
    ],
    [
        186,
        216,
        234
    ],
    [
        153,
        202,
        234
    ],
    [
        103,
        178,
        231
    ],
    [
        0,
        60,
        166
    ],
    [
        0,
        46,
        108
    ],
    [
        0,
        40,
        85
    ],
    [
        6,
        27,
        43
    ],
    [
        143,
        198,
        232
    ],
    [
        97,
        179,
        228
    ],
    [
        0,
        154,
        221
    ],
    [
        0,
        85,
        183
    ],
    [
        0,
        74,
        151
    ],
    [
        0,
        55,
        99
    ],
    [
        0,
        37,
        61
    ],
    [
        110,
        196,
        232
    ],
    [
        64,
        180,
        229
    ],
    [
        0,
        160,
        223
    ],
    [
        0,
        92,
        184
    ],
    [
        0,
        73,
        135
    ],
    [
        0,
        58,
        92
    ],
    [
        1,
        41,
        57
    ],
    [
        151,
        212,
        233
    ],
    [
        87,
        192,
        232
    ],
    [
        0,
        167,
        225
    ],
    [
        0,
        116,
        200
    ],
    [
        0,
        95,
        155
    ],
    [
        0,
        77,
        113
    ],
    [
        0,
        61,
        81
    ],
    [
        152,
        218,
        233
    ],
    [
        87,
        200,
        231
    ],
    [
        0,
        178,
        226
    ],
    [
        0,
        129,
        201
    ],
    [
        0,
        105,
        166
    ],
    [
        0,
        86,
        124
    ],
    [
        0,
        58,
        73
    ],
    [
        102,
        207,
        227
    ],
    [
        0,
        193,
        222
    ],
    [
        0,
        166,
        206
    ],
    [
        0,
        143,
        190
    ],
    [
        0,
        125,
        164
    ],
    [
        0,
        101,
        127
    ],
    [
        0,
        70,
        79
    ],
    [
        98,
        208,
        223
    ],
    [
        0,
        191,
        213
    ],
    [
        0,
        171,
        199
    ],
    [
        0,
        138,
        171
    ],
    [
        0,
        116,
        140
    ],
    [
        0,
        95,
        113
    ],
    [
        0,
        77,
        88
    ],
    [
        176,
        226,
        226
    ],
    [
        134,
        218,
        222
    ],
    [
        37,
        202,
        211
    ],
    [
        0,
        153,
        168
    ],
    [
        0,
        137,
        149
    ],
    [
        0,
        112,
        120
    ],
    [
        0,
        93,
        98
    ],
    [
        155,
        217,
        217
    ],
    [
        100,
        203,
        201
    ],
    [
        0,
        175,
        169
    ],
    [
        0,
        131,
        116
    ],
    [
        0,
        113,
        102
    ],
    [
        0,
        102,
        94
    ],
    [
        0,
        81,
        75
    ],
    [
        110,
        218,
        213
    ],
    [
        27,
        207,
        201
    ],
    [
        0,
        187,
        179
    ],
    [
        0,
        161,
        154
    ],
    [
        0,
        131,
        122
    ],
    [
        0,
        87,
        79
    ],
    [
        0,
        75,
        68
    ],
    [
        94,
        41,
        42
    ],
    [
        129,
        48,
        51
    ],
    [
        147,
        50,
        53
    ],
    [
        222,
        135,
        151
    ],
    [
        236,
        162,
        176
    ],
    [
        244,
        189,
        198
    ],
    [
        244,
        198,
        206
    ],
    [
        82,
        48,
        44
    ],
    [
        106,
        56,
        51
    ],
    [
        122,
        63,
        56
    ],
    [
        200,
        134,
        140
    ],
    [
        220,
        165,
        171
    ],
    [
        230,
        187,
        192
    ],
    [
        235,
        196,
        198
    ],
    [
        65,
        31,
        31
    ],
    [
        137,
        75,
        81
    ],
    [
        157,
        97,
        103
    ],
    [
        178,
        123,
        128
    ],
    [
        205,
        161,
        165
    ],
    [
        221,
        183,
        187
    ],
    [
        225,
        194,
        193
    ],
    [
        87,
        41,
        48
    ],
    [
        112,
        45,
        61
    ],
    [
        135,
        53,
        76
    ],
    [
        216,
        145,
        168
    ],
    [
        230,
        169,
        184
    ],
    [
        235,
        179,
        195
    ],
    [
        237,
        190,
        200
    ],
    [
        99,
        44,
        79
    ],
    [
        133,
        49,
        117
    ],
    [
        148,
        52,
        140
    ],
    [
        214,
        139,
        198
    ],
    [
        228,
        172,
        213
    ],
    [
        233,
        190,
        219
    ],
    [
        237,
        197,
        222
    ],
    [
        83,
        41,
        67
    ],
    [
        109,
        58,
        93
    ],
    [
        129,
        83,
        116
    ],
    [
        158,
        118,
        145
    ],
    [
        193,
        164,
        183
    ],
    [
        212,
        191,
        204
    ],
    [
        218,
        198,
        206
    ],
    [
        77,
        46,
        71
    ],
    [
        91,
        48,
        93
    ],
    [
        102,
        46,
        107
    ],
    [
        168,
        126,
        177
    ],
    [
        189,
        155,
        196
    ],
    [
        204,
        176,
        207
    ],
    [
        215,
        194,
        214
    ],
    [
        76,
        48,
        64
    ],
    [
        104,
        68,
        88
    ],
    [
        136,
        99,
        120
    ],
    [
        176,
        149,
        165
    ],
    [
        200,
        175,
        187
    ],
    [
        210,
        189,
        198
    ],
    [
        221,
        205,
        210
    ],
    [
        89,
        43,
        94
    ],
    [
        116,
        47,
        138
    ],
    [
        132,
        48,
        166
    ],
    [
        186,
        128,
        208
    ],
    [
        206,
        161,
        220
    ],
    [
        218,
        183,
        226
    ],
    [
        226,
        199,
        230
    ],
    [
        30,
        25,
        51
    ],
    [
        64,
        57,
        95
    ],
    [
        90,
        83,
        119
    ],
    [
        142,
        136,
        163
    ],
    [
        180,
        175,
        195
    ],
    [
        199,
        195,
        209
    ],
    [
        218,
        214,
        222
    ],
    [
        28,
        30,
        41
    ],
    [
        33,
        40,
        68
    ],
    [
        30,
        52,
        93
    ],
    [
        142,
        158,
        188
    ],
    [
        162,
        177,
        200
    ],
    [
        187,
        198,
        214
    ],
    [
        198,
        207,
        218
    ],
    [
        0,
        37,
        57
    ],
    [
        0,
        47,
        86
    ],
    [
        0,
        59,
        112
    ],
    [
        124,
        173,
        211
    ],
    [
        164,
        199,
        226
    ],
    [
        191,
        212,
        230
    ],
    [
        199,
        217,
        230
    ],
    [
        10,
        30,
        44
    ],
    [
        79,
        115,
        138
    ],
    [
        93,
        126,
        149
    ],
    [
        124,
        151,
        171
    ],
    [
        167,
        186,
        200
    ],
    [
        184,
        199,
        211
    ],
    [
        191,
        205,
        212
    ],
    [
        8,
        41,
        47
    ],
    [
        0,
        47,
        59
    ],
    [
        0,
        59,
        76
    ],
    [
        106,
        162,
        184
    ],
    [
        141,
        183,
        201
    ],
    [
        164,
        197,
        210
    ],
    [
        187,
        210,
        219
    ],
    [
        8,
        38,
        44
    ],
    [
        9,
        92,
        102
    ],
    [
        77,
        133,
        141
    ],
    [
        126,
        167,
        172
    ],
    [
        147,
        182,
        187
    ],
    [
        172,
        198,
        200
    ],
    [
        183,
        206,
        207
    ],
    [
        26,
        50,
        46
    ],
    [
        63,
        91,
        87
    ],
    [
        95,
        119,
        116
    ],
    [
        132,
        151,
        148
    ],
    [
        157,
        174,
        171
    ],
    [
        178,
        191,
        187
    ],
    [
        188,
        199,
        196
    ],
    [
        42,
        70,
        51
    ],
    [
        34,
        90,
        64
    ],
    [
        44,
        112,
        79
    ],
    [
        112,
        160,
        135
    ],
    [
        135,
        175,
        153
    ],
    [
        155,
        189,
        170
    ],
    [
        175,
        201,
        184
    ],
    [
        23,
        47,
        40
    ],
    [
        67,
        104,
        90
    ],
    [
        91,
        127,
        112
    ],
    [
        126,
        156,
        144
    ],
    [
        146,
        171,
        160
    ],
    [
        166,
        188,
        176
    ],
    [
        190,
        205,
        194
    ],
    [
        27,
        60,
        51
    ],
    [
        0,
        88,
        77
    ],
    [
        0,
        110,
        98
    ],
    [
        103,
        186,
        175
    ],
    [
        130,
        199,
        188
    ],
    [
        159,
        212,
        201
    ],
    [
        183,
        219,
        209
    ],
    [
        34,
        54,
        42
    ],
    [
        94,
        114,
        96
    ],
    [
        114,
        132,
        114
    ],
    [
        149,
        164,
        149
    ],
    [
        164,
        177,
        163
    ],
    [
        178,
        188,
        175
    ],
    [
        188,
        196,
        185
    ],
    [
        22,
        61,
        52
    ],
    [
        0,
        106,
        90
    ],
    [
        0,
        127,
        109
    ],
    [
        105,
        201,
        185
    ],
    [
        148,
        218,
        206
    ],
    [
        162,
        222,
        210
    ],
    [
        180,
        225,
        216
    ],
    [
        78,
        90,
        48
    ],
    [
        105,
        129,
        58
    ],
    [
        122,
        155,
        73
    ],
    [
        171,
        194,
        126
    ],
    [
        184,
        205,
        149
    ],
    [
        191,
        207,
        154
    ],
    [
        197,
        212,
        163
    ],
    [
        64,
        71,
        38
    ],
    [
        96,
        102,
        55
    ],
    [
        118,
        122,
        76
    ],
    [
        138,
        143,
        100
    ],
    [
        164,
        169,
        130
    ],
    [
        180,
        183,
        148
    ],
    [
        196,
        198,
        166
    ],
    [
        215,
        209,
        196
    ],
    [
        199,
        185,
        171
    ],
    [
        183,
        169,
        152
    ],
    [
        164,
        147,
        130
    ],
    [
        124,
        103,
        85
    ],
    [
        101,
        80,
        60
    ],
    [
        72,
        54,
        39
    ],
    [
        211,
        204,
        188
    ],
    [
        184,
        176,
        155
    ],
    [
        168,
        159,
        135
    ],
    [
        169,
        171,
        160
    ],
    [
        147,
        153,
        143
    ],
    [
        143,
        142,
        136
    ],
    [
        75,
        78,
        83
    ],
    [
        218,
        223,
        225
    ],
    [
        165,
        187,
        194
    ],
    [
        152,
        163,
        173
    ],
    [
        118,
        133,
        145
    ],
    [
        66,
        83,
        99
    ],
    [
        38,
        54,
        69
    ],
    [
        20,
        29,
        40
    ],
    [
        247,
        234,
        95
    ],
    [
        248,
        233,
        70
    ],
    [
        253,
        227,
        0
    ],
    [
        255,
        221,
        0
    ],
    [
        198,
        169,
        0
    ],
    [
        177,
        151,
        0
    ],
    [
        139,
        122,
        39
    ],
    [
        251,
        227,
        67
    ],
    [
        254,
        224,
        30
    ],
    [
        255,
        219,
        0
    ],
    [
        255,
        209,
        0
    ],
    [
        219,
        169,
        0
    ],
    [
        172,
        137,
        0
    ],
    [
        157,
        131,
        15
    ],
    [
        253,
        223,
        80
    ],
    [
        254,
        221,
        60
    ],
    [
        255,
        217,
        35
    ],
    [
        255,
        205,
        0
    ],
    [
        204,
        151,
        0
    ],
    [
        174,
        132,
        0
    ],
    [
        138,
        115,
        31
    ],
    [
        254,
        218,
        99
    ],
    [
        255,
        215,
        85
    ],
    [
        255,
        208,
        62
    ],
    [
        255,
        198,
        39
    ],
    [
        237,
        169,
        0
    ],
    [
        183,
        132,
        0
    ],
    [
        157,
        118,
        12
    ],
    [
        249,
        223,
        141
    ],
    [
        254,
        214,
        113
    ],
    [
        255,
        200,
        66
    ],
    [
        255,
        183,
        24
    ],
    [
        201,
        145,
        13
    ],
    [
        175,
        132,
        29
    ],
    [
        138,
        107,
        36
    ],
    [
        245,
        219,
        107
    ],
    [
        247,
        212,
        75
    ],
    [
        246,
        206,
        60
    ],
    [
        247,
        167,
        0
    ],
    [
        208,
        137,
        0
    ],
    [
        163,
        115,
        0
    ],
    [
        110,
        86,
        27
    ],
    [
        255,
        209,
        108
    ],
    [
        255,
        197,
        86
    ],
    [
        255,
        191,
        59
    ],
    [
        255,
        163,
        0
    ],
    [
        225,
        124,
        0
    ],
    [
        178,
        109,
        0
    ],
    [
        117,
        83,
        25
    ],
    [
        254,
        207,
        132
    ],
    [
        255,
        197,
        108
    ],
    [
        255,
        181,
        70
    ],
    [
        255,
        158,
        21
    ],
    [
        216,
        120,
        0
    ],
    [
        156,
        95,
        21
    ],
    [
        112,
        75,
        28
    ],
    [
        246,
        197,
        91
    ],
    [
        245,
        189,
        71
    ],
    [
        246,
        179,
        51
    ],
    [
        243,
        138,
        0
    ],
    [
        213,
        126,
        0
    ],
    [
        171,
        107,
        14
    ],
    [
        115,
        91,
        40
    ],
    [
        255,
        203,
        138
    ],
    [
        255,
        194,
        122
    ],
    [
        255,
        178,
        88
    ],
    [
        255,
        131,
        0
    ],
    [
        233,
        114,
        0
    ],
    [
        193,
        106,
        16
    ],
    [
        158,
        89,
        22
    ],
    [
        255,
        173,
        94
    ],
    [
        255,
        143,
        18
    ],
    [
        255,
        106,
        0
    ],
    [
        255,
        80,
        0
    ],
    [
        189,
        70,
        0
    ],
    [
        151,
        69,
        8
    ],
    [
        104,
        56,
        22
    ],
    [
        243,
        208,
        158
    ],
    [
        242,
        188,
        123
    ],
    [
        241,
        159,
        83
    ],
    [
        239,
        117,
        33
    ],
    [
        210,
        93,
        18
    ],
    [
        165,
        84,
        26
    ],
    [
        99,
        60,
        32
    ],
    [
        255,
        185,
        141
    ],
    [
        255,
        161,
        103
    ],
    [
        255,
        126,
        46
    ],
    [
        255,
        107,
        11
    ],
    [
        220,
        95,
        19
    ],
    [
        169,
        85,
        33
    ],
    [
        142,
        71,
        30
    ],
    [
        255,
        190,
        158
    ],
    [
        255,
        157,
        107
    ],
    [
        255,
        127,
        63
    ],
    [
        255,
        102,
        27
    ],
    [
        231,
        82,
        0
    ],
    [
        194,
        82,
        25
    ],
    [
        117,
        56,
        27
    ],
    [
        255,
        163,
        136
    ],
    [
        255,
        141,
        106
    ],
    [
        255,
        107,
        53
    ],
    [
        255,
        76,
        0
    ],
    [
        226,
        67,
        1
    ],
    [
        173,
        67,
        28
    ],
    [
        134,
        57,
        31
    ],
    [
        255,
        180,
        170
    ],
    [
        255,
        134,
        113
    ],
    [
        255,
        91,
        52
    ],
    [
        255,
        70,
        17
    ],
    [
        211,
        68,
        28
    ],
    [
        153,
        56,
        31
    ],
    [
        109,
        53,
        39
    ],
    [
        255,
        177,
        184
    ],
    [
        255,
        129,
        136
    ],
    [
        255,
        88,
        89
    ],
    [
        255,
        67,
        55
    ],
    [
        229,
        60,
        46
    ],
    [
        195,
        58,
        50
    ],
    [
        132,
        50,
        44
    ],
    [
        255,
        164,
        179
    ],
    [
        255,
        142,
        158
    ],
    [
        255,
        72,
        91
    ],
    [
        244,
        36,
        52
    ],
    [
        216,
        38,
        46
    ],
    [
        179,
        39,
        45
    ],
    [
        128,
        37,
        40
    ],
    [
        255,
        175,
        191
    ],
    [
        255,
        99,
        125
    ],
    [
        251,
        52,
        72
    ],
    [
        246,
        50,
        62
    ],
    [
        208,
        50,
        56
    ],
    [
        168,
        51,
        56
    ],
    [
        102,
        50,
        51
    ],
    [
        254,
        187,
        202
    ],
    [
        255,
        155,
        177
    ],
    [
        252,
        82,
        114
    ],
    [
        235,
        0,
        40
    ],
    [
        207,
        10,
        44
    ],
    [
        171,
        22,
        43
    ],
    [
        121,
        35,
        46
    ],
    [
        218,
        216,
        214
    ],
    [
        210,
        207,
        205
    ],
    [
        201,
        200,
        199
    ],
    [
        188,
        187,
        186
    ],
    [
        179,
        177,
        177
    ],
    [
        169,
        168,
        169
    ],
    [
        153,
        152,
        153
    ],
    [
        138,
        138,
        141
    ],
    [
        119,
        119,
        121
    ],
    [
        100,
        100,
        105
    ],
    [
        85,
        85,
        89
    ],
    [
        51,
        46,
        31
    ],
    [
        33,
        38,
        32
    ],
    [
        49,
        37,
        28
    ],
    [
        63,
        42,
        46
    ],
    [
        16,
        24,
        31
    ],
    [
        61,
        56,
        52
    ],
    [
        76,
        64,
        40
    ],
    [
        85,
        69,
        37
    ],
    [
        92,
        73,
        37
    ],
    [
        159,
        145,
        93
    ],
    [
        179,
        167,
        125
    ],
    [
        195,
        186,
        152
    ],
    [
        205,
        196,
        166
    ],
    [
        99,
        78,
        37
    ],
    [
        142,
        118,
        48
    ],
    [
        155,
        132,
        66
    ],
    [
        181,
        162,
        104
    ],
    [
        199,
        182,
        131
    ],
    [
        209,
        195,
        146
    ],
    [
        214,
        202,
        158
    ],
    [
        108,
        90,
        35
    ],
    [
        165,
        141,
        40
    ],
    [
        186,
        156,
        19
    ],
    [
        219,
        197,
        84
    ],
    [
        224,
        204,
        97
    ],
    [
        231,
        214,
        126
    ],
    [
        236,
        222,
        151
    ],
    [
        94,
        69,
        42
    ],
    [
        119,
        77,
        39
    ],
    [
        143,
        89,
        39
    ],
    [
        189,
        149,
        90
    ],
    [
        202,
        169,
        118
    ],
    [
        214,
        186,
        139
    ],
    [
        223,
        201,
        162
    ],
    [
        81,
        43,
        28
    ],
    [
        151,
        95,
        53
    ],
    [
        176,
        124,
        87
    ],
    [
        193,
        147,
        114
    ],
    [
        206,
        167,
        135
    ],
    [
        222,
        191,
        164
    ],
    [
        225,
        197,
        172
    ],
    [
        107,
        63,
        34
    ],
    [
        167,
        89,
        40
    ],
    [
        188,
        97,
        35
    ],
    [
        231,
        157,
        108
    ],
    [
        245,
        190,
        153
    ],
    [
        244,
        198,
        166
    ],
    [
        244,
        207,
        178
    ],
    [
        93,
        51,
        38
    ],
    [
        129,
        76,
        58
    ],
    [
        153,
        106,
        87
    ],
    [
        78,
        136,
        119
    ],
    [
        195,
        162,
        145
    ],
    [
        207,
        180,
        167
    ],
    [
        218,
        194,
        182
    ],
    [
        81,
        53,
        40
    ],
    [
        102,
        58,
        42
    ],
    [
        117,
        61,
        41
    ],
    [
        174,
        127,
        101
    ],
    [
        201,
        167,
        145
    ],
    [
        214,
        185,
        167
    ],
    [
        222,
        199,
        182
    ],
    [
        104,
        47,
        36
    ],
    [
        159,
        49,
        34
    ],
    [
        226,
        35,
        26
    ],
    [
        239,
        143,
        122
    ],
    [
        240,
        166,
        147
    ],
    [
        241,
        184,
        168
    ],
    [
        240,
        194,
        178
    ],
    [
        122,
        222,
        212
    ],
    [
        34,
        211,
        197
    ],
    [
        0,
        196,
        179
    ],
    [
        0,
        175,
        154
    ],
    [
        0,
        147,
        130
    ],
    [
        0,
        117,
        100
    ],
    [
        0,
        76,
        66
    ],
    [
        109,
        203,
        184
    ],
    [
        72,
        195,
        177
    ],
    [
        0,
        169,
        142
    ],
    [
        0,
        152,
        119
    ],
    [
        0,
        128,
        100
    ],
    [
        0,
        104,
        82
    ],
    [
        3,
        68,
        54
    ],
    [
        166,
        229,
        216
    ],
    [
        137,
        225,
        208
    ],
    [
        51,
        217,
        194
    ],
    [
        0,
        168,
        135
    ],
    [
        0,
        149,
        120
    ],
    [
        0,
        121,
        95
    ],
    [
        0,
        99,
        79
    ],
    [
        143,
        212,
        188
    ],
    [
        109,
        205,
        177
    ],
    [
        0,
        177,
        136
    ],
    [
        0,
        148,
        93
    ],
    [
        0,
        120,
        82
    ],
    [
        0,
        101,
        71
    ],
    [
        13,
        85,
        63
    ],
    [
        118,
        224,
        192
    ],
    [
        63,
        212,
        173
    ],
    [
        0,
        192,
        138
    ],
    [
        0,
        172,
        104
    ],
    [
        0,
        117,
        73
    ],
    [
        0,
        97,
        64
    ],
    [
        20,
        70,
        51
    ],
    [
        160,
        216,
        179
    ],
    [
        145,
        213,
        171
    ],
    [
        110,
        202,
        151
    ],
    [
        0,
        152,
        68
    ],
    [
        0,
        131,
        61
    ],
    [
        1,
        104,
        54
    ],
    [
        43,
        80,
        52
    ],
    [
        162,
        227,
        185
    ],
    [
        140,
        224,
        176
    ],
    [
        123,
        222,
        167
    ],
    [
        0,
        174,
        65
    ],
    [
        0,
        149,
        58
    ],
    [
        0,
        120,
        51
    ],
    [
        28,
        86,
        49
    ],
    [
        172,
        218,
        144
    ],
    [
        162,
        214,
        131
    ],
    [
        108,
        192,
        73
    ],
    [
        63,
        174,
        41
    ],
    [
        77,
        156,
        45
    ],
    [
        75,
        139,
        42
    ],
    [
        73,
        117,
        39
    ],
    [
        194,
        223,
        135
    ],
    [
        183,
        220,
        120
    ],
    [
        163,
        213,
        93
    ],
    [
        119,
        188,
        31
    ],
    [
        98,
        166,
        10
    ],
    [
        100,
        139,
        26
    ],
    [
        84,
        97,
        33
    ],
    [
        212,
        235,
        141
    ],
    [
        204,
        233,
        127
    ],
    [
        196,
        231,
        106
    ],
    [
        148,
        213,
        0
    ],
    [
        130,
        188,
        0
    ],
    [
        121,
        153,
        0
    ],
    [
        89,
        97,
        27
    ],
    [
        226,
        230,
        101
    ],
    [
        219,
        227,
        65
    ],
    [
        206,
        219,
        0
    ],
    [
        195,
        213,
        0
    ],
    [
        170,
        173,
        0
    ],
    [
        149,
        147,
        0
    ],
    [
        121,
        112,
        31
    ],
    [
        236,
        235,
        107
    ],
    [
        227,
        231,
        51
    ],
    [
        225,
        230,
        30
    ],
    [
        206,
        222,
        0
    ],
    [
        182,
        189,
        0
    ],
    [
        154,
        148,
        0
    ],
    [
        131,
        121,
        2
    ],
    [
        241,
        236,
        114
    ],
    [
        238,
        232,
        56
    ],
    [
        236,
        232,
        19
    ],
    [
        225,
        224,
        0
    ],
    [
        192,
        184,
        0
    ],
    [
        173,
        163,
        0
    ],
    [
        162,
        145,
        0
    ],
    [
        245,
        232,
        91
    ],
    [
        245,
        229,
        0
    ],
    [
        242,
        223,
        0
    ],
    [
        238,
        220,
        0
    ],
    [
        190,
        166,
        0
    ],
    [
        154,
        134,
        0
    ],
    [
        106,
        92,
        31
    ],
    [
        198,
        190,
        181
    ],
    [
        177,
        169,
        158
    ],
    [
        158,
        148,
        139
    ],
    [
        141,
        131,
        122
    ],
    [
        119,
        109,
        99
    ],
    [
        105,
        96,
        87
    ],
    [
        46,
        41,
        37
    ],
    [
        199,
        187,
        181
    ],
    [
        178,
        167,
        161
    ],
    [
        153,
        139,
        134
    ],
    [
        135,
        120,
        115
    ],
    [
        117,
        102,
        96
    ],
    [
        96,
        80,
        76
    ],
    [
        57,
        46,
        44
    ],
    [
        187,
        185,
        175
    ],
    [
        170,
        168,
        158
    ],
    [
        146,
        146,
        135
    ],
    [
        127,
        126,
        115
    ],
    [
        102,
        102,
        92
    ],
    [
        82,
        82,
        73
    ],
    [
        33,
        34,
        33
    ],
    [
        201,
        200,
        199
    ],
    [
        178,
        179,
        178
    ],
    [
        159,
        161,
        161
    ],
    [
        138,
        139,
        140
    ],
    [
        112,
        113,
        112
    ],
    [
        84,
        87,
        89
    ],
    [
        37,
        39,
        41
    ],
    [
        208,
        210,
        211
    ],
    [
        195,
        197,
        200
    ],
    [
        163,
        169,
        172
    ],
    [
        124,
        133,
        140
    ],
    [
        92,
        102,
        111
    ],
    [
        51,
        61,
        71
    ],
    [
        30,
        36,
        43
    ],
    [
        208,
        196,
        196
    ],
    [
        192,
        178,
        181
    ],
    [
        170,
        152,
        156
    ],
    [
        123,
        101,
        104
    ],
    [
        88,
        68,
        68
    ],
    [
        68,
        54,
        52
    ],
    [
        56,
        45,
        43
    ],
    [
        189,
        198,
        195
    ],
    [
        160,
        172,
        170
    ],
    [
        144,
        156,
        156
    ],
    [
        112,
        123,
        123
    ],
    [
        78,
        87,
        88
    ],
    [
        61,
        69,
        66
    ],
    [
        55,
        57,
        53
    ],
    [
        216,
        209,
        201
    ],
    [
        205,
        195,
        187
    ],
    [
        193,
        183,
        175
    ],
    [
        182,
        173,
        164
    ],
    [
        174,
        161,
        152
    ],
    [
        167,
        155,
        148
    ],
    [
        151,
        139,
        130
    ],
    [
        142,
        130,
        121
    ],
    [
        133,
        119,
        111
    ],
    [
        123,
        109,
        101
    ],
    [
        112,
        98,
        88
    ]
]);
define("szinnevek", [], {
    "5747C": [
        62,
        67,
        28
    ],
    "5757C": [
        108,
        112,
        45
    ],
    "5767C": [
        140,
        140,
        72
    ],
    "5777C": [
        162,
        164,
        103
    ],
    "5787C": [
        186,
        188,
        137
    ],
    "5797C": [
        199,
        199,
        154
    ],
    "5807C": [
        209,
        208,
        169
    ],
    "581C": [
        100,
        92,
        32
    ],
    "582C": [
        145,
        140,
        16
    ],
    "583C": [
        185,
        190,
        16
    ],
    "584C": [
        213,
        214,
        84
    ],
    "585C": [
        222,
        221,
        110
    ],
    "586C": [
        227,
        225,
        123
    ],
    "587C": [
        229,
        227,
        140
    ],
    "5815C": [
        86,
        79,
        35
    ],
    "5825C": [
        139,
        128,
        58
    ],
    "5835C": [
        161,
        152,
        86
    ],
    "5845C": [
        177,
        169,
        110
    ],
    "5855C": [
        193,
        186,
        133
    ],
    "5865C": [
        204,
        198,
        147
    ],
    "5875C": [
        213,
        205,
        158
    ],
    "600C": [
        242,
        234,
        154
    ],
    "602C": [
        242,
        231,
        122
    ],
    "603C": [
        239,
        224,
        72
    ],
    "604C": [
        237,
        218,
        34
    ],
    "605C": [
        226,
        204,
        0
    ],
    "606C": [
        209,
        180,
        0
    ],
    "607C": [
        237,
        228,
        153
    ],
    "608C": [
        234,
        224,
        133
    ],
    "609C": [
        233,
        220,
        117
    ],
    "610C": [
        107,
        50,
        48
    ],
    "611C": [
        216,
        199,
        34
    ],
    "612C": [
        199,
        176,
        0
    ],
    "613C": [
        181,
        154,
        0
    ],
    "614C": [
        221,
        212,
        152
    ],
    "615C": [
        215,
        205,
        140
    ],
    "616C": [
        210,
        199,
        128
    ],
    "617C": [
        193,
        180,
        95
    ],
    "618C": [
        173,
        157,
        59
    ],
    "619C": [
        160,
        144,
        40
    ],
    "620C": [
        140,
        122,
        22
    ],
    "621C": [
        208,
        224,
        215
    ],
    "622C": [
        184,
        204,
        194
    ],
    "623C": [
        154,
        184,
        172
    ],
    "624C": [
        120,
        157,
        144
    ],
    "625C": [
        79,
        126,
        112
    ],
    "626C": [
        37,
        91,
        77
    ],
    "627C": [
        18,
        49,
        42
    ],
    "628C": [
        184,
        220,
        225
    ],
    "629C": [
        153,
        209,
        220
    ],
    "630C": [
        118,
        195,
        212
    ],
    "631C": [
        58,
        176,
        200
    ],
    "632C": [
        0,
        145,
        178
    ],
    "633C": [
        0,
        113,
        151
    ],
    "634C": [
        0,
        92,
        130
    ],
    "635C": [
        163,
        217,
        231
    ],
    "636C": [
        137,
        210,
        230
    ],
    "637C": [
        77,
        193,
        223
    ],
    "638C": [
        0,
        172,
        215
    ],
    "639C": [
        0,
        147,
        201
    ],
    "640C": [
        0,
        128,
        187
    ],
    "641C": [
        0,
        101,
        160
    ],
    "642C": [
        212,
        220,
        229
    ],
    "643C": [
        199,
        212,
        226
    ],
    "644C": [
        156,
        182,
        210
    ],
    "645C": [
        125,
        160,
        195
    ],
    "646C": [
        93,
        137,
        179
    ],
    "647C": [
        38,
        95,
        146
    ],
    "648C": [
        0,
        44,
        92
    ],
    "649C": [
        220,
        225,
        233
    ],
    "650C": [
        207,
        217,
        229
    ],
    "651C": [
        168,
        187,
        213
    ],
    "652C": [
        126,
        154,
        191
    ],
    "653C": [
        52,
        96,
        148
    ],
    "654C": [
        0,
        57,
        111
    ],
    "655C": [
        0,
        36,
        83
    ],
    "656C": [
        222,
        227,
        235
    ],
    "657C": [
        202,
        215,
        235
    ],
    "658C": [
        178,
        200,
        232
    ],
    "659C": [
        123,
        163,
        220
    ],
    "660C": [
        64,
        124,
        201
    ],
    "661C": [
        0,
        52,
        148
    ],
    "662C": [
        0,
        27,
        112
    ],
    "663C": [
        230,
        225,
        229
    ],
    "664C": [
        225,
        218,
        225
    ],
    "665C": [
        200,
        187,
        207
    ],
    "666C": [
        163,
        145,
        177
    ],
    "667C": [
        125,
        104,
        144
    ],
    "668C": [
        99,
        75,
        119
    ],
    "669C": [
        63,
        42,
        85
    ],
    "670C": [
        237,
        210,
        225
    ],
    "671C": [
        233,
        187,
        215
    ],
    "672C": [
        227,
        159,
        200
    ],
    "673C": [
        223,
        132,
        186
    ],
    "674C": [
        205,
        85,
        153
    ],
    "675C": [
        180,
        35,
        113
    ],
    "676C": [
        155,
        0,
        78
    ],
    "677C": [
        232,
        205,
        219
    ],
    "678C": [
        231,
        199,
        215
    ],
    "679C": [
        224,
        189,
        210
    ],
    "680C": [
        205,
        148,
        179
    ],
    "681C": [
        180,
        106,
        148
    ],
    "682C": [
        157,
        71,
        119
    ],
    "683C": [
        127,
        39,
        84
    ],
    "189C": [
        253,
        163,
        186
    ],
    "190C": [
        251,
        116,
        151
    ],
    "191C": [
        245,
        65,
        108
    ],
    "192C": [
        236,
        0,
        68
    ],
    "193C": [
        197,
        9,
        59
    ],
    "194C": [
        160,
        39,
        65
    ],
    "195C": [
        123,
        47,
        62
    ],
    "1895C": [
        248,
        181,
        204
    ],
    "1905C": [
        250,
        154,
        186
    ],
    "1915C": [
        245,
        75,
        127
    ],
    "1925C": [
        233,
        0,
        75
    ],
    "1935C": [
        203,
        0,
        60
    ],
    "1945C": [
        172,
        6,
        60
    ],
    "1955C": [
        143,
        19,
        54
    ],
    "196C": [
        239,
        198,
        205
    ],
    "197C": [
        238,
        154,
        172
    ],
    "198C": [
        231,
        66,
        96
    ],
    "199C": [
        220,
        0,
        49
    ],
    "200C": [
        194,
        0,
        47
    ],
    "201C": [
        163,
        31,
        52
    ],
    "202C": [
        139,
        35,
        49
    ],
    "203C": [
        240,
        178,
        201
    ],
    "204C": [
        236,
        128,
        168
    ],
    "205C": [
        231,
        65,
        122
    ],
    "206C": [
        214,
        0,
        54
    ],
    "207C": [
        171,
        0,
        50
    ],
    "208C": [
        139,
        28,
        64
    ],
    "209C": [
        115,
        36,
        60
    ],
    "210C": [
        253,
        159,
        200
    ],
    "211C": [
        251,
        126,
        180
    ],
    "212C": [
        245,
        80,
        150
    ],
    "213C": [
        234,
        29,
        117
    ],
    "214C": [
        213,
        15,
        103
    ],
    "215C": [
        175,
        21,
        86
    ],
    "216C": [
        128,
        33,
        69
    ],
    "217C": [
        238,
        189,
        218
    ],
    "218C": [
        235,
        107,
        175
    ],
    "219C": [
        225,
        18,
        130
    ],
    "Rubine Red C": [
        214,
        0,
        86
    ],
    "220C": [
        171,
        0,
        78
    ],
    "221C": [
        151,
        0,
        71
    ],
    "222C": [
        112,
        28,
        69
    ],
    "223C": [
        242,
        148,
        205
    ],
    "224C": [
        239,
        110,
        186
    ],
    "225C": [
        229,
        25,
        146
    ],
    "226C": [
        215,
        0,
        109
    ],
    "227C": [
        176,
        0,
        96
    ],
    "228C": [
        141,
        11,
        86
    ],
    "229C": [
        105,
        32,
        68
    ],
    "230C": [
        247,
        166,
        214
    ],
    "231C": [
        246,
        119,
        196
    ],
    "232C": [
        239,
        63,
        169
    ],
    "Rhodamine Red C": [
        231,
        0,
        148
    ],
    "233C": [
        204,
        0,
        122
    ],
    "234C": [
        166,
        0,
        99
    ],
    "235C": [
        136,
        13,
        83
    ],
    "236C": [
        244,
        167,
        220
    ],
    "237C": [
        241,
        133,
        206
    ],
    "238C": [
        234,
        93,
        189
    ],
    "239C": [
        224,
        62,
        174
    ],
    "240C": [
        203,
        43,
        153
    ],
    "241C": [
        180,
        23,
        130
    ],
    "242C": [
        131,
        34,
        94
    ],
    "2365C": [
        242,
        185,
        223
    ],
    "2375C": [
        230,
        119,
        203
    ],
    "2385C": [
        217,
        57,
        178
    ],
    "2395C": [
        207,
        0,
        158
    ],
    "2405C": [
        181,
        0,
        140
    ],
    "2415C": [
        162,
        0,
        124
    ],
    "2425C": [
        136,
        0,
        99
    ],
    "243C": [
        238,
        183,
        226
    ],
    "244C": [
        233,
        155,
        218
    ],
    "245C": [
        227,
        126,
        209
    ],
    "246C": [
        205,
        37,
        175
    ],
    "247C": [
        193,
        20,
        161
    ],
    "248C": [
        169,
        25,
        141
    ],
    "249C": [
        132,
        38,
        106
    ],
    "250C": [
        234,
        185,
        228
    ],
    "251C": [
        225,
        155,
        223
    ],
    "252C": [
        209,
        98,
        205
    ],
    "Purple C": [
        193,
        38,
        184
    ],
    "253C": [
        179,
        25,
        171
    ],
    "254C": [
        158,
        26,
        150
    ],
    "255C": [
        119,
        33,
        108
    ],
    "256C": [
        218,
        189,
        220
    ],
    "257C": [
        201,
        160,
        206
    ],
    "258C": [
        144,
        71,
        153
    ],
    "259C": [
        112,
        32,
        118
    ],
    "260C": [
        103,
        37,
        101
    ],
    "261C": [
        95,
        40,
        94
    ],
    "262C": [
        84,
        39,
        78
    ],
    "2562C": [
        219,
        168,
        226
    ],
    "2572C": [
        204,
        138,
        218
    ],
    "2582C": [
        178,
        78,
        196
    ],
    "2592C": [
        159,
        38,
        180
    ],
    "2602C": [
        139,
        24,
        155
    ],
    "2612C": [
        123,
        36,
        129
    ],
    "2622C": [
        104,
        47,
        100
    ],
    "2563C": [
        206,
        162,
        215
    ],
    "2573C": [
        188,
        131,
        201
    ],
    "2583C": [
        163,
        93,
        180
    ],
    "2593C": [
        163,
        93,
        180
    ],
    "2603C": [
        115,
        33,
        129
    ],
    "2613C": [
        106,
        30,
        116
    ],
    "2623C": [
        98,
        31,
        101
    ],
    "2567C": [
        196,
        159,
        216
    ],
    "2577C": [
        170,
        123,
        201
    ],
    "2587C": [
        132,
        70,
        172
    ],
    "2597C": [
        93,
        12,
        139
    ],
    "2607C": [
        82,
        9,
        117
    ],
    "2617C": [
        73,
        12,
        102
    ],
    "2627C": [
        62,
        16,
        81
    ],
    "263C": [
        218,
        196,
        229
    ],
    "264C": [
        196,
        166,
        225
    ],
    "265C": [
        147,
        99,
        204
    ],
    "266C": [
        120,
        60,
        189
    ],
    "267C": [
        98,
        37,
        157
    ],
    "268C": [
        90,
        43,
        129
    ],
    "269C": [
        83,
        44,
        108
    ],
    "2635C": [
        198,
        179,
        225
    ],
    "2645C": [
        176,
        149,
        218
    ],
    "2655C": [
        150,
        120,
        210
    ],
    "2665C": [
        127,
        86,
        197
    ],
    "Violet C": [
        67,
        0,
        152
    ],
    "2685C": [
        50,
        0,
        110
    ],
    "2695C": [
        47,
        26,
        69
    ],
    "270C": [
        182,
        179,
        223
    ],
    "271C": [
        151,
        148,
        210
    ],
    "272C": [
        117,
        115,
        192
    ],
    "273C": [
        35,
        19,
        94
    ],
    "274C": [
        33,
        22,
        80
    ],
    "275C": [
        33,
        22,
        70
    ],
    "276C": [
        35,
        28,
        52
    ],
    "2705C": [
        169,
        163,
        223
    ],
    "2715C": [
        139,
        131,
        214
    ],
    "2725C": [
        103,
        93,
        198
    ],
    "2735C": [
        41,
        0,
        136
    ],
    "2745C": [
        37,
        6,
        112
    ],
    "2755C": [
        34,
        16,
        96
    ],
    "2765C": [
        32,
        21,
        69
    ],
    "2706C": [
        204,
        210,
        233
    ],
    "2716C": [
        159,
        173,
        228
    ],
    "2726C": [
        69,
        92,
        199
    ],
    "2736C": [
        23,
        36,
        169
    ],
    "2746C": [
        16,
        30,
        142
    ],
    "2756C": [
        18,
        31,
        107
    ],
    "2766C": [
        19,
        27,
        77
    ],
    "2707C": [
        196,
        213,
        236
    ],
    "2717C": [
        167,
        197,
        237
    ],
    "2727C": [
        46,
        125,
        225
    ],
    "Blue 072C": [
        5,
        13,
        158
    ],
    "2747C": [
        0,
        26,
        113
    ],
    "2757C": [
        0,
        30,
        95
    ],
    "2767C": [
        20,
        40,
        75
    ],
    "2708C": [
        185,
        203,
        234
    ],
    "2718C": [
        90,
        135,
        217
    ],
    "2728C": [
        0,
        71,
        186
    ],
    "2738C": [
        0,
        9,
        139
    ],
    "2748C": [
        0,
        25,
        112
    ],
    "2758C": [
        0,
        29,
        96
    ],
    "2768C": [
        5,
        28,
        72
    ],
    "277C": [
        173,
        200,
        231
    ],
    "278C": [
        140,
        183,
        232
    ],
    "279C": [
        62,
        141,
        221
    ],
    "Reflex Blue C": [
        0,
        21,
        136
    ],
    "280C": [
        1,
        33,
        104
    ],
    "281C": [
        0,
        31,
        91
    ],
    "282C": [
        3,
        29,
        64
    ],
    "684C": [
        229,
        197,
        211
    ],
    "685C": [
        222,
        181,
        200
    ],
    "686C": [
        211,
        159,
        183
    ],
    "687C": [
        193,
        132,
        163
    ],
    "688C": [
        171,
        99,
        136
    ],
    "689C": [
        140,
        59,
        102
    ],
    "690C": [
        100,
        34,
        63
    ],
    "691C": [
        236,
        204,
        206
    ],
    "692C": [
        230,
        190,
        194
    ],
    "693C": [
        219,
        162,
        170
    ],
    "694C": [
        199,
        132,
        143
    ],
    "695C": [
        184,
        107,
        120
    ],
    "696C": [
        155,
        71,
        85
    ],
    "697C": [
        140,
        59,
        68
    ],
    "698C": [
        245,
        211,
        214
    ],
    "699C": [
        247,
        194,
        202
    ],
    "700C": [
        247,
        172,
        184
    ],
    "701C": [
        234,
        133,
        151
    ],
    "702C": [
        215,
        92,
        113
    ],
    "703C": [
        189,
        58,
        73
    ],
    "704C": [
        162,
        41,
        46
    ],
    "705C": [
        248,
        218,
        223
    ],
    "706C": [
        248,
        205,
        214
    ],
    "707C": [
        251,
        181,
        195
    ],
    "708C": [
        252,
        145,
        162
    ],
    "709C": [
        244,
        97,
        119
    ],
    "710C": [
        231,
        61,
        80
    ],
    "711C": [
        209,
        42,
        46
    ],
    "712C": [
        255,
        201,
        153
    ],
    "713C": [
        255,
        190,
        132
    ],
    "714C": [
        255,
        169,
        96
    ],
    "715C": [
        249,
        141,
        41
    ],
    "716C": [
        237,
        119,
        0
    ],
    "717C": [
        215,
        94,
        0
    ],
    "718C": [
        193,
        77,
        0
    ],
    "719C": [
        238,
        199,
        161
    ],
    "720C": [
        233,
        183,
        137
    ],
    "721C": [
        223,
        163,
        108
    ],
    "722C": [
        203,
        130,
        65
    ],
    "723C": [
        180,
        106,
        31
    ],
    "724C": [
        149,
        78,
        14
    ],
    "725C": [
        127,
        64,
        20
    ],
    "726C": [
        226,
        193,
        157
    ],
    "727C": [
        220,
        179,
        140
    ],
    "728C": [
        207,
        160,
        117
    ],
    "729C": [
        183,
        129,
        79
    ],
    "730C": [
        160,
        102,
        44
    ],
    "731C": [
        121,
        66,
        15
    ],
    "732C": [
        100,
        52,
        15
    ],
    "7401C": [
        248,
        224,
        164
    ],
    "7402C": [
        238,
        215,
        151
    ],
    "7403C": [
        241,
        210,
        130
    ],
    "7404C": [
        247,
        217,
        61
    ],
    "7405C": [
        243,
        205,
        0
    ],
    "7406C": [
        244,
        195,
        0
    ],
    "7407C": [
        206,
        159,
        81
    ],
    "7408C": [
        249,
        190,
        0
    ],
    "7409C": [
        246,
        178,
        33
    ],
    "7410C": [
        255,
        174,
        117
    ],
    "7411C": [
        235,
        165,
        93
    ],
    "7412C": [
        214,
        130,
        48
    ],
    "7413C": [
        225,
        132,
        49
    ],
    "7414C": [
        196,
        108,
        19
    ],
    "7415C": [
        234,
        184,
        166
    ],
    "7416C": [
        235,
        104,
        81
    ],
    "7417C": [
        232,
        74,
        55
    ],
    "7418C": [
        209,
        84,
        88
    ],
    "7419C": [
        178,
        75,
        88
    ],
    "7420C": [
        160,
        32,
        64
    ],
    "7421C": [
        106,
        26,
        49
    ],
    "7422C": [
        246,
        205,
        211
    ],
    "7423C": [
        228,
        98,
        133
    ],
    "7424C": [
        231,
        69,
        130
    ],
    "7425C": [
        186,
        34,
        83
    ],
    "7426C": [
        168,
        15,
        61
    ],
    "7427C": [
        157,
        22,
        46
    ],
    "7428C": [
        109,
        42,
        60
    ],
    "7429C": [
        229,
        186,
        201
    ],
    "7430C": [
        224,
        168,
        190
    ],
    "7431C": [
        206,
        126,
        156
    ],
    "7432C": [
        187,
        90,
        127
    ],
    "7433C": [
        173,
        56,
        99
    ],
    "7434C": [
        159,
        48,
        88
    ],
    "7435C": [
        138,
        36,
        78
    ],
    "7436C": [
        242,
        218,
        233
    ],
    "7437C": [
        207,
        172,
        206
    ],
    "7438C": [
        216,
        157,
        215
    ],
    "7439C": [
        182,
        135,
        184
    ],
    "7440C": [
        167,
        118,
        165
    ],
    "7441C": [
        162,
        92,
        191
    ],
    "7442C": [
        154,
        60,
        187
    ],
    "7443C": [
        222,
        218,
        232
    ],
    "7444C": [
        183,
        184,
        220
    ],
    "7445C": [
        168,
        161,
        193
    ],
    "7446C": [
        140,
        133,
        201
    ],
    "7447C": [
        96,
        71,
        117
    ],
    "7448C": [
        76,
        55,
        75
    ],
    "7449C": [
        67,
        38,
        58
    ],
    "7450C": [
        190,
        195,
        217
    ],
    "7451C": [
        137,
        169,
        226
    ],
    "7452C": [
        129,
        147,
        220
    ],
    "7453C": [
        124,
        164,
        221
    ],
    "7454C": [
        97,
        141,
        180
    ],
    "7455C": [
        58,
        92,
        172
    ],
    "7456C": [
        96,
        109,
        178
    ],
    "7457C": [
        187,
        219,
        229
    ],
    "7458C": [
        113,
        177,
        200
    ],
    "7459C": [
        65,
        150,
        180
    ],
    "7460C": [
        0,
        131,
        193
    ],
    "7461C": [
        0,
        124,
        186
    ],
    "7462C": [
        0,
        83,
        138
    ],
    "7463C": [
        0,
        42,
        72
    ],
    "7464C": [
        161,
        207,
        202
    ],
    "7465C": [
        62,
        191,
        172
    ],
    "7466C": [
        0,
        173,
        187
    ],
    "7467C": [
        0,
        160,
        174
    ],
    "7468C": [
        0,
        114,
        152
    ],
    "7469C": [
        0,
        93,
        133
    ],
    "7470C": [
        0,
        87,
        111
    ],
    "7471C": [
        125,
        219,
        211
    ],
    "7472C": [
        90,
        182,
        178
    ],
    "7473C": [
        30,
        152,
        138
    ],
    "7474C": [
        0,
        116,
        128
    ],
    "7475C": [
        70,
        121,
        122
    ],
    "7476C": [
        7,
        80,
        86
    ],
    "7477C": [
        38,
        75,
        89
    ],
    "7478C": [
        151,
        226,
        190
    ],
    "7479C": [
        5,
        206,
        123
    ],
    "7480C": [
        0,
        188,
        111
    ],
    "7481C": [
        0,
        180,
        79
    ],
    "7482C": [
        0,
        157,
        78
    ],
    "7483C": [
        35,
        92,
        55
    ],
    "7484C": [
        0,
        86,
        63
    ],
    "7485C": [
        209,
        221,
        186
    ],
    "7486C": [
        188,
        223,
        146
    ],
    "7487C": [
        139,
        220,
        100
    ],
    "7488C": [
        114,
        213,
        74
    ],
    "7489C": [
        114,
        168,
        79
    ],
    "7490C": [
        115,
        152,
        73
    ],
    "7491C": [
        121,
        132,
        59
    ],
    "7492C": [
        194,
        202,
        127
    ],
    "7493C": [
        189,
        195,
        145
    ],
    "7494C": [
        156,
        174,
        135
    ],
    "7495C": [
        144,
        153,
        61
    ],
    "7496C": [
        119,
        135,
        28
    ],
    "7497C": [
        123,
        113,
        86
    ],
    "7498C": [
        92,
        97,
        52
    ],
    "7499C": [
        242,
        228,
        177
    ],
    "7500C": [
        225,
        208,
        165
    ],
    "7501C": [
        218,
        199,
        157
    ],
    "7502C": [
        209,
        183,
        134
    ],
    "7503C": [
        171,
        151,
        102
    ],
    "7504C": [
        150,
        120,
        93
    ],
    "7505C": [
        131,
        96,
        63
    ],
    "7506C": [
        243,
        218,
        177
    ],
    "7507C": [
        254,
        209,
        152
    ],
    "7508C": [
        226,
        183,
        125
    ],
    "7509C": [
        216,
        164,
        96
    ],
    "7510C": [
        200,
        137,
        60
    ],
    "7511C": [
        186,
        119,
        38
    ],
    "7512C": [
        168,
        99,
        24
    ],
    "7513C": [
        227,
        182,
        164
    ],
    "7514C": [
        215,
        162,
        132
    ],
    "7515C": [
        199,
        139,
        102
    ],
    "7516C": [
        156,
        84,
        42
    ],
    "7517C": [
        137,
        66,
        28
    ],
    "7518C": [
        111,
        78,
        70
    ],
    "7519C": [
        95,
        74,
        59
    ],
    "7520C": [
        237,
        190,
        175
    ],
    "7521C": [
        196,
        154,
        129
    ],
    "7522C": [
        186,
        105,
        84
    ],
    "7523C": [
        175,
        92,
        86
    ],
    "7524C": [
        168,
        81,
        71
    ],
    "7525C": [
        159,
        104,
        79
    ],
    "7526C": [
        141,
        58,
        24
    ],
    "283C": [
        148,
        192,
        233
    ],
    "284C": [
        108,
        170,
        228
    ],
    "285C": [
        0,
        112,
        205
    ],
    "286C": [
        0,
        50,
        160
    ],
    "287C": [
        0,
        47,
        134
    ],
    "288C": [
        0,
        44,
        115
    ],
    "289C": [
        12,
        34,
        63
    ],
    "290C": [
        186,
        216,
        234
    ],
    "291C": [
        153,
        202,
        234
    ],
    "292C": [
        103,
        178,
        231
    ],
    "293C": [
        0,
        60,
        166
    ],
    "294C": [
        0,
        46,
        108
    ],
    "295C": [
        0,
        40,
        85
    ],
    "296C": [
        6,
        27,
        43
    ],
    "2905C": [
        143,
        198,
        232
    ],
    "2915C": [
        97,
        179,
        228
    ],
    "2925C": [
        0,
        154,
        221
    ],
    "2935C": [
        0,
        85,
        183
    ],
    "2945C": [
        0,
        74,
        151
    ],
    "2955C": [
        0,
        55,
        99
    ],
    "2965C": [
        0,
        37,
        61
    ],
    "297C": [
        110,
        196,
        232
    ],
    "298C": [
        64,
        180,
        229
    ],
    "299C": [
        0,
        160,
        223
    ],
    "300C": [
        0,
        92,
        184
    ],
    "301C": [
        0,
        73,
        135
    ],
    "302C": [
        0,
        58,
        92
    ],
    "303C": [
        1,
        41,
        57
    ],
    "2975C": [
        151,
        212,
        233
    ],
    "2985C": [
        87,
        192,
        232
    ],
    "2995C": [
        0,
        167,
        225
    ],
    "3005C": [
        0,
        116,
        200
    ],
    "3015C": [
        0,
        95,
        155
    ],
    "3025C": [
        0,
        77,
        113
    ],
    "3035C": [
        0,
        61,
        81
    ],
    "304C": [
        152,
        218,
        233
    ],
    "305C": [
        87,
        200,
        231
    ],
    "306C": [
        0,
        178,
        226
    ],
    "Process Blue C": [
        0,
        129,
        201
    ],
    "307C": [
        0,
        105,
        166
    ],
    "308C": [
        0,
        86,
        124
    ],
    "309C": [
        0,
        58,
        73
    ],
    "310C": [
        102,
        207,
        227
    ],
    "311C": [
        0,
        193,
        222
    ],
    "312C": [
        0,
        166,
        206
    ],
    "313C": [
        0,
        143,
        190
    ],
    "314C": [
        0,
        125,
        164
    ],
    "315C": [
        0,
        101,
        127
    ],
    "316C": [
        0,
        70,
        79
    ],
    "3105C": [
        98,
        208,
        223
    ],
    "3115C": [
        0,
        191,
        213
    ],
    "3125C": [
        0,
        171,
        199
    ],
    "3135C": [
        0,
        138,
        171
    ],
    "3145C": [
        0,
        116,
        140
    ],
    "3155C": [
        0,
        95,
        113
    ],
    "3165C": [
        0,
        77,
        88
    ],
    "317C": [
        176,
        226,
        226
    ],
    "318C": [
        134,
        218,
        222
    ],
    "319C": [
        37,
        202,
        211
    ],
    "320C": [
        0,
        153,
        168
    ],
    "321C": [
        0,
        137,
        149
    ],
    "322C": [
        0,
        112,
        120
    ],
    "323C": [
        0,
        93,
        98
    ],
    "324C": [
        155,
        217,
        217
    ],
    "325C": [
        100,
        203,
        201
    ],
    "326C": [
        0,
        175,
        169
    ],
    "327C": [
        0,
        131,
        116
    ],
    "328C": [
        0,
        113,
        102
    ],
    "329C": [
        0,
        102,
        94
    ],
    "330C": [
        0,
        81,
        75
    ],
    "3242C": [
        110,
        218,
        213
    ],
    "3252C": [
        27,
        207,
        201
    ],
    "3262C": [
        0,
        187,
        179
    ],
    "3272C": [
        0,
        161,
        154
    ],
    "3282C": [
        0,
        131,
        122
    ],
    "3292C": [
        0,
        87,
        79
    ],
    "3302C": [
        0,
        75,
        68
    ],
    "490C": [
        94,
        41,
        42
    ],
    "491C": [
        129,
        48,
        51
    ],
    "492C": [
        147,
        50,
        53
    ],
    "493C": [
        222,
        135,
        151
    ],
    "494C": [
        236,
        162,
        176
    ],
    "495C": [
        244,
        189,
        198
    ],
    "496C": [
        244,
        198,
        206
    ],
    "497C": [
        82,
        48,
        44
    ],
    "498C": [
        106,
        56,
        51
    ],
    "499C": [
        122,
        63,
        56
    ],
    "500C": [
        200,
        134,
        140
    ],
    "501C": [
        220,
        165,
        171
    ],
    "502C": [
        230,
        187,
        192
    ],
    "503C": [
        235,
        196,
        198
    ],
    "4975C": [
        65,
        31,
        31
    ],
    "4985C": [
        137,
        75,
        81
    ],
    "4995C": [
        157,
        97,
        103
    ],
    "5005C": [
        178,
        123,
        128
    ],
    "5015C": [
        205,
        161,
        165
    ],
    "5025C": [
        221,
        183,
        187
    ],
    "5035C": [
        225,
        194,
        193
    ],
    "504C": [
        87,
        41,
        48
    ],
    "505C": [
        112,
        45,
        61
    ],
    "506C": [
        135,
        53,
        76
    ],
    "507C": [
        216,
        145,
        168
    ],
    "508C": [
        230,
        169,
        184
    ],
    "509C": [
        235,
        179,
        195
    ],
    "510C": [
        237,
        190,
        200
    ],
    "511C": [
        99,
        44,
        79
    ],
    "512C": [
        133,
        49,
        117
    ],
    "513C": [
        148,
        52,
        140
    ],
    "514C": [
        214,
        139,
        198
    ],
    "515C": [
        228,
        172,
        213
    ],
    "516C": [
        233,
        190,
        219
    ],
    "517C": [
        237,
        197,
        222
    ],
    "5115C": [
        83,
        41,
        67
    ],
    "5125C": [
        109,
        58,
        93
    ],
    "5135C": [
        129,
        83,
        116
    ],
    "5145C": [
        158,
        118,
        145
    ],
    "5155C": [
        193,
        164,
        183
    ],
    "5165C": [
        212,
        191,
        204
    ],
    "5175C": [
        218,
        198,
        206
    ],
    "518C": [
        77,
        46,
        71
    ],
    "519C": [
        91,
        48,
        93
    ],
    "520C": [
        102,
        46,
        107
    ],
    "521C": [
        168,
        126,
        177
    ],
    "522C": [
        189,
        155,
        196
    ],
    "523C": [
        204,
        176,
        207
    ],
    "524C": [
        215,
        194,
        214
    ],
    "5185C": [
        76,
        48,
        64
    ],
    "5195C": [
        104,
        68,
        88
    ],
    "5205C": [
        136,
        99,
        120
    ],
    "5215C": [
        176,
        149,
        165
    ],
    "5225C": [
        200,
        175,
        187
    ],
    "5235C": [
        210,
        189,
        198
    ],
    "5245C": [
        221,
        205,
        210
    ],
    "525C": [
        89,
        43,
        94
    ],
    "526C": [
        116,
        47,
        138
    ],
    "527C": [
        132,
        48,
        166
    ],
    "528C": [
        186,
        128,
        208
    ],
    "529C": [
        206,
        161,
        220
    ],
    "530C": [
        218,
        183,
        226
    ],
    "531C": [
        226,
        199,
        230
    ],
    "5255C": [
        30,
        25,
        51
    ],
    "5265C": [
        64,
        57,
        95
    ],
    "5275C": [
        90,
        83,
        119
    ],
    "5285C": [
        142,
        136,
        163
    ],
    "5295C": [
        180,
        175,
        195
    ],
    "5305C": [
        199,
        195,
        209
    ],
    "5315C": [
        218,
        214,
        222
    ],
    "532C": [
        28,
        30,
        41
    ],
    "533C": [
        33,
        40,
        68
    ],
    "534C": [
        30,
        52,
        93
    ],
    "535C": [
        142,
        158,
        188
    ],
    "536C": [
        162,
        177,
        200
    ],
    "537C": [
        187,
        198,
        214
    ],
    "538C": [
        198,
        207,
        218
    ],
    "539C": [
        0,
        37,
        57
    ],
    "540C": [
        0,
        47,
        86
    ],
    "541C": [
        0,
        59,
        112
    ],
    "542C": [
        124,
        173,
        211
    ],
    "543C": [
        164,
        199,
        226
    ],
    "544C": [
        191,
        212,
        230
    ],
    "545C": [
        199,
        217,
        230
    ],
    "5395C": [
        10,
        30,
        44
    ],
    "5405C": [
        79,
        115,
        138
    ],
    "5415C": [
        93,
        126,
        149
    ],
    "5425C": [
        124,
        151,
        171
    ],
    "5435C": [
        167,
        186,
        200
    ],
    "5445C": [
        184,
        199,
        211
    ],
    "5455C": [
        191,
        205,
        212
    ],
    "546C": [
        8,
        41,
        47
    ],
    "547C": [
        0,
        47,
        59
    ],
    "548C": [
        0,
        59,
        76
    ],
    "549C": [
        106,
        162,
        184
    ],
    "550C": [
        141,
        183,
        201
    ],
    "551C": [
        164,
        197,
        210
    ],
    "552C": [
        187,
        210,
        219
    ],
    "5463C": [
        8,
        38,
        44
    ],
    "5473C": [
        9,
        92,
        102
    ],
    "5483C": [
        77,
        133,
        141
    ],
    "5493C": [
        126,
        167,
        172
    ],
    "5503C": [
        147,
        182,
        187
    ],
    "5513C": [
        172,
        198,
        200
    ],
    "5523C": [
        183,
        206,
        207
    ],
    "5467C": [
        26,
        50,
        46
    ],
    "5477C": [
        63,
        91,
        87
    ],
    "5487C": [
        95,
        119,
        116
    ],
    "5497C": [
        132,
        151,
        148
    ],
    "5507C": [
        157,
        174,
        171
    ],
    "5517C": [
        178,
        191,
        187
    ],
    "5527C": [
        188,
        199,
        196
    ],
    "553C": [
        42,
        70,
        51
    ],
    "554C": [
        34,
        90,
        64
    ],
    "555C": [
        44,
        112,
        79
    ],
    "556C": [
        112,
        160,
        135
    ],
    "557C": [
        135,
        175,
        153
    ],
    "558C": [
        155,
        189,
        170
    ],
    "559C": [
        175,
        201,
        184
    ],
    "5535C": [
        23,
        47,
        40
    ],
    "5545C": [
        67,
        104,
        90
    ],
    "5555C": [
        91,
        127,
        112
    ],
    "5565C": [
        126,
        156,
        144
    ],
    "5575C": [
        146,
        171,
        160
    ],
    "5585C": [
        166,
        188,
        176
    ],
    "5595C": [
        190,
        205,
        194
    ],
    "560C": [
        27,
        60,
        51
    ],
    "561C": [
        0,
        88,
        77
    ],
    "562C": [
        0,
        110,
        98
    ],
    "563C": [
        103,
        186,
        175
    ],
    "564C": [
        130,
        199,
        188
    ],
    "565C": [
        159,
        212,
        201
    ],
    "566C": [
        183,
        219,
        209
    ],
    "5605C": [
        34,
        54,
        42
    ],
    "5615C": [
        94,
        114,
        96
    ],
    "5625C": [
        114,
        132,
        114
    ],
    "5635C": [
        149,
        164,
        149
    ],
    "5645C": [
        164,
        177,
        163
    ],
    "5655C": [
        178,
        188,
        175
    ],
    "5665C": [
        188,
        196,
        185
    ],
    "567C": [
        22,
        61,
        52
    ],
    "568C": [
        0,
        106,
        90
    ],
    "569C": [
        0,
        127,
        109
    ],
    "570C": [
        105,
        201,
        185
    ],
    "571C": [
        148,
        218,
        206
    ],
    "572C": [
        162,
        222,
        210
    ],
    "573C": [
        180,
        225,
        216
    ],
    "574C": [
        78,
        90,
        48
    ],
    "575C": [
        105,
        129,
        58
    ],
    "576C": [
        122,
        155,
        73
    ],
    "577C": [
        171,
        194,
        126
    ],
    "578C": [
        184,
        205,
        149
    ],
    "579C": [
        191,
        207,
        154
    ],
    "580C": [
        197,
        212,
        163
    ],
    "5743C": [
        64,
        71,
        38
    ],
    "5753C": [
        96,
        102,
        55
    ],
    "5763C": [
        118,
        122,
        76
    ],
    "5773C": [
        138,
        143,
        100
    ],
    "5783C": [
        164,
        169,
        130
    ],
    "5793C": [
        180,
        183,
        148
    ],
    "5803C": [
        196,
        198,
        166
    ],
    "7527C": [
        215,
        209,
        196
    ],
    "7528C": [
        199,
        185,
        171
    ],
    "7529C": [
        183,
        169,
        152
    ],
    "7530C": [
        164,
        147,
        130
    ],
    "7531C": [
        124,
        103,
        85
    ],
    "7532C": [
        101,
        80,
        60
    ],
    "7533C": [
        72,
        54,
        39
    ],
    "7534C": [
        211,
        204,
        188
    ],
    "7535C": [
        184,
        176,
        155
    ],
    "7536C": [
        168,
        159,
        135
    ],
    "7537C": [
        169,
        171,
        160
    ],
    "7538C": [
        147,
        153,
        143
    ],
    "7539C": [
        143,
        142,
        136
    ],
    "7540C": [
        75,
        78,
        83
    ],
    "7541C": [
        218,
        223,
        225
    ],
    "7542C": [
        165,
        187,
        194
    ],
    "7543C": [
        152,
        163,
        173
    ],
    "7544C": [
        118,
        133,
        145
    ],
    "7545C": [
        66,
        83,
        99
    ],
    "7546C": [
        38,
        54,
        69
    ],
    "7547C": [
        20,
        29,
        40
    ],
    "100C": [
        247,
        234,
        95
    ],
    "101C": [
        248,
        233,
        70
    ],
    "102C": [
        253,
        227,
        0
    ],
    "Yellow C": [
        255,
        221,
        0
    ],
    "103C": [
        198,
        169,
        0
    ],
    "104C": [
        177,
        151,
        0
    ],
    "105C": [
        139,
        122,
        39
    ],
    "106C": [
        251,
        227,
        67
    ],
    "107C": [
        254,
        224,
        30
    ],
    "108C": [
        255,
        219,
        0
    ],
    "109C": [
        255,
        209,
        0
    ],
    "110C": [
        219,
        169,
        0
    ],
    "111C": [
        172,
        137,
        0
    ],
    "112C": [
        157,
        131,
        15
    ],
    "113C": [
        253,
        223,
        80
    ],
    "114C": [
        254,
        221,
        60
    ],
    "115C": [
        255,
        217,
        35
    ],
    "116C": [
        255,
        205,
        0
    ],
    "117C": [
        204,
        151,
        0
    ],
    "118C": [
        174,
        132,
        0
    ],
    "119C": [
        138,
        115,
        31
    ],
    "120C": [
        254,
        218,
        99
    ],
    "121C": [
        255,
        215,
        85
    ],
    "122C": [
        255,
        208,
        62
    ],
    "123C": [
        255,
        198,
        39
    ],
    "124C": [
        237,
        169,
        0
    ],
    "125C": [
        183,
        132,
        0
    ],
    "126C": [
        157,
        118,
        12
    ],
    "1205C": [
        249,
        223,
        141
    ],
    "1215C": [
        254,
        214,
        113
    ],
    "1225C": [
        255,
        200,
        66
    ],
    "1235C": [
        255,
        183,
        24
    ],
    "1245C": [
        201,
        145,
        13
    ],
    "1255C": [
        175,
        132,
        29
    ],
    "1265C": [
        138,
        107,
        36
    ],
    "127C": [
        245,
        219,
        107
    ],
    "128C": [
        247,
        212,
        75
    ],
    "129C": [
        246,
        206,
        60
    ],
    "130C": [
        247,
        167,
        0
    ],
    "131C": [
        208,
        137,
        0
    ],
    "132C": [
        163,
        115,
        0
    ],
    "133C": [
        110,
        86,
        27
    ],
    "134C": [
        255,
        209,
        108
    ],
    "135C": [
        255,
        197,
        86
    ],
    "136C": [
        255,
        191,
        59
    ],
    "137C": [
        255,
        163,
        0
    ],
    "138C": [
        225,
        124,
        0
    ],
    "139C": [
        178,
        109,
        0
    ],
    "140C": [
        117,
        83,
        25
    ],
    "1345C": [
        254,
        207,
        132
    ],
    "1355C": [
        255,
        197,
        108
    ],
    "1365C": [
        255,
        181,
        70
    ],
    "1375C": [
        255,
        158,
        21
    ],
    "1385C": [
        216,
        120,
        0
    ],
    "1395C": [
        156,
        95,
        21
    ],
    "1405C": [
        112,
        75,
        28
    ],
    "141C": [
        246,
        197,
        91
    ],
    "142C": [
        245,
        189,
        71
    ],
    "143C": [
        246,
        179,
        51
    ],
    "144C": [
        243,
        138,
        0
    ],
    "145C": [
        213,
        126,
        0
    ],
    "146C": [
        171,
        107,
        14
    ],
    "147C": [
        115,
        91,
        40
    ],
    "148C": [
        255,
        203,
        138
    ],
    "149C": [
        255,
        194,
        122
    ],
    "150C": [
        255,
        178,
        88
    ],
    "151C": [
        255,
        131,
        0
    ],
    "152C": [
        233,
        114,
        0
    ],
    "153C": [
        193,
        106,
        16
    ],
    "154C": [
        158,
        89,
        22
    ],
    "1485C": [
        255,
        173,
        94
    ],
    "1495C": [
        255,
        143,
        18
    ],
    "1505C": [
        255,
        106,
        0
    ],
    "Orange 021C": [
        255,
        80,
        0
    ],
    "1525C": [
        189,
        70,
        0
    ],
    "1535C": [
        151,
        69,
        8
    ],
    "1545C": [
        104,
        56,
        22
    ],
    "155C": [
        243,
        208,
        158
    ],
    "156C": [
        242,
        188,
        123
    ],
    "157C": [
        241,
        159,
        83
    ],
    "158C": [
        239,
        117,
        33
    ],
    "159C": [
        210,
        93,
        18
    ],
    "160C": [
        165,
        84,
        26
    ],
    "161C": [
        99,
        60,
        32
    ],
    "1555C": [
        255,
        185,
        141
    ],
    "1565C": [
        255,
        161,
        103
    ],
    "1575C": [
        255,
        126,
        46
    ],
    "1585C": [
        255,
        107,
        11
    ],
    "1595C": [
        220,
        95,
        19
    ],
    "1605C": [
        169,
        85,
        33
    ],
    "1615C": [
        142,
        71,
        30
    ],
    "162C": [
        255,
        190,
        158
    ],
    "163C": [
        255,
        157,
        107
    ],
    "164C": [
        255,
        127,
        63
    ],
    "165C": [
        255,
        102,
        27
    ],
    "166C": [
        231,
        82,
        0
    ],
    "167C": [
        194,
        82,
        25
    ],
    "168C": [
        117,
        56,
        27
    ],
    "1625C": [
        255,
        163,
        136
    ],
    "1635C": [
        255,
        141,
        106
    ],
    "1645C": [
        255,
        107,
        53
    ],
    "1655C": [
        255,
        76,
        0
    ],
    "1665C": [
        226,
        67,
        1
    ],
    "1675C": [
        173,
        67,
        28
    ],
    "1685C": [
        134,
        57,
        31
    ],
    "169C": [
        255,
        180,
        170
    ],
    "170C": [
        255,
        134,
        113
    ],
    "171C": [
        255,
        91,
        52
    ],
    "172C": [
        255,
        70,
        17
    ],
    "173C": [
        211,
        68,
        28
    ],
    "174C": [
        153,
        56,
        31
    ],
    "175C": [
        109,
        53,
        39
    ],
    "176C": [
        255,
        177,
        184
    ],
    "177C": [
        255,
        129,
        136
    ],
    "178C": [
        255,
        88,
        89
    ],
    "Warm Red C": [
        255,
        67,
        55
    ],
    "179C": [
        229,
        60,
        46
    ],
    "180C": [
        195,
        58,
        50
    ],
    "181C": [
        132,
        50,
        44
    ],
    "1765C": [
        255,
        164,
        179
    ],
    "1775C": [
        255,
        142,
        158
    ],
    "1785C": [
        255,
        72,
        91
    ],
    "1788C": [
        244,
        36,
        52
    ],
    "1795C": [
        216,
        38,
        46
    ],
    "1805C": [
        179,
        39,
        45
    ],
    "1815C": [
        128,
        37,
        40
    ],
    "1767C": [
        255,
        175,
        191
    ],
    "1777C": [
        255,
        99,
        125
    ],
    "1787C": [
        251,
        52,
        72
    ],
    "Red 032C": [
        246,
        50,
        62
    ],
    "1797C": [
        208,
        50,
        56
    ],
    "1807C": [
        168,
        51,
        56
    ],
    "1817C": [
        102,
        50,
        51
    ],
    "182C": [
        254,
        187,
        202
    ],
    "183C": [
        255,
        155,
        177
    ],
    "184C": [
        252,
        82,
        114
    ],
    "185C": [
        235,
        0,
        40
    ],
    "186C": [
        207,
        10,
        44
    ],
    "187C": [
        171,
        22,
        43
    ],
    "188C": [
        121,
        35,
        46
    ],
    "Cool Gray 1C": [
        218,
        216,
        214
    ],
    "Cool Gray 2C": [
        210,
        207,
        205
    ],
    "Cool Gray 3C": [
        201,
        200,
        199
    ],
    "Cool Gray 4C": [
        188,
        187,
        186
    ],
    "Cool Gray 5C": [
        179,
        177,
        177
    ],
    "Cool Gray 6C": [
        169,
        168,
        169
    ],
    "Cool Gray 7C": [
        153,
        152,
        153
    ],
    "Cool Gray 8C": [
        138,
        138,
        141
    ],
    "Cool Gray 9C": [
        119,
        119,
        121
    ],
    "Cool Gray 10C": [
        100,
        100,
        105
    ],
    "Cool Gray 11C": [
        85,
        85,
        89
    ],
    "Black 2C": [
        51,
        46,
        31
    ],
    "Black 3C": [
        33,
        38,
        32
    ],
    "Black 4C": [
        49,
        37,
        28
    ],
    "Black 5C": [
        63,
        42,
        46
    ],
    "Black 6C": [
        16,
        24,
        31
    ],
    "Black 7C": [
        61,
        56,
        52
    ],
    "448C": [
        76,
        64,
        40
    ],
    "449C": [
        85,
        69,
        37
    ],
    "450C": [
        92,
        73,
        37
    ],
    "451C": [
        159,
        145,
        93
    ],
    "452C": [
        179,
        167,
        125
    ],
    "453C": [
        195,
        186,
        152
    ],
    "454C": [
        205,
        196,
        166
    ],
    "4485C": [
        99,
        78,
        37
    ],
    "4495C": [
        142,
        118,
        48
    ],
    "4505C": [
        155,
        132,
        66
    ],
    "4515C": [
        181,
        162,
        104
    ],
    "4525C": [
        199,
        182,
        131
    ],
    "4535C": [
        209,
        195,
        146
    ],
    "4545C": [
        214,
        202,
        158
    ],
    "455C": [
        108,
        90,
        35
    ],
    "456C": [
        165,
        141,
        40
    ],
    "457C": [
        186,
        156,
        19
    ],
    "458C": [
        219,
        197,
        84
    ],
    "459C": [
        224,
        204,
        97
    ],
    "460C": [
        231,
        214,
        126
    ],
    "461C": [
        236,
        222,
        151
    ],
    "462C": [
        94,
        69,
        42
    ],
    "463C": [
        119,
        77,
        39
    ],
    "464C": [
        143,
        89,
        39
    ],
    "465C": [
        189,
        149,
        90
    ],
    "466C": [
        202,
        169,
        118
    ],
    "467C": [
        214,
        186,
        139
    ],
    "468C": [
        223,
        201,
        162
    ],
    "4625C": [
        81,
        43,
        28
    ],
    "4635C": [
        151,
        95,
        53
    ],
    "4645C": [
        176,
        124,
        87
    ],
    "4655C": [
        193,
        147,
        114
    ],
    "4665C": [
        206,
        167,
        135
    ],
    "4675C": [
        222,
        191,
        164
    ],
    "4685C": [
        225,
        197,
        172
    ],
    "469C": [
        107,
        63,
        34
    ],
    "470C": [
        167,
        89,
        40
    ],
    "471C": [
        188,
        97,
        35
    ],
    "472C": [
        231,
        157,
        108
    ],
    "473C": [
        245,
        190,
        153
    ],
    "474C": [
        244,
        198,
        166
    ],
    "475C": [
        244,
        207,
        178
    ],
    "4695C": [
        93,
        51,
        38
    ],
    "4705C": [
        129,
        76,
        58
    ],
    "4715C": [
        153,
        106,
        87
    ],
    "4725C": [
        78,
        136,
        119
    ],
    "4735C": [
        195,
        162,
        145
    ],
    "4745C": [
        207,
        180,
        167
    ],
    "4755C": [
        218,
        194,
        182
    ],
    "476C": [
        81,
        53,
        40
    ],
    "477C": [
        102,
        58,
        42
    ],
    "478C": [
        117,
        61,
        41
    ],
    "479C": [
        174,
        127,
        101
    ],
    "480C": [
        201,
        167,
        145
    ],
    "481C": [
        214,
        185,
        167
    ],
    "482C": [
        222,
        199,
        182
    ],
    "483C": [
        104,
        47,
        36
    ],
    "484C": [
        159,
        49,
        34
    ],
    "485C": [
        226,
        35,
        26
    ],
    "486C": [
        239,
        143,
        122
    ],
    "487C": [
        240,
        166,
        147
    ],
    "488C": [
        241,
        184,
        168
    ],
    "489C": [
        240,
        194,
        178
    ],
    "3245C": [
        122,
        222,
        212
    ],
    "3255C": [
        34,
        211,
        197
    ],
    "3265C": [
        0,
        196,
        179
    ],
    "3275C": [
        0,
        175,
        154
    ],
    "3285C": [
        0,
        147,
        130
    ],
    "3295C": [
        0,
        117,
        100
    ],
    "3305C": [
        0,
        76,
        66
    ],
    "3248C": [
        109,
        203,
        184
    ],
    "3258C": [
        72,
        195,
        177
    ],
    "3268C": [
        0,
        169,
        142
    ],
    "3278C": [
        0,
        152,
        119
    ],
    "3288C": [
        0,
        128,
        100
    ],
    "3298C": [
        0,
        104,
        82
    ],
    "3308C": [
        3,
        68,
        54
    ],
    "331C": [
        166,
        229,
        216
    ],
    "332C": [
        137,
        225,
        208
    ],
    "333C": [
        51,
        217,
        194
    ],
    "Green C": [
        0,
        168,
        135
    ],
    "334C": [
        0,
        149,
        120
    ],
    "335C": [
        0,
        121,
        95
    ],
    "336C": [
        0,
        99,
        79
    ],
    "337C": [
        143,
        212,
        188
    ],
    "338C": [
        109,
        205,
        177
    ],
    "339C": [
        0,
        177,
        136
    ],
    "340C": [
        0,
        148,
        93
    ],
    "341C": [
        0,
        120,
        82
    ],
    "342C": [
        0,
        101,
        71
    ],
    "343C": [
        13,
        85,
        63
    ],
    "3375C": [
        118,
        224,
        192
    ],
    "3385C": [
        63,
        212,
        173
    ],
    "3395C": [
        0,
        192,
        138
    ],
    "3405C": [
        0,
        172,
        104
    ],
    "3415C": [
        0,
        117,
        73
    ],
    "3425C": [
        0,
        97,
        64
    ],
    "3435C": [
        20,
        70,
        51
    ],
    "344C": [
        160,
        216,
        179
    ],
    "345C": [
        145,
        213,
        171
    ],
    "346C": [
        110,
        202,
        151
    ],
    "347C": [
        0,
        152,
        68
    ],
    "348C": [
        0,
        131,
        61
    ],
    "349C": [
        1,
        104,
        54
    ],
    "350C": [
        43,
        80,
        52
    ],
    "351C": [
        162,
        227,
        185
    ],
    "352C": [
        140,
        224,
        176
    ],
    "353C": [
        123,
        222,
        167
    ],
    "354C": [
        0,
        174,
        65
    ],
    "355C": [
        0,
        149,
        58
    ],
    "356C": [
        0,
        120,
        51
    ],
    "357C": [
        28,
        86,
        49
    ],
    "358C": [
        172,
        218,
        144
    ],
    "359C": [
        162,
        214,
        131
    ],
    "360C": [
        108,
        192,
        73
    ],
    "361C": [
        63,
        174,
        41
    ],
    "362C": [
        77,
        156,
        45
    ],
    "363C": [
        75,
        139,
        42
    ],
    "364C": [
        73,
        117,
        39
    ],
    "365C": [
        194,
        223,
        135
    ],
    "366C": [
        183,
        220,
        120
    ],
    "367C": [
        163,
        213,
        93
    ],
    "368C": [
        119,
        188,
        31
    ],
    "369C": [
        98,
        166,
        10
    ],
    "370C": [
        100,
        139,
        26
    ],
    "371C": [
        84,
        97,
        33
    ],
    "372C": [
        212,
        235,
        141
    ],
    "373C": [
        204,
        233,
        127
    ],
    "374C": [
        196,
        231,
        106
    ],
    "375C": [
        148,
        213,
        0
    ],
    "376C": [
        130,
        188,
        0
    ],
    "377C": [
        121,
        153,
        0
    ],
    "378C": [
        89,
        97,
        27
    ],
    "379C": [
        226,
        230,
        101
    ],
    "380C": [
        219,
        227,
        65
    ],
    "381C": [
        206,
        219,
        0
    ],
    "382C": [
        195,
        213,
        0
    ],
    "383C": [
        170,
        173,
        0
    ],
    "384C": [
        149,
        147,
        0
    ],
    "385C": [
        121,
        112,
        31
    ],
    "386C": [
        236,
        235,
        107
    ],
    "387C": [
        227,
        231,
        51
    ],
    "388C": [
        225,
        230,
        30
    ],
    "389C": [
        206,
        222,
        0
    ],
    "390C": [
        182,
        189,
        0
    ],
    "391C": [
        154,
        148,
        0
    ],
    "392C": [
        131,
        121,
        2
    ],
    "393C": [
        241,
        236,
        114
    ],
    "394C": [
        238,
        232,
        56
    ],
    "395C": [
        236,
        232,
        19
    ],
    "396C": [
        225,
        224,
        0
    ],
    "397C": [
        192,
        184,
        0
    ],
    "398C": [
        173,
        163,
        0
    ],
    "399C": [
        162,
        145,
        0
    ],
    "3935C": [
        245,
        232,
        91
    ],
    "3945C": [
        245,
        229,
        0
    ],
    "3955C": [
        242,
        223,
        0
    ],
    "3965C": [
        238,
        220,
        0
    ],
    "3975C": [
        190,
        166,
        0
    ],
    "3985C": [
        154,
        134,
        0
    ],
    "3995C": [
        106,
        92,
        31
    ],
    "400C": [
        198,
        190,
        181
    ],
    "401C": [
        177,
        169,
        158
    ],
    "402C": [
        158,
        148,
        139
    ],
    "403C": [
        141,
        131,
        122
    ],
    "404C": [
        119,
        109,
        99
    ],
    "405C": [
        105,
        96,
        87
    ],
    "Black C": [
        46,
        41,
        37
    ],
    "406C": [
        199,
        187,
        181
    ],
    "407C": [
        178,
        167,
        161
    ],
    "408C": [
        153,
        139,
        134
    ],
    "409C": [
        135,
        120,
        115
    ],
    "410C": [
        117,
        102,
        96
    ],
    "411C": [
        96,
        80,
        76
    ],
    "412C": [
        57,
        46,
        44
    ],
    "413C": [
        187,
        185,
        175
    ],
    "414C": [
        170,
        168,
        158
    ],
    "415C": [
        146,
        146,
        135
    ],
    "416C": [
        127,
        126,
        115
    ],
    "417C": [
        102,
        102,
        92
    ],
    "418C": [
        82,
        82,
        73
    ],
    "419C": [
        33,
        34,
        33
    ],
    "420C": [
        201,
        200,
        199
    ],
    "421C": [
        178,
        179,
        178
    ],
    "422C": [
        159,
        161,
        161
    ],
    "423C": [
        138,
        139,
        140
    ],
    "424C": [
        112,
        113,
        112
    ],
    "425C": [
        84,
        87,
        89
    ],
    "426C": [
        37,
        39,
        41
    ],
    "427C": [
        208,
        210,
        211
    ],
    "428C": [
        195,
        197,
        200
    ],
    "429C": [
        163,
        169,
        172
    ],
    "430C": [
        124,
        133,
        140
    ],
    "431C": [
        92,
        102,
        111
    ],
    "432C": [
        51,
        61,
        71
    ],
    "433C": [
        30,
        36,
        43
    ],
    "434C": [
        208,
        196,
        196
    ],
    "435C": [
        192,
        178,
        181
    ],
    "436C": [
        170,
        152,
        156
    ],
    "437C": [
        123,
        101,
        104
    ],
    "438C": [
        88,
        68,
        68
    ],
    "439C": [
        68,
        54,
        52
    ],
    "440C": [
        56,
        45,
        43
    ],
    "441C": [
        189,
        198,
        195
    ],
    "442C": [
        160,
        172,
        170
    ],
    "443C": [
        144,
        156,
        156
    ],
    "444C": [
        112,
        123,
        123
    ],
    "445C": [
        78,
        87,
        88
    ],
    "446C": [
        61,
        69,
        66
    ],
    "447C": [
        55,
        57,
        53
    ],
    "Warm Gray 1C": [
        216,
        209,
        201
    ],
    "Warm Gray 2C": [
        205,
        195,
        187
    ],
    "Warm Gray 3C": [
        193,
        183,
        175
    ],
    "Warm GraY 4C": [
        182,
        173,
        164
    ],
    "Warm Gray 5C": [
        174,
        161,
        152
    ],
    "Warm Gray 6C": [
        167,
        155,
        148
    ],
    "Warm Gray 7C": [
        151,
        139,
        130
    ],
    "Warm Gray 8C": [
        142,
        130,
        121
    ],
    "Warm GraY 9C": [
        133,
        119,
        111
    ],
    "Warm Gray 10C": [
        123,
        109,
        101
    ],
    "Warm Gray 11C": [
        112,
        98,
        88
    ]
});
define("settings", ["require", "exports", "szinek", "szinnevek"], function (require, exports, inputColorRestrictions, inputColorAliases) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Settings = exports.ClusteringColorSpace = void 0;
    var ClusteringColorSpace;
    (function (ClusteringColorSpace) {
        ClusteringColorSpace[ClusteringColorSpace["RGB"] = 0] = "RGB";
        ClusteringColorSpace[ClusteringColorSpace["HSL"] = 1] = "HSL";
        ClusteringColorSpace[ClusteringColorSpace["LAB"] = 2] = "LAB";
    })(ClusteringColorSpace = exports.ClusteringColorSpace || (exports.ClusteringColorSpace = {}));
    class Settings {
        constructor() {
            this.kMeansNrOfClusters = 16;
            this.kMeansMinDeltaDifference = 1;
            this.kMeansClusteringColorSpace = ClusteringColorSpace.RGB;
            this.kMeansColorRestrictions = inputColorRestrictions;
            this.colorAliases = inputColorAliases;
            this.narrowPixelStripCleanupRuns = 3; // 3 seems like a good compromise between removing enough narrow pixel strips to convergence. This fixes e.g. https://i.imgur.com/dz4ANz1.png
            this.removeFacetsSmallerThanNrOfPoints = 20;
            this.removeFacetsFromLargeToSmall = true;
            this.maximumNumberOfFacets = Number.MAX_VALUE;
            this.nrOfTimesToHalveBorderSegments = 2;
            this.resizeImageIfTooLarge = true;
            this.resizeImageWidth = 1024;
            this.resizeImageHeight = 1024;
            this.randomSeed = new Date().getTime();
        }
    }
    exports.Settings = Settings;
});
define("structs/typedarrays", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.BooleanArray2D = exports.Uint8Array2D = exports.Uint32Array2D = void 0;
    class Uint32Array2D {
        constructor(width, height) {
            this.width = width;
            this.height = height;
            this.arr = new Uint32Array(width * height);
        }
        get(x, y) {
            return this.arr[y * this.width + x];
        }
        set(x, y, value) {
            this.arr[y * this.width + x] = value;
        }
    }
    exports.Uint32Array2D = Uint32Array2D;
    class Uint8Array2D {
        constructor(width, height) {
            this.width = width;
            this.height = height;
            this.arr = new Uint8Array(width * height);
        }
        get(x, y) {
            return this.arr[y * this.width + x];
        }
        set(x, y, value) {
            this.arr[y * this.width + x] = value;
        }
        matchAllAround(x, y, value) {
            const idx = y * this.width + x;
            return (x - 1 >= 0 && this.arr[idx - 1] === value) &&
                (y - 1 >= 0 && this.arr[idx - this.width] === value) &&
                (x + 1 < this.width && this.arr[idx + 1] === value) &&
                (y + 1 < this.height && this.arr[idx + this.width] === value);
        }
    }
    exports.Uint8Array2D = Uint8Array2D;
    class BooleanArray2D {
        constructor(width, height) {
            this.width = width;
            this.height = height;
            this.arr = new Uint8Array(width * height);
        }
        get(x, y) {
            return this.arr[y * this.width + x] !== 0;
        }
        set(x, y, value) {
            this.arr[y * this.width + x] = value ? 1 : 0;
        }
    }
    exports.BooleanArray2D = BooleanArray2D;
});
define("colorreductionmanagement", ["require", "exports", "common", "lib/clustering", "lib/colorconversion", "settings", "structs/typedarrays", "random", "szinek"], function (require, exports, common_1, clustering_1, colorconversion_1, settings_1, typedarrays_1, random_1, allowedColors) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ColorReducer = exports.ColorMapResult = void 0;
    class ColorMapResult {
    }
    exports.ColorMapResult = ColorMapResult;
    class ColorReducer {
        /**
         *  Creates a map of the various colors used
         */
        static createColorMap(kmeansImgData) {
            const imgColorIndices = new typedarrays_1.Uint8Array2D(kmeansImgData.width, kmeansImgData.height);
            let colorIndex = 0;
            const colors = {};
            const colorsByIndex = [];
            let idx = 0;
            for (let j = 0; j < kmeansImgData.height; j++) {
                for (let i = 0; i < kmeansImgData.width; i++) {
                    const r = kmeansImgData.data[idx++];
                    const g = kmeansImgData.data[idx++];
                    const b = kmeansImgData.data[idx++];
                    const a = kmeansImgData.data[idx++];
                    let currentColorIndex;
                    const color = r + "," + g + "," + b;
                    if (typeof colors[color] === "undefined") {
                        currentColorIndex = colorIndex;
                        colors[color] = colorIndex;
                        colorsByIndex.push([r, g, b]);
                        colorIndex++;
                    }
                    else {
                        currentColorIndex = colors[color];
                    }
                    imgColorIndices.set(i, j, currentColorIndex);
                }
            }
            const result = new ColorMapResult();
            result.imgColorIndices = imgColorIndices;
            result.colorsByIndex = colorsByIndex;
            result.width = kmeansImgData.width;
            result.height = kmeansImgData.height;
            return result;
        }
        /**
         *  Applies K-means clustering on the imgData to reduce the colors to
         *  k clusters and then output the result to the given outputImgData
         */
        static applyKMeansClustering(imgData, outputImgData, ctx, settings, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                const vectors = [];
                let idx = 0;
                let vIdx = 0;
                const bitsToChopOff = 2; // r,g,b gets rounded to every 4 values, 0,4,8,...
                // group by color, add points as 1D index to prevent Point object allocation
                const pointsByColor = {};
                for (let j = 0; j < imgData.height; j++) {
                    for (let i = 0; i < imgData.width; i++) {
                        let r = imgData.data[idx++];
                        let g = imgData.data[idx++];
                        let b = imgData.data[idx++];
                        const a = imgData.data[idx++];
                        // small performance boost: reduce bitness of colors by chopping off the last bits
                        // this will group more colors with only slight variation in color together, reducing the size of the points
                        r = r >> bitsToChopOff << bitsToChopOff;
                        g = g >> bitsToChopOff << bitsToChopOff;
                        b = b >> bitsToChopOff << bitsToChopOff;
                        const color = `${r},${g},${b}`;
                        if (!(color in pointsByColor)) {
                            pointsByColor[color] = [j * imgData.width + i];
                        }
                        else {
                            pointsByColor[color].push(j * imgData.width + i);
                        }
                    }
                }
                for (const color of Object.keys(pointsByColor)) {
                    const rgb = color.split(",").map((v) => parseInt(v));
                    // determine vector data based on color space conversion
                    let data;
                    if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.RGB) {
                        data = rgb;
                    }
                    else if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.HSL) {
                        data = (0, colorconversion_1.rgbToHsl)(rgb[0], rgb[1], rgb[2]);
                    }
                    else if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.LAB) {
                        data = (0, colorconversion_1.rgb2lab)(rgb);
                    }
                    else {
                        data = rgb;
                    }
                    // determine the weight (#pointsOfColor / #totalpoints) of each color
                    const weight = pointsByColor[color].length / (imgData.width * imgData.height);
                    const vec = new clustering_1.Vector(data, weight);
                    vec.tag = rgb;
                    vectors[vIdx++] = vec;
                }
                const random = new random_1.Random(settings.randomSeed === 0 ? new Date().getTime() : settings.randomSeed);
                // vectors of all the unique colors are built, time to cluster them
                const kmeans = new clustering_1.KMeans(vectors, settings.kMeansNrOfClusters, random);
                let curTime = new Date().getTime();
                kmeans.step();
                while (kmeans.currentDeltaDistanceDifference > settings.kMeansMinDeltaDifference) {
                    kmeans.step();
                    // update GUI every 500ms
                    if (new Date().getTime() - curTime > 500) {
                        curTime = new Date().getTime();
                        yield (0, common_1.delay)(0);
                        if (onUpdate != null) {
                            ColorReducer.updateKmeansOutputImageData(kmeans, settings, pointsByColor, imgData, outputImgData, false);
                            onUpdate(kmeans);
                        }
                    }
                }
                // update the output image data (because it will be used for further processing)
                ColorReducer.updateKmeansOutputImageData(kmeans, settings, pointsByColor, imgData, outputImgData, true);
                if (onUpdate != null) {
                    onUpdate(kmeans);
                }
            });
        }
        /**
         *  Updates the image data from the current kmeans centroids and their respective associated colors (vectors)
         */
        static updateKmeansOutputImageData(kmeans, settings, pointsByColor, imgData, outputImgData, restrictToSpecifiedColors) {
            for (let c = 0; c < kmeans.centroids.length; c++) {
                // for each cluster centroid
                const centroid = kmeans.centroids[c];
                // points per category are the different unique colors belonging to that cluster
                for (const v of kmeans.pointsPerCategory[c]) {
                    // determine the rgb color value of the cluster centroid
                    let rgb;
                    if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.RGB) {
                        rgb = centroid.values;
                    }
                    else if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.HSL) {
                        const hsl = centroid.values;
                        rgb = (0, colorconversion_1.hslToRgb)(hsl[0], hsl[1], hsl[2]);
                    }
                    else if (settings.kMeansClusteringColorSpace === settings_1.ClusteringColorSpace.LAB) {
                        const lab = centroid.values;
                        rgb = (0, colorconversion_1.lab2rgb)(lab);
                    }
                    else {
                        rgb = centroid.values;
                    }
                    // remove decimals
                    rgb = rgb.map(v => Math.floor(v));
                    if (restrictToSpecifiedColors) {
                        // LACI
                        settings.kMeansColorRestrictions = allowedColors;
                        if (settings.kMeansColorRestrictions.length > 0) {
                            // there are color restrictions, for each centroid find the color from the color restrictions that's the closest
                            let minDistance = Number.MAX_VALUE;
                            let closestRestrictedColor = null;
                            for (const color of settings.kMeansColorRestrictions) {
                                // RGB distance is not very good for the human eye perception, convert both to lab and then calculate the distance
                                const centroidLab = (0, colorconversion_1.rgb2lab)(rgb);
                                let restrictionLab;
                                if (typeof color === "string") {
                                    restrictionLab = (0, colorconversion_1.rgb2lab)(settings.colorAliases[color]);
                                }
                                else {
                                    restrictionLab = (0, colorconversion_1.rgb2lab)(color);
                                }
                                const distance = Math.sqrt((centroidLab[0] - restrictionLab[0]) * (centroidLab[0] - restrictionLab[0]) +
                                    (centroidLab[1] - restrictionLab[1]) * (centroidLab[1] - restrictionLab[1]) +
                                    (centroidLab[2] - restrictionLab[2]) * (centroidLab[2] - restrictionLab[2]));
                                if (distance < minDistance) {
                                    minDistance = distance;
                                    closestRestrictedColor = color;
                                }
                            }
                            // use this color instead
                            if (closestRestrictedColor !== null) {
                                if (typeof closestRestrictedColor === "string") {
                                    rgb = settings.colorAliases[closestRestrictedColor];
                                }
                                else {
                                    rgb = closestRestrictedColor;
                                }
                            }
                        }
                    }
                    let pointRGB = v.tag;
                    // replace all pixels of the old color by the new centroid color
                    const pointColor = `${Math.floor(pointRGB[0])},${Math.floor(pointRGB[1])},${Math.floor(pointRGB[2])}`;
                    for (const pt of pointsByColor[pointColor]) {
                        const ptx = pt % imgData.width;
                        const pty = Math.floor(pt / imgData.width);
                        let dataOffset = (pty * imgData.width + ptx) * 4;
                        outputImgData.data[dataOffset++] = rgb[0];
                        outputImgData.data[dataOffset++] = rgb[1];
                        outputImgData.data[dataOffset++] = rgb[2];
                    }
                }
            }
        }
        /**
         *  Builds a distance matrix for each color to each other
         */
        static buildColorDistanceMatrix(colorsByIndex) {
            const colorDistances = new Array(colorsByIndex.length);
            for (let j = 0; j < colorsByIndex.length; j++) {
                colorDistances[j] = new Array(colorDistances.length);
            }
            for (let j = 0; j < colorsByIndex.length; j++) {
                for (let i = j; i < colorsByIndex.length; i++) {
                    const c1 = colorsByIndex[j];
                    const c2 = colorsByIndex[i];
                    const distance = Math.sqrt((c1[0] - c2[0]) * (c1[0] - c2[0]) +
                        (c1[1] - c2[1]) * (c1[1] - c2[1]) +
                        (c1[2] - c2[2]) * (c1[2] - c2[2]));
                    colorDistances[i][j] = distance;
                    colorDistances[j][i] = distance;
                }
            }
            return colorDistances;
        }
        static processNarrowPixelStripCleanup(colormapResult) {
            return __awaiter(this, void 0, void 0, function* () {
                // build the color distance matrix, which describes the distance of each color to each other
                const colorDistances = ColorReducer.buildColorDistanceMatrix(colormapResult.colorsByIndex);
                let count = 0;
                const imgColorIndices = colormapResult.imgColorIndices;
                for (let j = 1; j < colormapResult.height - 1; j++) {
                    for (let i = 1; i < colormapResult.width - 1; i++) {
                        const top = imgColorIndices.get(i, j - 1);
                        const bottom = imgColorIndices.get(i, j + 1);
                        const left = imgColorIndices.get(i - 1, j);
                        const right = imgColorIndices.get(i + 1, j);
                        const cur = imgColorIndices.get(i, j);
                        if (cur !== top && cur !== bottom && cur !== left && cur !== right) {
                            // single pixel
                        }
                        else if (cur !== top && cur !== bottom) {
                            // check the color distance whether the top or bottom color is closer
                            const topColorDistance = colorDistances[cur][top];
                            const bottomColorDistance = colorDistances[cur][bottom];
                            imgColorIndices.set(i, j, topColorDistance < bottomColorDistance ? top : bottom);
                            count++;
                        }
                        else if (cur !== left && cur !== right) {
                            // check the color distance whether the top or bottom color is closer
                            const leftColorDistance = colorDistances[cur][left];
                            const rightColorDistance = colorDistances[cur][right];
                            imgColorIndices.set(i, j, leftColorDistance < rightColorDistance ? left : right);
                            count++;
                        }
                    }
                }
                console.log(count + " pixels replaced to remove narrow pixel strips");
            });
        }
    }
    exports.ColorReducer = ColorReducer;
});
define("structs/point", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Point = void 0;
    class Point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
        distanceTo(pt) {
            // don't do euclidean because then neighbours should be diagonally as well
            // because sqrt(2) < 2
            //  return Math.sqrt((pt.x - this.x) * (pt.x - this.x) + (pt.y - this.y) * (pt.y - this.y));
            return Math.abs(pt.x - this.x) + Math.abs(pt.y - this.y);
        }
        distanceToCoord(x, y) {
            // don't do euclidean because then neighbours should be diagonally as well
            // because sqrt(2) < 2
            //  return Math.sqrt((pt.x - this.x) * (pt.x - this.x) + (pt.y - this.y) * (pt.y - this.y));
            return Math.abs(x - this.x) + Math.abs(y - this.y);
        }
    }
    exports.Point = Point;
});
define("structs/boundingbox", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.BoundingBox = void 0;
    class BoundingBox {
        constructor() {
            this.minX = Number.MAX_VALUE;
            this.minY = Number.MAX_VALUE;
            this.maxX = Number.MIN_VALUE;
            this.maxY = Number.MIN_VALUE;
        }
        get width() {
            return this.maxX - this.minX + 1;
        }
        get height() {
            return this.maxY - this.minY + 1;
        }
    }
    exports.BoundingBox = BoundingBox;
});
define("facetmanagement", ["require", "exports", "structs/point"], function (require, exports, point_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetResult = exports.Facet = exports.PathPoint = exports.OrientationEnum = void 0;
    var OrientationEnum;
    (function (OrientationEnum) {
        OrientationEnum[OrientationEnum["Left"] = 0] = "Left";
        OrientationEnum[OrientationEnum["Top"] = 1] = "Top";
        OrientationEnum[OrientationEnum["Right"] = 2] = "Right";
        OrientationEnum[OrientationEnum["Bottom"] = 3] = "Bottom";
    })(OrientationEnum = exports.OrientationEnum || (exports.OrientationEnum = {}));
    /**
     * PathPoint is a point with an orientation that indicates which wall border is set
     */
    class PathPoint extends point_1.Point {
        constructor(pt, orientation) {
            super(pt.x, pt.y);
            this.orientation = orientation;
        }
        getWallX() {
            let x = this.x;
            if (this.orientation === OrientationEnum.Left) {
                x -= 0.5;
            }
            else if (this.orientation === OrientationEnum.Right) {
                x += 0.5;
            }
            return x;
        }
        getWallY() {
            let y = this.y;
            if (this.orientation === OrientationEnum.Top) {
                y -= 0.5;
            }
            else if (this.orientation === OrientationEnum.Bottom) {
                y += 0.5;
            }
            return y;
        }
        getNeighbour(facetResult) {
            switch (this.orientation) {
                case OrientationEnum.Left:
                    if (this.x - 1 >= 0) {
                        return facetResult.facetMap.get(this.x - 1, this.y);
                    }
                    break;
                case OrientationEnum.Right:
                    if (this.x + 1 < facetResult.width) {
                        return facetResult.facetMap.get(this.x + 1, this.y);
                    }
                    break;
                case OrientationEnum.Top:
                    if (this.y - 1 >= 0) {
                        return facetResult.facetMap.get(this.x, this.y - 1);
                    }
                    break;
                case OrientationEnum.Bottom:
                    if (this.y + 1 < facetResult.height) {
                        return facetResult.facetMap.get(this.x, this.y + 1);
                    }
                    break;
            }
            return -1;
        }
        toString() {
            return this.x + "," + this.y + " " + this.orientation;
        }
    }
    exports.PathPoint = PathPoint;
    /**
     *  A facet that represents an area of pixels of the same color
     */
    class Facet {
        constructor() {
            this.pointCount = 0;
            /**
             * Flag indicating if the neighbourfacets array is dirty. If it is, the neighbourfacets *have* to be rebuild
             * Before it can be used. This is useful to defer the rebuilding of the array until it's actually needed
             * and can remove a lot of duplicate building of the array because multiple facets were hitting the same neighbour
             * (over 50% on test images)
             */
            this.neighbourFacetsIsDirty = false;
        }
        getFullPathFromBorderSegments(useWalls) {
            const newpath = [];
            const addPoint = (pt) => {
                if (useWalls) {
                    newpath.push(new point_1.Point(pt.getWallX(), pt.getWallY()));
                }
                else {
                    newpath.push(new point_1.Point(pt.x, pt.y));
                }
            };
            let lastSegment = null;
            for (const seg of this.borderSegments) {
                // fix for the continuitity of the border segments. If transition points between border segments on the path aren't repeated, the
                // borders of the facets aren't always matching up leaving holes when rendered
                if (lastSegment != null) {
                    if (lastSegment.reverseOrder) {
                        addPoint(lastSegment.originalSegment.points[0]);
                    }
                    else {
                        addPoint(lastSegment.originalSegment.points[lastSegment.originalSegment.points.length - 1]);
                    }
                }
                for (let i = 0; i < seg.originalSegment.points.length; i++) {
                    const idx = seg.reverseOrder ? (seg.originalSegment.points.length - 1 - i) : i;
                    addPoint(seg.originalSegment.points[idx]);
                }
                lastSegment = seg;
            }
            return newpath;
        }
    }
    exports.Facet = Facet;
    /**
     *  Result of the facet construction, both as a map and as an array.
     *  Facets in the array can be null when they've been deleted
     */
    class FacetResult {
    }
    exports.FacetResult = FacetResult;
});
define("facetBorderSegmenter", ["require", "exports", "common", "structs/point", "facetmanagement"], function (require, exports, common_2, point_2, facetmanagement_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetBorderSegmenter = exports.FacetBoundarySegment = exports.PathSegment = void 0;
    /**
     *  Path segment is a segment of a border path that is adjacent to a specific neighbour facet
     */
    class PathSegment {
        constructor(points, neighbour) {
            this.points = points;
            this.neighbour = neighbour;
        }
    }
    exports.PathSegment = PathSegment;
    /**
     * Facet boundary segment describes the matched segment that is shared between 2 facets
     * When 2 segments are matched, one will be the original segment and the other one is removed
     * This ensures that all facets share the same segments, but sometimes in reverse order to ensure
     * the correct continuity of its entire oborder path
     */
    class FacetBoundarySegment {
        constructor(originalSegment, neighbour, reverseOrder) {
            this.originalSegment = originalSegment;
            this.neighbour = neighbour;
            this.reverseOrder = reverseOrder;
        }
    }
    exports.FacetBoundarySegment = FacetBoundarySegment;
    class FacetBorderSegmenter {
        /**
         *  Builds border segments that are shared between facets
         *  While border paths are all nice and fancy, they are not linked to neighbour facets
         *  So any change in the paths makes a not so nice gap between the facets, which makes smoothing them out impossible
         */
        static buildFacetBorderSegments(facetResult, nrOfTimesToHalvePoints = 2, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                // first chop up the border path in segments each time the neighbour at that point changes
                // (and sometimes even when it doesn't on that side but does on the neighbour's side)
                const segmentsPerFacet = FacetBorderSegmenter.prepareSegmentsPerFacet(facetResult);
                // now reduce the segment complexity with Haar wavelet reduction to smooth them out and make them
                // more curvy with data points instead of zig zag of a grid
                FacetBorderSegmenter.reduceSegmentComplexity(facetResult, segmentsPerFacet, nrOfTimesToHalvePoints);
                // now see which segments of facets with the prepared segments of the neighbour facets
                // and point them to the same one
                yield FacetBorderSegmenter.matchSegmentsWithNeighbours(facetResult, segmentsPerFacet, onUpdate);
            });
        }
        /**
         *  Chops up the border paths per facet into segments adjacent tothe same neighbour
         */
        static prepareSegmentsPerFacet(facetResult) {
            const segmentsPerFacet = new Array(facetResult.facets.length);
            for (const f of facetResult.facets) {
                if (f != null) {
                    const segments = [];
                    if (f.borderPath.length > 1) {
                        let currentPoints = [];
                        currentPoints.push(f.borderPath[0]);
                        for (let i = 1; i < f.borderPath.length; i++) {
                            const prevBorderPoint = f.borderPath[i - 1];
                            const curBorderPoint = f.borderPath[i];
                            const oldNeighbour = prevBorderPoint.getNeighbour(facetResult);
                            const curNeighbour = curBorderPoint.getNeighbour(facetResult);
                            let isTransitionPoint = false;
                            if (oldNeighbour !== curNeighbour) {
                                isTransitionPoint = true;
                            }
                            else {
                                // it's possible that due to inner facets inside the current facet that the
                                // border is interrupted on that facet's side, but not on the neighbour's side
                                if (oldNeighbour !== -1) {
                                    // check for tight rotations to break path if diagonals contain a different neighbour,
                                    // see https://i.imgur.com/o6Srqwj.png for visual path of the issue
                                    if (prevBorderPoint.x === curBorderPoint.x &&
                                        prevBorderPoint.y === curBorderPoint.y) {
                                        // rotation turn
                                        // check the diagonal neighbour to see if it remains the same
                                        //   +---+---+
                                        //   | dN|   |
                                        //   +---xxxx> (x = wall, dN = diagNeighbour)
                                        //   |   x f |
                                        //   +---v---+
                                        if ((prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Top && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Left) ||
                                            (prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Left && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Top)) {
                                            const diagNeighbour = facetResult.facetMap.get(curBorderPoint.x - 1, curBorderPoint.y - 1);
                                            if (diagNeighbour !== oldNeighbour) {
                                                isTransitionPoint = true;
                                            }
                                        }
                                        else if ((prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Top && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Right) ||
                                            (prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Right && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Top)) {
                                            const diagNeighbour = facetResult.facetMap.get(curBorderPoint.x + 1, curBorderPoint.y - 1);
                                            if (diagNeighbour !== oldNeighbour) {
                                                isTransitionPoint = true;
                                            }
                                        }
                                        else if ((prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Bottom && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Left) ||
                                            (prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Left && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Bottom)) {
                                            const diagNeighbour = facetResult.facetMap.get(curBorderPoint.x - 1, curBorderPoint.y + 1);
                                            if (diagNeighbour !== oldNeighbour) {
                                                isTransitionPoint = true;
                                            }
                                        }
                                        else if ((prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Bottom && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Right) ||
                                            (prevBorderPoint.orientation === facetmanagement_1.OrientationEnum.Right && curBorderPoint.orientation === facetmanagement_1.OrientationEnum.Bottom)) {
                                            const diagNeighbour = facetResult.facetMap.get(curBorderPoint.x + 1, curBorderPoint.y + 1);
                                            if (diagNeighbour !== oldNeighbour) {
                                                isTransitionPoint = true;
                                            }
                                        }
                                    }
                                }
                            }
                            currentPoints.push(curBorderPoint);
                            if (isTransitionPoint) {
                                // aha! a transition point, create the current points as new segment
                                // and start a new list
                                if (currentPoints.length > 1) {
                                    const segment = new PathSegment(currentPoints, oldNeighbour);
                                    segments.push(segment);
                                    currentPoints = [curBorderPoint];
                                }
                            }
                        }
                        // finally check if there is a remainder partial segment and either prepend
                        // the points to the first segment if they have the same neighbour or construct a
                        // new segment
                        if (currentPoints.length > 1) {
                            const oldNeighbour = f.borderPath[f.borderPath.length - 1].getNeighbour(facetResult);
                            if (segments.length > 0 && segments[0].neighbour === oldNeighbour) {
                                // the first segment and the remainder of the last one are the same part
                                // add the current points to the first segment by prefixing it
                                const mergedPoints = currentPoints.concat(segments[0].points);
                                segments[0].points = mergedPoints;
                            }
                            else {
                                // add the remainder as final segment
                                const segment = new PathSegment(currentPoints, oldNeighbour);
                                segments.push(segment);
                                currentPoints = [];
                            }
                        }
                    }
                    segmentsPerFacet[f.id] = segments;
                }
            }
            return segmentsPerFacet;
        }
        /**
         * Reduces each segment border path points
         */
        static reduceSegmentComplexity(facetResult, segmentsPerFacet, nrOfTimesToHalvePoints) {
            for (const f of facetResult.facets) {
                if (f != null) {
                    for (const segment of segmentsPerFacet[f.id]) {
                        for (let i = 0; i < nrOfTimesToHalvePoints; i++) {
                            segment.points = FacetBorderSegmenter.reduceSegmentHaarWavelet(segment.points, true, facetResult.width, facetResult.height);
                        }
                    }
                }
            }
        }
        /**
         *  Remove the points by taking the average per pair and using that as a new point
         *  in the reduced segment. The delta values that create the Haar wavelet are not tracked
         *  because they are unneeded.
         */
        static reduceSegmentHaarWavelet(newpath, skipOutsideBorders, width, height) {
            if (newpath.length <= 5) {
                return newpath;
            }
            const reducedPath = [];
            reducedPath.push(newpath[0]);
            for (let i = 1; i < newpath.length - 2; i += 2) {
                if (!skipOutsideBorders || (skipOutsideBorders && !FacetBorderSegmenter.isOutsideBorderPoint(newpath[i], width, height))) {
                    const cx = (newpath[i].x + newpath[i + 1].x) / 2;
                    const cy = (newpath[i].y + newpath[i + 1].y) / 2;
                    reducedPath.push(new facetmanagement_1.PathPoint(new point_2.Point(cx, cy), facetmanagement_1.OrientationEnum.Left));
                }
                else {
                    reducedPath.push(newpath[i]);
                    reducedPath.push(newpath[i + 1]);
                }
            }
            // close the loop
            reducedPath.push(newpath[newpath.length - 1]);
            return reducedPath;
        }
        static isOutsideBorderPoint(point, width, height) {
            return point.x === 0 || point.y === 0 || point.x === width - 1 || point.y === height - 1;
        }
        static calculateArea(path) {
            let total = 0;
            for (let i = 0; i < path.length; i++) {
                const addX = path[i].x;
                const addY = path[i === path.length - 1 ? 0 : i + 1].y;
                const subX = path[i === path.length - 1 ? 0 : i + 1].x;
                const subY = path[i].y;
                total += (addX * addY * 0.5);
                total -= (subX * subY * 0.5);
            }
            return Math.abs(total);
        }
        /**
         *  Matches all segments with each other between facets and their neighbour
         *  A segment matches when the start and end match or the start matches with the end and vice versa
         *  (then the segment will need to be traversed in reverse order)
         */
        static matchSegmentsWithNeighbours(facetResult, segmentsPerFacet, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                // max distance of the start/end points of the segment that it can be before the segments don't match up
                const MAX_DISTANCE = 4;
                // reserve room
                for (const f of facetResult.facets) {
                    if (f != null) {
                        f.borderSegments = new Array(segmentsPerFacet[f.id].length);
                    }
                }
                let count = 0;
                // and now the fun begins to match segments from 1 facet to its neighbours and vice versa
                for (const f of facetResult.facets) {
                    if (f != null) {
                        const debug = false;
                        for (let s = 0; s < segmentsPerFacet[f.id].length; s++) {
                            const segment = segmentsPerFacet[f.id][s];
                            if (segment != null && f.borderSegments[s] == null) {
                                f.borderSegments[s] = new FacetBoundarySegment(segment, segment.neighbour, false);
                                if (debug) {
                                    console.log("Setting facet " + f.id + " segment " + s + " to " + f.borderSegments[s]);
                                }
                                if (segment.neighbour !== -1) {
                                    const neighbourFacet = facetResult.facets[segment.neighbour];
                                    // see if there is a match to be found
                                    let matchFound = false;
                                    if (neighbourFacet != null) {
                                        const neighbourSegments = segmentsPerFacet[segment.neighbour];
                                        for (let ns = 0; ns < neighbourSegments.length; ns++) {
                                            const neighbourSegment = neighbourSegments[ns];
                                            // only try to match against the segments that aren't processed yet
                                            // and which are adjacent to the boundary of the current facet
                                            if (neighbourSegment != null && neighbourSegment.neighbour === f.id) {
                                                const segStartPoint = segment.points[0];
                                                const segEndPoint = segment.points[segment.points.length - 1];
                                                const nSegStartPoint = neighbourSegment.points[0];
                                                const nSegEndPoint = neighbourSegment.points[neighbourSegment.points.length - 1];
                                                let matchesStraight = (segStartPoint.distanceTo(nSegStartPoint) <= MAX_DISTANCE &&
                                                    segEndPoint.distanceTo(nSegEndPoint) <= MAX_DISTANCE);
                                                let matchesReverse = (segStartPoint.distanceTo(nSegEndPoint) <= MAX_DISTANCE &&
                                                    segEndPoint.distanceTo(nSegStartPoint) <= MAX_DISTANCE);
                                                if (matchesStraight && matchesReverse) {
                                                    // dang it , both match, it must be a tiny segment, but when placed wrongly it'll overlap in the path creating an hourglass 
                                                    //  e.g. https://i.imgur.com/XZQhxRV.png
                                                    // determine which is the closest
                                                    if (segStartPoint.distanceTo(nSegStartPoint) + segEndPoint.distanceTo(nSegEndPoint) <
                                                        segStartPoint.distanceTo(nSegEndPoint) + segEndPoint.distanceTo(nSegStartPoint)) {
                                                        matchesStraight = true;
                                                        matchesReverse = false;
                                                    }
                                                    else {
                                                        matchesStraight = false;
                                                        matchesReverse = true;
                                                    }
                                                }
                                                if (matchesStraight) {
                                                    // start & end points match
                                                    if (debug) {
                                                        console.log("Match found for facet " + f.id + " to neighbour " + neighbourFacet.id);
                                                    }
                                                    neighbourFacet.borderSegments[ns] = new FacetBoundarySegment(segment, f.id, false);
                                                    if (debug) {
                                                        console.log("Setting facet " + neighbourFacet.id + " segment " + ns + " to " + neighbourFacet.borderSegments[ns]);
                                                    }
                                                    segmentsPerFacet[neighbourFacet.id][ns] = null;
                                                    matchFound = true;
                                                    break;
                                                }
                                                else if (matchesReverse) {
                                                    // start & end points match  but in reverse order
                                                    if (debug) {
                                                        console.log("Reverse match found for facet " + f.id + " to neighbour " + neighbourFacet.id);
                                                    }
                                                    neighbourFacet.borderSegments[ns] = new FacetBoundarySegment(segment, f.id, true);
                                                    if (debug) {
                                                        console.log("Setting facet " + neighbourFacet.id + " segment " + ns + " to " + neighbourFacet.borderSegments[ns]);
                                                    }
                                                    segmentsPerFacet[neighbourFacet.id][ns] = null;
                                                    matchFound = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (!matchFound && debug) {
                                        // it's possible that the border is not shared with its neighbour
                                        // this can happen when the segment fully falls inside the other facet
                                        // though the above checks in the preparation of the segments should probably
                                        // cover all cases
                                        console.error("No match found for segment of " + f.id + ": " +
                                            ("siding " + segment.neighbour + " " + segment.points[0] + " -> " + segment.points[segment.points.length - 1]));
                                    }
                                }
                            }
                            // clear the current segment so it can't be processed again when processing the neighbour facet
                            segmentsPerFacet[f.id][s] = null;
                        }
                        if (count % 100 === 0) {
                            yield (0, common_2.delay)(0);
                            if (onUpdate != null) {
                                onUpdate(f.id / facetResult.facets.length);
                            }
                        }
                    }
                    count++;
                }
                if (onUpdate != null) {
                    onUpdate(1);
                }
            });
        }
    }
    exports.FacetBorderSegmenter = FacetBorderSegmenter;
});
define("facetBorderTracer", ["require", "exports", "common", "structs/point", "structs/typedarrays", "facetmanagement"], function (require, exports, common_3, point_3, typedarrays_2, facetmanagement_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetBorderTracer = void 0;
    class FacetBorderTracer {
        /**
         *  Traces the border path of the facet from the facet border points.
         *  Imagine placing walls around the outer side of the border points.
         */
        static buildFacetBorderPaths(facetResult, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                let count = 0;
                const borderMask = new typedarrays_2.BooleanArray2D(facetResult.width, facetResult.height);
                // sort by biggest facets first
                const facetProcessingOrder = facetResult.facets.filter((f) => f != null).slice(0).sort((a, b) => b.pointCount > a.pointCount ? 1 : (b.pointCount < a.pointCount ? -1 : 0)).map((f) => f.id);
                for (let fidx = 0; fidx < facetProcessingOrder.length; fidx++) {
                    const f = facetResult.facets[facetProcessingOrder[fidx]];
                    if (f != null) {
                        for (const bp of f.borderPoints) {
                            borderMask.set(bp.x, bp.y, true);
                        }
                        // keep track of which walls are already set on each pixel
                        // e.g. xWall.get(x,y) is the left wall of point x,y
                        // as the left wall of (x+1,y) and right wall of (x,y) is the same
                        // the right wall of x,y can be set with xWall.set(x+1,y).
                        // Analogous for the horizontal walls in yWall
                        const xWall = new typedarrays_2.BooleanArray2D(facetResult.width + 1, facetResult.height + 1);
                        const yWall = new typedarrays_2.BooleanArray2D(facetResult.width + 1, facetResult.height + 1);
                        // the first border point will guaranteed be one of the outer ones because
                        // it will be the first point that is encountered of the facet when building
                        // them in buildFacet with DFS.
                        // --> Or so I thought, which is apparently not the case in rare circumstances
                        // sooooo go look for a border that edges with the bounding box, this is definitely
                        // on the outer side then.
                        let borderStartIndex = -1;
                        for (let i = 0; i < f.borderPoints.length; i++) {
                            if ((f.borderPoints[i].x === f.bbox.minX || f.borderPoints[i].x === f.bbox.maxX) ||
                                (f.borderPoints[i].y === f.bbox.minY || f.borderPoints[i].y === f.bbox.maxY)) {
                                borderStartIndex = i;
                                break;
                            }
                        }
                        // determine the starting point orientation (the outside of facet)
                        const pt = new facetmanagement_2.PathPoint(f.borderPoints[borderStartIndex], facetmanagement_2.OrientationEnum.Left);
                        // L T R B
                        if (pt.x - 1 < 0 || facetResult.facetMap.get(pt.x - 1, pt.y) !== f.id) {
                            pt.orientation = facetmanagement_2.OrientationEnum.Left;
                        }
                        else if (pt.y - 1 < 0 || facetResult.facetMap.get(pt.x, pt.y - 1) !== f.id) {
                            pt.orientation = facetmanagement_2.OrientationEnum.Top;
                        }
                        else if (pt.x + 1 >= facetResult.width || facetResult.facetMap.get(pt.x + 1, pt.y) !== f.id) {
                            pt.orientation = facetmanagement_2.OrientationEnum.Right;
                        }
                        else if (pt.y + 1 >= facetResult.height || facetResult.facetMap.get(pt.x, pt.y + 1) !== f.id) {
                            pt.orientation = facetmanagement_2.OrientationEnum.Bottom;
                        }
                        // build a border path from that point
                        const path = FacetBorderTracer.getPath(pt, facetResult, f, borderMask, xWall, yWall);
                        f.borderPath = path;
                        if (count % 100 === 0) {
                            yield (0, common_3.delay)(0);
                            if (onUpdate != null) {
                                onUpdate(fidx / facetProcessingOrder.length);
                            }
                        }
                    }
                    count++;
                }
                if (onUpdate != null) {
                    onUpdate(1);
                }
            });
        }
        /**
         * Returns a border path starting from the given point
         */
        static getPath(pt, facetResult, f, borderMask, xWall, yWall) {
            const debug = false;
            let finished = false;
            const count = 0;
            const path = [];
            FacetBorderTracer.addPointToPath(path, pt, xWall, f, yWall);
            // check rotations first, then straight along the ouside and finally diagonally
            // this ensures that bends are always taken as tight as possible
            // so it doesn't skip border points to later loop back to and get stuck (hopefully)
            while (!finished) {
                if (debug) {
                    console.log(pt.x + " " + pt.y + " " + pt.orientation);
                }
                // yes, technically i could do some trickery to only get the left/top cases
                // by shifting the pixels but that means some more shenanigans in correct order of things
                // so whatever. (And yes I tried it but it wasn't worth the debugging hell that ensued)
                const possibleNextPoints = [];
                //   +---+---+
                //   |  <|   |
                //   +---+---+
                if (pt.orientation === facetmanagement_2.OrientationEnum.Left) {
                    // check rotate to top
                    //   +---+---+
                    //   |   |   |
                    //   +---xnnnn (x = old wall, n = new wall, F = current facet x,y)
                    //   |   x F |
                    //   +---x---+
                    if (((pt.y - 1 >= 0 && facetResult.facetMap.get(pt.x, pt.y - 1) !== f.id) // top exists and is a neighbour facet
                        || pt.y - 1 < 0) // or top doesn't exist, which is the boundary of the image
                        && !yWall.get(pt.x, pt.y)) { // and the wall isn't set yet
                        // can place top _ wall at x,y
                        if (debug) {
                            console.log("can place top _ wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rotate to bottom
                    //   +---+---+
                    //   |   |   |
                    //   +---x---+ (x = old wall, n = new wall, F = current facet x,y)
                    //   |   x F |
                    //   +---xnnnn
                    if (((pt.y + 1 < facetResult.height && facetResult.facetMap.get(pt.x, pt.y + 1) !== f.id) // bottom exists and is a neighbour facet
                        || pt.y + 1 >= facetResult.height) // or bottom doesn't exist, which is the boundary of the image
                        && !yWall.get(pt.x, pt.y + 1)) { // and the wall isn't set yet
                        // can place bottom  _ wall at x,y
                        if (debug) {
                            console.log("can place bottom _ wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check upwards
                    //   +---n---+
                    //   |   n   |
                    //   +---x---+ (x = old wall, n = new wall, F = current facet x,y)
                    //   |   x F |
                    //   +---x---+
                    if (pt.y - 1 >= 0 // top exists
                        && facetResult.facetMap.get(pt.x, pt.y - 1) === f.id // and is part of the same facet
                        && (pt.x - 1 < 0 || facetResult.facetMap.get(pt.x - 1, pt.y - 1) !== f.id) // and
                        && borderMask.get(pt.x, pt.y - 1)
                        && !xWall.get(pt.x, pt.y - 1)) {
                        // can place | wall at x,y-1
                        if (debug) {
                            console.log(`can place left | wall at x,y-1`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y - 1), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                    // check downwards
                    //   +---x---+
                    //   |   x F |
                    //   +---x---+ (x = old wall, n = new wall, F = current facet x,y)
                    //   |   n   |
                    //   +---n---+
                    if (pt.y + 1 < facetResult.height
                        && facetResult.facetMap.get(pt.x, pt.y + 1) === f.id
                        && (pt.x - 1 < 0 || facetResult.facetMap.get(pt.x - 1, pt.y + 1) !== f.id)
                        && borderMask.get(pt.x, pt.y + 1)
                        && !xWall.get(pt.x, pt.y + 1)) {
                        // can place | wall at x,y+1
                        if (debug) {
                            console.log("can place left | wall at x,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y + 1), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                    // check left upwards
                    //   +---+---+
                    //   |   |   |
                    //   nnnnx---+ (x = old wall, n = new wall, F = current facet x,y)
                    //   |   x F |
                    //   +---x---+
                    if (pt.y - 1 >= 0 && pt.x - 1 >= 0 // there is a left upwards
                        && facetResult.facetMap.get(pt.x - 1, pt.y - 1) === f.id // and it belongs to the same facet
                        && borderMask.get(pt.x - 1, pt.y - 1) // and is on the border
                        && !yWall.get(pt.x - 1, pt.y - 1 + 1) // and the bottom wall isn't set yet
                        && !yWall.get(pt.x, pt.y) // and the path didn't come from the top of the current one to prevent getting a T shaped path (issue: https://i.imgur.com/ggUWuXi.png)
                    ) {
                        // can place bottom _ wall at x-1,y-1
                        if (debug) {
                            console.log("can place bottom _ wall at x-1,y-1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y - 1), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check left downwards
                    //   +---x---+
                    //   |   x F |
                    //   nnnnx---+ (x = old wall, n = new wall, F = current facet x,y)
                    //   |   |   |
                    //   +---+---+
                    if (pt.y + 1 < facetResult.height && pt.x - 1 >= 0 // there is a left downwards
                        && facetResult.facetMap.get(pt.x - 1, pt.y + 1) === f.id // and belongs to the same facet
                        && borderMask.get(pt.x - 1, pt.y + 1) // and is on the border
                        && !yWall.get(pt.x - 1, pt.y + 1) // and the top wall isn't set yet
                        && !yWall.get(pt.x, pt.y + 1) // and the path didn't come from the bottom of the current point to prevent T shape
                    ) {
                        // can place top _ wall at x-1,y+1
                        if (debug) {
                            console.log("can place top _ wall at x-1,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y + 1), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                }
                else if (pt.orientation === facetmanagement_2.OrientationEnum.Top) {
                    // check rotate to left
                    if (((pt.x - 1 >= 0
                        && facetResult.facetMap.get(pt.x - 1, pt.y) !== f.id)
                        || pt.x - 1 < 0)
                        && !xWall.get(pt.x, pt.y)) {
                        // can place left | wall at x,y
                        if (debug) {
                            console.log("can place left | wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rotate to right
                    if (((pt.x + 1 < facetResult.width
                        && facetResult.facetMap.get(pt.x + 1, pt.y) !== f.id)
                        || pt.x + 1 >= facetResult.width)
                        && !xWall.get(pt.x + 1, pt.y)) {
                        // can place right | wall at x,y
                        if (debug) {
                            console.log("can place right | wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check leftwards
                    if (pt.x - 1 >= 0
                        && facetResult.facetMap.get(pt.x - 1, pt.y) === f.id
                        && (pt.y - 1 < 0 || facetResult.facetMap.get(pt.x - 1, pt.y - 1) !== f.id)
                        && borderMask.get(pt.x - 1, pt.y)
                        && !yWall.get(pt.x - 1, pt.y)) {
                        // can place top _ wall at x-1,y
                        if (debug) {
                            console.log(`can place top _ wall at x-1,y`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rightwards
                    if (pt.x + 1 < facetResult.width
                        && facetResult.facetMap.get(pt.x + 1, pt.y) === f.id
                        && (pt.y - 1 < 0 || facetResult.facetMap.get(pt.x + 1, pt.y - 1) !== f.id)
                        && borderMask.get(pt.x + 1, pt.y)
                        && !yWall.get(pt.x + 1, pt.y)) {
                        // can place top _ wall at x+1,y
                        if (debug) {
                            console.log(`can place top _ wall at x+1,y`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                    // check left upwards
                    if (pt.y - 1 >= 0 && pt.x - 1 >= 0 // there is a left upwards
                        && facetResult.facetMap.get(pt.x - 1, pt.y - 1) === f.id // and it belongs to the same facet
                        && borderMask.get(pt.x - 1, pt.y - 1) // and it's part of the border
                        && !xWall.get(pt.x - 1 + 1, pt.y - 1) // the right wall isn't set yet
                        && !xWall.get(pt.x, pt.y) // and the left wall of the current point isn't set yet to prevent |- path
                    ) {
                        // can place right | wall at x-1,y-1
                        if (debug) {
                            console.log("can place right | wall at x-1,y-1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y - 1), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check right upwards
                    if (pt.y - 1 >= 0 && pt.x + 1 < facetResult.width // there is a right upwards
                        && facetResult.facetMap.get(pt.x + 1, pt.y - 1) === f.id // and it belongs to the same facet
                        && borderMask.get(pt.x + 1, pt.y - 1) // and it's on the border
                        && !xWall.get(pt.x + 1, pt.y - 1) // and the left wall isn't set yet
                        && !xWall.get(pt.x + 1, pt.y) // and the right wall of the current point isn't set yet to prevent -| path
                    ) {
                        // can place left |  wall at x+1,y-1
                        if (debug) {
                            console.log("can place left |  wall at x+1,y-1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y - 1), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                }
                else if (pt.orientation === facetmanagement_2.OrientationEnum.Right) {
                    // check rotate to top
                    if (((pt.y - 1 >= 0
                        && facetResult.facetMap.get(pt.x, pt.y - 1) !== f.id)
                        || pt.y - 1 < 0)
                        && !yWall.get(pt.x, pt.y)) {
                        // can place top _ wall at x,y
                        if (debug) {
                            console.log("can place top _ wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rotate to bottom
                    if (((pt.y + 1 < facetResult.height
                        && facetResult.facetMap.get(pt.x, pt.y + 1) !== f.id)
                        || pt.y + 1 >= facetResult.height)
                        && !yWall.get(pt.x, pt.y + 1)) {
                        // can place bottom  _ wall at x,y
                        if (debug) {
                            console.log("can place bottom _ wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check upwards
                    if (pt.y - 1 >= 0
                        && facetResult.facetMap.get(pt.x, pt.y - 1) === f.id
                        && (pt.x + 1 >= facetResult.width || facetResult.facetMap.get(pt.x + 1, pt.y - 1) !== f.id)
                        && borderMask.get(pt.x, pt.y - 1)
                        && !xWall.get(pt.x + 1, pt.y - 1)) {
                        // can place right | wall at x,y-1
                        if (debug) {
                            console.log(`can place right | wall at x,y-1`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y - 1), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check downwards
                    if (pt.y + 1 < facetResult.height
                        && facetResult.facetMap.get(pt.x, pt.y + 1) === f.id
                        && (pt.x + 1 >= facetResult.width || facetResult.facetMap.get(pt.x + 1, pt.y + 1) !== f.id)
                        && borderMask.get(pt.x, pt.y + 1)
                        && !xWall.get(pt.x + 1, pt.y + 1)) {
                        // can place right | wall at x,y+1
                        if (debug) {
                            console.log("can place right | wall at x,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y + 1), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check right upwards
                    if (pt.y - 1 >= 0 && pt.x + 1 < facetResult.width // there is a right upwards
                        && facetResult.facetMap.get(pt.x + 1, pt.y - 1) === f.id // and belongs to the same facet
                        && borderMask.get(pt.x + 1, pt.y - 1) // and is on the border
                        && !yWall.get(pt.x + 1, pt.y - 1 + 1) // and the bottom wall isn't set yet
                        && !yWall.get(pt.x, pt.y) // and the top wall of the current point isn't set to prevent a T shape
                    ) {
                        // can place bottom _ wall at x+1,y-1
                        if (debug) {
                            console.log("can place bottom _ wall at x+1,y-1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y - 1), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check right downwards
                    if (pt.y + 1 < facetResult.height && pt.x + 1 < facetResult.width // there is a right downwards
                        && facetResult.facetMap.get(pt.x + 1, pt.y + 1) === f.id // and belongs to the same facet
                        && borderMask.get(pt.x + 1, pt.y + 1) // and is on the border
                        && !yWall.get(pt.x + 1, pt.y + 1) // and the top wall isn't visited yet
                        && !yWall.get(pt.x, pt.y + 1) // and the bottom wall of the current point isn't set to prevent a T shape
                    ) {
                        // can place top _ wall at x+1,y+1
                        if (debug) {
                            console.log("can place top _ wall at x+1,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y + 1), facetmanagement_2.OrientationEnum.Top);
                        possibleNextPoints.push(nextpt);
                    }
                }
                else if (pt.orientation === facetmanagement_2.OrientationEnum.Bottom) {
                    // check rotate to left
                    if (((pt.x - 1 >= 0
                        && facetResult.facetMap.get(pt.x - 1, pt.y) !== f.id)
                        || pt.x - 1 < 0)
                        && !xWall.get(pt.x, pt.y)) {
                        // can place left | wall at x,y
                        if (debug) {
                            console.log("can place left | wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rotate to right
                    if (((pt.x + 1 < facetResult.width
                        && facetResult.facetMap.get(pt.x + 1, pt.y) !== f.id)
                        || pt.x + 1 >= facetResult.width)
                        && !xWall.get(pt.x + 1, pt.y)) {
                        // can place right | wall at x,y
                        if (debug) {
                            console.log("can place right | wall at x,y");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x, pt.y), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check leftwards
                    if (pt.x - 1 >= 0
                        && facetResult.facetMap.get(pt.x - 1, pt.y) === f.id
                        && (pt.y + 1 >= facetResult.height || facetResult.facetMap.get(pt.x - 1, pt.y + 1) !== f.id)
                        && borderMask.get(pt.x - 1, pt.y)
                        && !yWall.get(pt.x - 1, pt.y + 1)) {
                        // can place bottom _ wall at x-1,y
                        if (debug) {
                            console.log(`can place bottom _ wall at x-1,y`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check rightwards
                    if (pt.x + 1 < facetResult.width
                        && facetResult.facetMap.get(pt.x + 1, pt.y) === f.id
                        && (pt.y + 1 >= facetResult.height || facetResult.facetMap.get(pt.x + 1, pt.y + 1) !== f.id)
                        && borderMask.get(pt.x + 1, pt.y)
                        && !yWall.get(pt.x + 1, pt.y + 1)) {
                        // can place top _ wall at x+1,y
                        if (debug) {
                            console.log(`can place bottom _ wall at x+1,y`);
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y), facetmanagement_2.OrientationEnum.Bottom);
                        possibleNextPoints.push(nextpt);
                    }
                    // check left downwards
                    if (pt.y + 1 < facetResult.height && pt.x - 1 >= 0 // there is a left downwards
                        && facetResult.facetMap.get(pt.x - 1, pt.y + 1) === f.id // and it's the same facet
                        && borderMask.get(pt.x - 1, pt.y + 1) // and it's on the border
                        && !xWall.get(pt.x - 1 + 1, pt.y + 1) // and the right wall isn't set yet
                        && !xWall.get(pt.x, pt.y) // and the left wall of the current point isn't set yet to prevent |- path
                    ) {
                        // can place right | wall at x-1,y-1
                        if (debug) {
                            console.log("can place right | wall at x-1,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x - 1, pt.y + 1), facetmanagement_2.OrientationEnum.Right);
                        possibleNextPoints.push(nextpt);
                    }
                    // check right downwards
                    if (pt.y + 1 < facetResult.height && pt.x + 1 < facetResult.width // there is a right downwards
                        && facetResult.facetMap.get(pt.x + 1, pt.y + 1) === f.id // and it's the same facet
                        && borderMask.get(pt.x + 1, pt.y + 1) // and it's on the border
                        && !xWall.get(pt.x + 1, pt.y + 1) // and the left wall isn't set yet
                        && !xWall.get(pt.x + 1, pt.y) // and the right wall of the current point isn't set yet to prevent -| path
                    ) {
                        // can place left |  wall at x+1,y+1
                        if (debug) {
                            console.log("can place left |  wall at x+1,y+1");
                        }
                        const nextpt = new facetmanagement_2.PathPoint(new point_3.Point(pt.x + 1, pt.y + 1), facetmanagement_2.OrientationEnum.Left);
                        possibleNextPoints.push(nextpt);
                    }
                }
                if (possibleNextPoints.length > 1) {
                    // TODO it's now not necessary anymore to aggregate all possibilities, the first one is going to be the correct
                    // selection to trace the entire border, so the if checks above can include a skip once ssible point is found again
                    pt = possibleNextPoints[0];
                    FacetBorderTracer.addPointToPath(path, pt, xWall, f, yWall);
                }
                else if (possibleNextPoints.length === 1) {
                    pt = possibleNextPoints[0];
                    FacetBorderTracer.addPointToPath(path, pt, xWall, f, yWall);
                }
                else {
                    finished = true;
                }
            }
            // clear up the walls set for the path so the array can be reused
            for (const pathPoint of path) {
                switch (pathPoint.orientation) {
                    case facetmanagement_2.OrientationEnum.Left:
                        xWall.set(pathPoint.x, pathPoint.y, false);
                        break;
                    case facetmanagement_2.OrientationEnum.Top:
                        yWall.set(pathPoint.x, pathPoint.y, false);
                        break;
                    case facetmanagement_2.OrientationEnum.Right:
                        xWall.set(pathPoint.x + 1, pathPoint.y, false);
                        break;
                    case facetmanagement_2.OrientationEnum.Bottom:
                        yWall.set(pathPoint.x, pathPoint.y + 1, false);
                        break;
                }
            }
            return path;
        }
        /**
         * Add a point to the border path and ensure the correct xWall/yWalls is set
         */
        static addPointToPath(path, pt, xWall, f, yWall) {
            path.push(pt);
            switch (pt.orientation) {
                case facetmanagement_2.OrientationEnum.Left:
                    xWall.set(pt.x, pt.y, true);
                    break;
                case facetmanagement_2.OrientationEnum.Top:
                    yWall.set(pt.x, pt.y, true);
                    break;
                case facetmanagement_2.OrientationEnum.Right:
                    xWall.set(pt.x + 1, pt.y, true);
                    break;
                case facetmanagement_2.OrientationEnum.Bottom:
                    yWall.set(pt.x, pt.y + 1, true);
                    break;
            }
        }
    }
    exports.FacetBorderTracer = FacetBorderTracer;
});
// Faster flood fill from
// http://www.adammil.net/blog/v126_A_More_Efficient_Flood_Fill.html
define("lib/fill", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.fill = void 0;
    function fill(x, y, width, height, visited, setFill) {
        // at this point, we know array[y,x] is clear, and we want to move as far as possible to the upper-left. moving
        // up is much more important than moving left, so we could try to make this smarter by sometimes moving to
        // the right if doing so would allow us to move further up, but it doesn't seem worth the complexit
        let xx = x;
        let yy = y;
        while (true) {
            const ox = xx;
            const oy = yy;
            while (yy !== 0 && !visited(xx, yy - 1)) {
                yy--;
            }
            while (xx !== 0 && !visited(xx - 1, yy)) {
                xx--;
            }
            if (xx === ox && yy === oy) {
                break;
            }
        }
        fillCore(xx, yy, width, height, visited, setFill);
    }
    exports.fill = fill;
    function fillCore(x, y, width, height, visited, setFill) {
        // at this point, we know that array[y,x] is clear, and array[y-1,x] and array[y,x-1] are set.
        // we'll begin scanning down and to the right, attempting to fill an entire rectangular block
        let lastRowLength = 0; // the number of cells that were clear in the last row we scanned
        do {
            let rowLength = 0;
            let sx = x; // keep track of how long this row is. sx is the starting x for the main scan below
            // now we want to handle a case like |***|, where we fill 3 cells in the first row and then after we move to
            // the second row we find the first  | **| cell is filled, ending our rectangular scan. rather than handling
            // this via the recursion below, we'll increase the starting value of 'x' and reduce the last row length to
            // match. then we'll continue trying to set the narrower rectangular block
            if (lastRowLength !== 0 && visited(x, y)) {
                do {
                    if (--lastRowLength === 0) {
                        return;
                    } // shorten the row. if it's full, we're done
                } while (visited(++x, y)); // otherwise, update the starting point of the main scan to match
                sx = x;
            }
            else {
                for (; x !== 0 && !visited(x - 1, y); rowLength++, lastRowLength++) {
                    x--;
                    setFill(x, y); // to avoid scanning the cells twice, we'll fill them and update rowLength here
                    // if there's something above the new starting point, handle that recursively. this deals with cases
                    // like |* **| when we begin filling from (2,0), move down to (2,1), and then move left to (0,1).
                    // the  |****| main scan assumes the portion of the previous row from x to x+lastRowLength has already
                    // been filled. adjusting x and lastRowLength breaks that assumption in this case, so we must fix it
                    if (y !== 0 && !visited(x, y - 1)) {
                        fill(x, y - 1, width, height, visited, setFill);
                    } // use _Fill since there may be more up and left
                }
            }
            // now at this point we can begin to scan the current row in the rectangular block. the span of the previous
            // row from x (inclusive) to x+lastRowLength (exclusive) has already been filled, so we don't need to
            // check it. so scan across to the right in the current row
            for (; sx < width && !visited(sx, y); rowLength++, sx++) {
                setFill(sx, y);
            }
            // now we've scanned this row. if the block is rectangular, then the previous row has already been scanned,
            // so we don't need to look upwards and we're going to scan the next row in the next iteration so we don't
            // need to look downwards. however, if the block is not rectangular, we may need to look upwards or rightwards
            // for some portion of the row. if this row was shorter than the last row, we may need to look rightwards near
            // the end, as in the case of |*****|, where the first row is 5 cells long and the second row is 3 cells long.
            // we must look to the right  |*** *| of the single cell at the end of the second row, i.e. at (4,1)
            if (rowLength < lastRowLength) {
                for (const end = x + lastRowLength; ++sx < end;) { // there. any clear cells would have been connected to the previous
                    if (!visited(sx, y)) {
                        fillCore(sx, y, width, height, visited, setFill);
                    } // row. the cells up and left must be set so use FillCore
                }
            }
            else if (rowLength > lastRowLength && y !== 0) {
                for (let ux = x + lastRowLength; ++ux < sx;) {
                    if (!visited(ux, y - 1)) {
                        fill(ux, y - 1, width, height, visited, setFill);
                    } // since there may be clear cells up and left, use _Fill
                }
            }
            lastRowLength = rowLength; // record the new row length
        } while (lastRowLength !== 0 && ++y < height); // if we get to a full row or to the bottom, we're done
    }
});
define("facetReducer", ["require", "exports", "colorreductionmanagement", "common", "facetCreator", "structs/typedarrays"], function (require, exports, colorreductionmanagement_1, common_4, facetCreator_1, typedarrays_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetReducer = void 0;
    class FacetReducer {
        /**
         *  Remove all facets that have a pointCount smaller than the given number.
         */
        static reduceFacets(smallerThan, removeFacetsFromLargeToSmall, maximumNumberOfFacets, colorsByIndex, facetResult, imgColorIndices, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                const visitedCache = new typedarrays_3.BooleanArray2D(facetResult.width, facetResult.height);
                // build the color distance matrix, which describes the distance of each color to each other
                const colorDistances = colorreductionmanagement_1.ColorReducer.buildColorDistanceMatrix(colorsByIndex);
                // process facets from large to small. This results in better consistency with the original image
                // because the small facets act as boundary for the large merges keeping them mostly in place of where they should remain
                // then afterwards the smaller ones are deleted which will just end up completely isolated and thus entirely replaced
                // with the outer facet. But then again, what do I know, I'm just a comment.
                const facetProcessingOrder = facetResult.facets.filter((f) => f != null).slice(0).sort((a, b) => b.pointCount > a.pointCount ? 1 : (b.pointCount < a.pointCount ? -1 : 0)).map((f) => f.id);
                if (!removeFacetsFromLargeToSmall) {
                    facetProcessingOrder.reverse();
                }
                let curTime = new Date().getTime();
                for (let fidx = 0; fidx < facetProcessingOrder.length; fidx++) {
                    const f = facetResult.facets[facetProcessingOrder[fidx]];
                    // facets can be removed by merging by others due to a previous facet deletion
                    if (f != null && f.pointCount < smallerThan) {
                        FacetReducer.deleteFacet(f.id, facetResult, imgColorIndices, colorDistances, visitedCache);
                        if (new Date().getTime() - curTime > 500) {
                            curTime = new Date().getTime();
                            yield (0, common_4.delay)(0);
                            if (onUpdate != null) {
                                onUpdate(0.5 * fidx / facetProcessingOrder.length);
                            }
                        }
                    }
                }
                let facetCount = facetResult.facets.filter(f => f != null).length;
                if (facetCount > maximumNumberOfFacets) {
                    console.log(`There are still ${facetCount} facets, more than the maximum of ${maximumNumberOfFacets}. Removing the smallest facets`);
                }
                const startFacetCount = facetCount;
                while (facetCount > maximumNumberOfFacets) {
                    // because facets can be merged, reevaluate the order of facets to make sure the smallest one is removed 
                    // this is slower but more accurate
                    const facetProcessingOrder = facetResult.facets.filter((f) => f != null).slice(0)
                        .sort((a, b) => b.pointCount > a.pointCount ? 1 : (b.pointCount < a.pointCount ? -1 : 0))
                        .map((f) => f.id)
                        .reverse();
                    const facetToRemove = facetResult.facets[facetProcessingOrder[0]];
                    FacetReducer.deleteFacet(facetToRemove.id, facetResult, imgColorIndices, colorDistances, visitedCache);
                    facetCount = facetResult.facets.filter(f => f != null).length;
                    if (new Date().getTime() - curTime > 500) {
                        curTime = new Date().getTime();
                        yield (0, common_4.delay)(0);
                        if (onUpdate != null) {
                            onUpdate(0.5 + 0.5 - (facetCount - maximumNumberOfFacets) / (startFacetCount - maximumNumberOfFacets));
                        }
                    }
                }
                // this.trimFacets(facetResult, imgColorIndices, colorDistances, visitedCache);
                if (onUpdate != null) {
                    onUpdate(1);
                }
            });
        }
        // /**
        //  * Trims facets with narrow paths either horizontally or vertically, potentially splitting the facet into multiple facets
        //  */
        // public static trimFacets(facetResult: FacetResult, imgColorIndices: Uint8Array2D, colorDistances: number[][], visitedArrayCache: BooleanArray2D) {
        //     for (const facet of facetResult.facets) {
        //         if (facet !== null) {
        //             const facetPointsToReallocate: Point[] = [];
        //             for (let y: number = facet.bbox.minY; y <= facet.bbox.maxY; y++) {
        //                 for (let x: number = facet.bbox.minX; x <= facet.bbox.maxX; x++) {
        //                     if (x > 0 && y > 0 && x < facetResult.width - 1 && y < facetResult.height - 1 &&
        //                         facetResult.facetMap.get(x, y) === facet.id) {
        //                         // check if isolated horizontally
        //                         const top = facetResult.facetMap.get(x, y - 1);
        //                         const bottom = facetResult.facetMap.get(x, y + 1);
        //                         if (top !== facet.id && bottom !== facet.id) {
        //                             // . ? .
        //                             // . F .
        //                             // . ? .
        //                             // mark pixel of facet that it should be removed
        //                             facetPointsToReallocate.push(new Point(x, y));
        //                             const closestNeighbour = FacetReducer.getClosestNeighbourForPixel(facet, facetResult, x, y, colorDistances);
        //                             // copy over color of closest neighbour
        //                             imgColorIndices.set(x, y, facetResult.facets[closestNeighbour]!.color);
        //                             console.log("Flagged " + x + "," + y + " to trim");
        //                         }
        //                     }
        //                 }
        //             }
        //             if (facetPointsToReallocate.length > 0) {
        //                 FacetReducer.rebuildForFacetChange(visitedArrayCache, facet, imgColorIndices, facetResult);
        //             }
        //         }
        //     }
        // }
        /**
         * Deletes a facet. All points belonging to the facet are moved to the nearest neighbour facet
         * based on the distance of the neighbour border points. This results in a voronoi like filling in of the
         * void the deletion made
         */
        static deleteFacet(facetIdToRemove, facetResult, imgColorIndices, colorDistances, visitedArrayCache) {
            const facetToRemove = facetResult.facets[facetIdToRemove];
            if (facetToRemove === null) { // already removed
                return;
            }
            if (facetToRemove.neighbourFacetsIsDirty) {
                facetCreator_1.FacetCreator.buildFacetNeighbour(facetToRemove, facetResult);
            }
            if (facetToRemove.neighbourFacets.length > 0) {
                // there are many small facets, it's faster to just iterate over all points within its bounding box
                // and seeing which belong to the facet than to keep track of the inner points (along with the border points)
                // per facet, because that generates a lot of extra heap objects that need to be garbage collected each time
                // a facet is rebuilt
                for (let j = facetToRemove.bbox.minY; j <= facetToRemove.bbox.maxY; j++) {
                    for (let i = facetToRemove.bbox.minX; i <= facetToRemove.bbox.maxX; i++) {
                        if (facetResult.facetMap.get(i, j) === facetToRemove.id) {
                            const closestNeighbour = FacetReducer.getClosestNeighbourForPixel(facetToRemove, facetResult, i, j, colorDistances);
                            if (closestNeighbour !== -1) {
                                // copy over color of closest neighbour
                                imgColorIndices.set(i, j, facetResult.facets[closestNeighbour].color);
                            }
                            else {
                                console.warn(`No closest neighbour found for point ${i},${j}`);
                            }
                        }
                    }
                }
            }
            else {
                console.warn(`Facet ${facetToRemove.id} does not have any neighbours`);
            }
            // Rebuild all the neighbour facets that have been changed. While it could probably be faster by just adding the points manually
            // to the facet map and determine if the border points are still valid, it's more complex than that. It's possible that due to the change in points
            // that 2 neighbours of the same colors have become linked and need to merged as well. So it's easier to just rebuild the entire facet
            FacetReducer.rebuildForFacetChange(visitedArrayCache, facetToRemove, imgColorIndices, facetResult);
            // now mark the facet to remove as deleted
            facetResult.facets[facetToRemove.id] = null;
        }
        static rebuildForFacetChange(visitedArrayCache, facet, imgColorIndices, facetResult) {
            FacetReducer.rebuildChangedNeighbourFacets(visitedArrayCache, facet, imgColorIndices, facetResult);
            // sanity check: make sure that all points have been replaced by neighbour facets. It's possible that some points will have
            // been left out because there is no continuity with the neighbour points
            // this occurs for diagonal points to the neighbours and more often when the closest
            // color is chosen when distances are equal.
            // It's probably possible to enforce that this will never happen in the above code but
            // this is a constraint that is expensive to enforce and doesn't happen all that much
            // so instead try and merge if with any of its direct neighbours if possible
            let needsToRebuild = false;
            for (let y = facet.bbox.minY; y <= facet.bbox.maxY; y++) {
                for (let x = facet.bbox.minX; x <= facet.bbox.maxX; x++) {
                    if (facetResult.facetMap.get(x, y) === facet.id) {
                        console.warn(`Point ${x},${y} was reallocated to neighbours for facet ${facet.id}`);
                        needsToRebuild = true;
                        if (x - 1 >= 0 && facetResult.facetMap.get(x - 1, y) !== facet.id && facetResult.facets[facetResult.facetMap.get(x - 1, y)] !== null) {
                            imgColorIndices.set(x, y, facetResult.facets[facetResult.facetMap.get(x - 1, y)].color);
                        }
                        else if (y - 1 >= 0 && facetResult.facetMap.get(x, y - 1) !== facet.id && facetResult.facets[facetResult.facetMap.get(x, y - 1)] !== null) {
                            imgColorIndices.set(x, y, facetResult.facets[facetResult.facetMap.get(x, y - 1)].color);
                        }
                        else if (x + 1 < facetResult.width && facetResult.facetMap.get(x + 1, y) !== facet.id && facetResult.facets[facetResult.facetMap.get(x + 1, y)] !== null) {
                            imgColorIndices.set(x, y, facetResult.facets[facetResult.facetMap.get(x + 1, y)].color);
                        }
                        else if (y + 1 < facetResult.height && facetResult.facetMap.get(x, y + 1) !== facet.id && facetResult.facets[facetResult.facetMap.get(x, y + 1)] !== null) {
                            imgColorIndices.set(x, y, facetResult.facets[facetResult.facetMap.get(x, y + 1)].color);
                        }
                        else {
                            console.error(`Unable to reallocate point ${x},${y}`);
                        }
                    }
                }
            }
            // now we need to go through the thing again to build facets and update the neighbours
            if (needsToRebuild) {
                FacetReducer.rebuildChangedNeighbourFacets(visitedArrayCache, facet, imgColorIndices, facetResult);
            }
        }
        /**
         * Determines the closest neighbour for a given pixel of a facet, based on the closest distance to the neighbour AND the when tied, the closest color
         */
        static getClosestNeighbourForPixel(facetToRemove, facetResult, x, y, colorDistances) {
            let closestNeighbour = -1;
            let minDistance = Number.MAX_VALUE;
            let minColorDistance = Number.MAX_VALUE;
            // ensure the neighbour facets is up to date if it was marked as dirty
            if (facetToRemove.neighbourFacetsIsDirty) {
                facetCreator_1.FacetCreator.buildFacetNeighbour(facetToRemove, facetResult);
            }
            // determine which neighbour will receive the current point based on the distance, and if there are more with the same
            // distance, then take the neighbour with the closes color
            for (const neighbourIdx of facetToRemove.neighbourFacets) {
                const neighbour = facetResult.facets[neighbourIdx];
                if (neighbour != null) {
                    for (const bpt of neighbour.borderPoints) {
                        const distance = bpt.distanceToCoord(x, y);
                        if (distance < minDistance) {
                            minDistance = distance;
                            closestNeighbour = neighbourIdx;
                            minColorDistance = Number.MAX_VALUE; // reset color distance
                        }
                        else if (distance === minDistance) {
                            // if the distance is equal as the min distance
                            // then see if the neighbour's color is closer to the current color
                            // note: this causes morepoints to be reallocated to different neighbours
                            // in the sanity check later, but still yields a better visual result
                            const colorDistance = colorDistances[facetToRemove.color][neighbour.color];
                            if (colorDistance < minColorDistance) {
                                minColorDistance = colorDistance;
                                closestNeighbour = neighbourIdx;
                            }
                        }
                    }
                }
            }
            return closestNeighbour;
        }
        /**
         *  Rebuilds the given changed facets
         */
        static rebuildChangedNeighbourFacets(visitedArrayCache, facetToRemove, imgColorIndices, facetResult) {
            const changedNeighboursSet = {};
            if (facetToRemove.neighbourFacetsIsDirty) {
                facetCreator_1.FacetCreator.buildFacetNeighbour(facetToRemove, facetResult);
            }
            for (const neighbourIdx of facetToRemove.neighbourFacets) {
                const neighbour = facetResult.facets[neighbourIdx];
                if (neighbour != null) {
                    // re-evaluate facet
                    // track all the facets that needs to have their neighbour list updated, which is also going to be all the neighbours of the neighbours that are being updated
                    changedNeighboursSet[neighbourIdx] = true;
                    if (neighbour.neighbourFacetsIsDirty) {
                        facetCreator_1.FacetCreator.buildFacetNeighbour(neighbour, facetResult);
                    }
                    for (const n of neighbour.neighbourFacets) {
                        changedNeighboursSet[n] = true;
                    }
                    // rebuild the neighbour facet
                    const newFacet = facetCreator_1.FacetCreator.buildFacet(neighbourIdx, neighbour.color, neighbour.borderPoints[0].x, neighbour.borderPoints[0].y, visitedArrayCache, imgColorIndices, facetResult);
                    facetResult.facets[neighbourIdx] = newFacet;
                    // it's possible that any of the neighbour facets are now overlapping
                    // because if for example facet Red - Green - Red, Green is removed
                    // then it will become Red - Red and both facets will overlap
                    // this means the facet will have 0 points remaining
                    if (newFacet.pointCount === 0) {
                        // remove the empty facet as well
                        facetResult.facets[neighbourIdx] = null;
                    }
                }
            }
            // reset the visited array for all neighbours
            // while the visited array could be recreated per facet to remove, it's quite big and introduces
            // a lot of allocation / cleanup overhead. Due to the size of the facets it's usually faster
            // to just flag every point of the facet as false again
            if (facetToRemove.neighbourFacetsIsDirty) {
                facetCreator_1.FacetCreator.buildFacetNeighbour(facetToRemove, facetResult);
            }
            for (const neighbourIdx of facetToRemove.neighbourFacets) {
                const neighbour = facetResult.facets[neighbourIdx];
                if (neighbour != null) {
                    for (let y = neighbour.bbox.minY; y <= neighbour.bbox.maxY; y++) {
                        for (let x = neighbour.bbox.minX; x <= neighbour.bbox.maxX; x++) {
                            if (facetResult.facetMap.get(x, y) === neighbour.id) {
                                visitedArrayCache.set(x, y, false);
                            }
                        }
                    }
                }
            }
            // rebuild neighbour array for affected neighbours
            for (const k of Object.keys(changedNeighboursSet)) {
                if (changedNeighboursSet.hasOwnProperty(k)) {
                    const neighbourIdx = parseInt(k);
                    const f = facetResult.facets[neighbourIdx];
                    if (f != null) {
                        // it's a lot faster when deferring the neighbour array updates
                        // because a lot of facets that are deleted share the same facet neighbours
                        // and removing the unnecessary neighbour array checks until they it's needed
                        // speeds things up significantly
                        // FacetCreator.buildFacetNeighbour(f, facetResult);
                        f.neighbourFacets = null;
                        f.neighbourFacetsIsDirty = true;
                    }
                }
            }
        }
    }
    exports.FacetReducer = FacetReducer;
});
define("facetCreator", ["require", "exports", "common", "lib/fill", "structs/boundingbox", "structs/point", "structs/typedarrays", "facetmanagement"], function (require, exports, common_5, fill_1, boundingbox_1, point_4, typedarrays_4, facetmanagement_3) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetCreator = void 0;
    class FacetCreator {
        /**
         *  Constructs the facets with its border points for each area of pixels of the same color
         */
        static getFacets(width, height, imgColorIndices, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                const result = new facetmanagement_3.FacetResult();
                result.width = width;
                result.height = height;
                // setup visited mask
                const visited = new typedarrays_4.BooleanArray2D(result.width, result.height);
                // setup facet map & array
                result.facetMap = new typedarrays_4.Uint32Array2D(result.width, result.height);
                result.facets = [];
                // depth first traversal to find the different facets
                let count = 0;
                for (let j = 0; j < result.height; j++) {
                    for (let i = 0; i < result.width; i++) {
                        const colorIndex = imgColorIndices.get(i, j);
                        if (!visited.get(i, j)) {
                            const facetIndex = result.facets.length;
                            // build a facet starting at point i,j
                            const facet = FacetCreator.buildFacet(facetIndex, colorIndex, i, j, visited, imgColorIndices, result);
                            result.facets.push(facet);
                            if (count % 100 === 0) {
                                yield (0, common_5.delay)(0);
                                if (onUpdate != null) {
                                    onUpdate(count / (result.width * result.height));
                                }
                            }
                        }
                        count++;
                    }
                }
                yield (0, common_5.delay)(0);
                // fill in the neighbours of all facets by checking the neighbours of the border points
                for (const f of result.facets) {
                    if (f != null) {
                        FacetCreator.buildFacetNeighbour(f, result);
                    }
                }
                if (onUpdate != null) {
                    onUpdate(1);
                }
                return result;
            });
        }
        /**
         *  Builds a facet at given x,y using depth first search to visit all pixels of the same color
         */
        static buildFacet(facetIndex, facetColorIndex, x, y, visited, imgColorIndices, facetResult) {
            const facet = new facetmanagement_3.Facet();
            facet.id = facetIndex;
            facet.color = facetColorIndex;
            facet.bbox = new boundingbox_1.BoundingBox();
            facet.borderPoints = [];
            facet.neighbourFacetsIsDirty = true; // not built neighbours yet
            facet.neighbourFacets = null;
            (0, fill_1.fill)(x, y, facetResult.width, facetResult.height, (ptx, pty) => visited.get(ptx, pty) || imgColorIndices.get(ptx, pty) !== facetColorIndex, (ptx, pty) => {
                visited.set(ptx, pty, true);
                facetResult.facetMap.set(ptx, pty, facetIndex);
                facet.pointCount++;
                // determine if the point is a border or not
                /*  const isInnerPoint = (ptx - 1 >= 0 && imgColorIndices.get(ptx - 1, pty) === facetColorIndex) &&
                      (pty - 1 >= 0 && imgColorIndices.get(ptx, pty - 1) === facetColorIndex) &&
                      (ptx + 1 < facetResult.width && imgColorIndices.get(ptx + 1, pty) === facetColorIndex) &&
                      (pty + 1 < facetResult.height && imgColorIndices.get(ptx, pty + 1) === facetColorIndex);
                */
                const isInnerPoint = imgColorIndices.matchAllAround(ptx, pty, facetColorIndex);
                if (!isInnerPoint) {
                    facet.borderPoints.push(new point_4.Point(ptx, pty));
                }
                // update bounding box of facet
                if (ptx > facet.bbox.maxX) {
                    facet.bbox.maxX = ptx;
                }
                if (pty > facet.bbox.maxY) {
                    facet.bbox.maxY = pty;
                }
                if (ptx < facet.bbox.minX) {
                    facet.bbox.minX = ptx;
                }
                if (pty < facet.bbox.minY) {
                    facet.bbox.minY = pty;
                }
            });
            /*
               // using a 1D flattened stack (x*width+y), we can avoid heap allocations of Point objects, which halves the garbage collection time
             let stack: number[] = [];
             stack.push(y * facetResult.width + x);
    
             while (stack.length > 0) {
                 let pt = stack.pop()!;
                 let ptx = pt % facetResult.width;
                 let pty = Math.floor(pt / facetResult.width);
    
                 // if the point wasn't visited before and matches
                 // the same color
                 if (!visited.get(ptx, pty) &&
                     imgColorIndices.get(ptx, pty) == facetColorIndex) {
    
                     visited.set(ptx, pty, true);
                     facetResult.facetMap.set(ptx, pty, facetIndex);
                     facet.pointCount++;
    
                     // determine if the point is a border or not
                     let isInnerPoint = (ptx - 1 >= 0 && imgColorIndices.get(ptx - 1, pty) == facetColorIndex) &&
                         (pty - 1 >= 0 && imgColorIndices.get(ptx, pty - 1) == facetColorIndex) &&
                         (ptx + 1 < facetResult.width && imgColorIndices.get(ptx + 1, pty) == facetColorIndex) &&
                         (pty + 1 < facetResult.height && imgColorIndices.get(ptx, pty + 1) == facetColorIndex);
    
                     if (!isInnerPoint)
                         facet.borderPoints.push(new Point(ptx, pty));
    
                     // update bounding box of facet
                     if (ptx > facet.bbox.maxX) facet.bbox.maxX = ptx;
                     if (pty > facet.bbox.maxY) facet.bbox.maxY = pty;
                     if (ptx < facet.bbox.minX) facet.bbox.minX = ptx;
                     if (pty < facet.bbox.minY) facet.bbox.minY = pty;
    
                     // visit direct adjacent points
                     if (ptx - 1 >= 0 && !visited.get(ptx - 1, pty))
                         stack.push(pty * facetResult.width + (ptx - 1)); //stack.push(new Point(pt.x - 1, pt.y));
                     if (pty - 1 >= 0 && !visited.get(ptx, pty - 1))
                         stack.push((pty - 1) * facetResult.width + ptx); //stack.push(new Point(pt.x, pt.y - 1));
                     if (ptx + 1 < facetResult.width && !visited.get(ptx + 1, pty))
                         stack.push(pty * facetResult.width + (ptx + 1));//stack.push(new Point(pt.x + 1, pt.y));
                     if (pty + 1 < facetResult.height && !visited.get(ptx, pty + 1))
                         stack.push((pty + 1) * facetResult.width + ptx); //stack.push(new Point(pt.x, pt.y + 1));
                 }
             }
             */
            return facet;
        }
        /**
         * Check which neighbour facets the given facet has by checking the neighbour facets at each border point
         */
        static buildFacetNeighbour(facet, facetResult) {
            facet.neighbourFacets = [];
            const uniqueFacets = {}; // poor man's set
            for (const pt of facet.borderPoints) {
                if (pt.x - 1 >= 0) {
                    const leftFacetId = facetResult.facetMap.get(pt.x - 1, pt.y);
                    if (leftFacetId !== facet.id) {
                        uniqueFacets[leftFacetId] = true;
                    }
                }
                if (pt.y - 1 >= 0) {
                    const topFacetId = facetResult.facetMap.get(pt.x, pt.y - 1);
                    if (topFacetId !== facet.id) {
                        uniqueFacets[topFacetId] = true;
                    }
                }
                if (pt.x + 1 < facetResult.width) {
                    const rightFacetId = facetResult.facetMap.get(pt.x + 1, pt.y);
                    if (rightFacetId !== facet.id) {
                        uniqueFacets[rightFacetId] = true;
                    }
                }
                if (pt.y + 1 < facetResult.height) {
                    const bottomFacetId = facetResult.facetMap.get(pt.x, pt.y + 1);
                    if (bottomFacetId !== facet.id) {
                        uniqueFacets[bottomFacetId] = true;
                    }
                }
            }
            for (const k of Object.keys(uniqueFacets)) {
                if (uniqueFacets.hasOwnProperty(k)) {
                    facet.neighbourFacets.push(parseInt(k));
                }
            }
            // the neighbour array is updated so it's not dirty anymore
            facet.neighbourFacetsIsDirty = false;
        }
    }
    exports.FacetCreator = FacetCreator;
});
define("lib/datastructs", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.PriorityQueue = exports.Map = void 0;
    class Map {
        constructor() {
            this.obj = {};
        }
        containsKey(key) {
            return key in this.obj;
        }
        getKeys() {
            const keys = [];
            for (const el in this.obj) {
                if (this.obj.hasOwnProperty(el)) {
                    keys.push(el);
                }
            }
            return keys;
        }
        get(key) {
            const o = this.obj[key];
            if (typeof o === "undefined") {
                return null;
            }
            else {
                return o;
            }
        }
        put(key, value) {
            this.obj[key] = value;
        }
        remove(key) {
            delete this.obj[key];
        }
        clone() {
            const m = new Map();
            m.obj = {};
            for (const p in this.obj) {
                m.obj[p] = this.obj[p];
            }
            return m;
        }
    }
    exports.Map = Map;
    class Heap {
        constructor() {
            this.array = [];
            this.keyMap = new Map();
        }
        add(obj) {
            if (this.keyMap.containsKey(obj.getKey())) {
                throw new Error("Item with key " + obj.getKey() + " already exists in the heap");
            }
            this.array.push(obj);
            this.keyMap.put(obj.getKey(), this.array.length - 1);
            this.checkParentRequirement(this.array.length - 1);
        }
        replaceAt(idx, newobj) {
            this.array[idx] = newobj;
            this.keyMap.put(newobj.getKey(), idx);
            this.checkParentRequirement(idx);
            this.checkChildrenRequirement(idx);
        }
        shift() {
            return this.removeAt(0);
        }
        remove(obj) {
            const idx = this.keyMap.get(obj.getKey());
            if (idx === -1) {
                return;
            }
            this.removeAt(idx);
        }
        removeWhere(predicate) {
            const itemsToRemove = [];
            for (let i = this.array.length - 1; i >= 0; i--) {
                if (predicate(this.array[i])) {
                    itemsToRemove.push(this.array[i]);
                }
            }
            for (const el of itemsToRemove) {
                this.remove(el);
            }
            for (const el of this.array) {
                if (predicate(el)) {
                    console.log("Idx of element not removed: " + this.keyMap.get(el.getKey()));
                    throw new Error("element not removed: " + el.getKey());
                }
            }
        }
        removeAt(idx) {
            const obj = this.array[idx];
            this.keyMap.remove(obj.getKey());
            const isLastElement = idx === this.array.length - 1;
            if (this.array.length > 0) {
                const newobj = this.array.pop();
                if (!isLastElement && this.array.length > 0) {
                    this.replaceAt(idx, newobj);
                }
            }
            return obj;
        }
        foreach(func) {
            const arr = this.array.sort((e, e2) => e.compareTo(e2));
            for (const el of arr) {
                func(el);
            }
        }
        peek() {
            return this.array[0];
        }
        contains(key) {
            return this.keyMap.containsKey(key);
        }
        at(key) {
            const obj = this.keyMap.get(key);
            if (typeof obj === "undefined") {
                return null;
            }
            else {
                return this.array[obj];
            }
        }
        size() {
            return this.array.length;
        }
        checkHeapRequirement(item) {
            const idx = this.keyMap.get(item.getKey());
            if (idx != null) {
                this.checkParentRequirement(idx);
                this.checkChildrenRequirement(idx);
            }
        }
        checkChildrenRequirement(idx) {
            let stop = false;
            while (!stop) {
                const left = this.getLeftChildIndex(idx);
                let right = left === -1 ? -1 : left + 1;
                if (left === -1) {
                    return;
                }
                if (right >= this.size()) {
                    right = -1;
                }
                let minIdx;
                if (right === -1) {
                    minIdx = left;
                }
                else {
                    minIdx = (this.array[left].compareTo(this.array[right]) < 0) ? left : right;
                }
                if (this.array[idx].compareTo(this.array[minIdx]) > 0) {
                    this.swap(idx, minIdx);
                    idx = minIdx; // iteratively instead of recursion for this.checkChildrenRequirement(minIdx);
                }
                else {
                    stop = true;
                }
            }
        }
        checkParentRequirement(idx) {
            let curIdx = idx;
            let parentIdx = Heap.getParentIndex(curIdx);
            while (parentIdx >= 0 && this.array[parentIdx].compareTo(this.array[curIdx]) > 0) {
                this.swap(curIdx, parentIdx);
                curIdx = parentIdx;
                parentIdx = Heap.getParentIndex(curIdx);
            }
        }
        dump() {
            if (this.size() === 0) {
                return;
            }
            const idx = 0;
            const leftIdx = this.getLeftChildIndex(idx);
            const rightIdx = leftIdx + 1;
            console.log(this.array);
            console.log("--- keymap ---");
            console.log(this.keyMap);
        }
        swap(i, j) {
            this.keyMap.put(this.array[i].getKey(), j);
            this.keyMap.put(this.array[j].getKey(), i);
            const tmp = this.array[i];
            this.array[i] = this.array[j];
            this.array[j] = tmp;
        }
        getLeftChildIndex(curIdx) {
            const idx = ((curIdx + 1) * 2) - 1;
            if (idx >= this.array.length) {
                return -1;
            }
            else {
                return idx;
            }
        }
        static getParentIndex(curIdx) {
            if (curIdx === 0) {
                return -1;
            }
            return Math.floor((curIdx + 1) / 2) - 1;
        }
        clone() {
            const h = new Heap();
            h.array = this.array.slice(0);
            h.keyMap = this.keyMap.clone();
            return h;
        }
    }
    class PriorityQueue {
        constructor() {
            this.heap = new Heap();
        }
        enqueue(obj) {
            this.heap.add(obj);
        }
        peek() {
            return this.heap.peek();
        }
        updatePriority(key) {
            this.heap.checkHeapRequirement(key);
        }
        get(key) {
            return this.heap.at(key);
        }
        get size() {
            return this.heap.size();
        }
        dequeue() {
            return this.heap.shift();
        }
        dump() {
            this.heap.dump();
        }
        contains(key) {
            return this.heap.contains(key);
        }
        removeWhere(predicate) {
            this.heap.removeWhere(predicate);
        }
        foreach(func) {
            this.heap.foreach(func);
        }
        clone() {
            const p = new PriorityQueue();
            p.heap = this.heap.clone();
            return p;
        }
    }
    exports.PriorityQueue = PriorityQueue;
});
define("lib/polylabel", ["require", "exports", "lib/datastructs"], function (require, exports, datastructs_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.pointToPolygonDist = exports.polylabel = void 0;
    function polylabel(polygon, precision = 1.0) {
        // find the bounding box of the outer ring
        let minX = Number.MAX_VALUE;
        let minY = Number.MAX_VALUE;
        let maxX = Number.MIN_VALUE;
        let maxY = Number.MIN_VALUE;
        for (let i = 0; i < polygon[0].length; i++) {
            const p = polygon[0][i];
            if (p.x < minX) {
                minX = p.x;
            }
            if (p.y < minY) {
                minY = p.y;
            }
            if (p.x > maxX) {
                maxX = p.x;
            }
            if (p.y > maxY) {
                maxY = p.y;
            }
        }
        const width = maxX - minX;
        const height = maxY - minY;
        const cellSize = Math.min(width, height);
        let h = cellSize / 2;
        // a priority queue of cells in order of their "potential" (max distance to polygon)
        const cellQueue = new datastructs_1.PriorityQueue();
        if (cellSize === 0) {
            return { pt: { x: minX, y: minY }, distance: 0 };
        }
        // cover polygon with initial cells
        for (let x = minX; x < maxX; x += cellSize) {
            for (let y = minY; y < maxY; y += cellSize) {
                cellQueue.enqueue(new Cell(x + h, y + h, h, polygon));
            }
        }
        // take centroid as the first best guess
        let bestCell = getCentroidCell(polygon);
        // special case for rectangular polygons
        const bboxCell = new Cell(minX + width / 2, minY + height / 2, 0, polygon);
        if (bboxCell.d > bestCell.d) {
            bestCell = bboxCell;
        }
        let numProbes = cellQueue.size;
        while (cellQueue.size > 0) {
            // pick the most promising cell from the queue
            const cell = cellQueue.dequeue();
            // update the best cell if we found a better one
            if (cell.d > bestCell.d) {
                bestCell = cell;
            }
            // do not drill down further if there's no chance of a better solution
            if (cell.max - bestCell.d <= precision) {
                continue;
            }
            // split the cell into four cells
            h = cell.h / 2;
            cellQueue.enqueue(new Cell(cell.x - h, cell.y - h, h, polygon));
            cellQueue.enqueue(new Cell(cell.x + h, cell.y - h, h, polygon));
            cellQueue.enqueue(new Cell(cell.x - h, cell.y + h, h, polygon));
            cellQueue.enqueue(new Cell(cell.x + h, cell.y + h, h, polygon));
            numProbes += 4;
        }
        return { pt: { x: bestCell.x, y: bestCell.y }, distance: bestCell.d };
    }
    exports.polylabel = polylabel;
    class Cell {
        constructor(x, y, h, polygon) {
            this.x = x;
            this.y = y;
            this.h = h;
            this.d = pointToPolygonDist(x, y, polygon);
            this.max = this.d + this.h * Math.SQRT2;
        }
        compareTo(other) {
            return other.max - this.max;
        }
        getKey() {
            return this.x + "," + this.y;
        }
    }
    // get squared distance from a point px,py to a segment [a-b]
    function getSegDistSq(px, py, a, b) {
        let x = a.x;
        let y = a.y;
        let dx = b.x - x;
        let dy = b.y - y;
        if (dx !== 0 || dy !== 0) {
            const t = ((px - x) * dx + (py - y) * dy) / (dx * dx + dy * dy);
            if (t > 1) {
                x = b.x;
                y = b.y;
            }
            else if (t > 0) {
                x += dx * t;
                y += dy * t;
            }
        }
        dx = px - x;
        dy = py - y;
        return dx * dx + dy * dy;
    }
    /**
     * Signed distance from point to polygon outline (negative if point is outside)
     */
    function pointToPolygonDist(x, y, polygon) {
        let inside = false;
        let minDistSq = Infinity;
        for (let k = 0; k < polygon.length; k++) {
            const ring = polygon[k];
            for (let i = 0, len = ring.length, j = len - 1; i < len; j = i++) {
                const a = ring[i];
                const b = ring[j];
                if ((a.y > y !== b.y > y) &&
                    (x < (b.x - a.x) * (y - a.y) / (b.y - a.y) + a.x)) {
                    inside = !inside;
                }
                minDistSq = Math.min(minDistSq, getSegDistSq(x, y, a, b));
            }
        }
        return (inside ? 1 : -1) * Math.sqrt(minDistSq);
    }
    exports.pointToPolygonDist = pointToPolygonDist;
    // get polygon centroid
    function getCentroidCell(polygon) {
        let area = 0;
        let x = 0;
        let y = 0;
        const points = polygon[0];
        for (let i = 0, len = points.length, j = len - 1; i < len; j = i++) {
            const a = points[i];
            const b = points[j];
            const f = a.x * b.y - b.x * a.y;
            x += (a.x + b.x) * f;
            y += (a.y + b.y) * f;
            area += f * 3;
        }
        if (area === 0) {
            return new Cell(points[0].x, points[0].y, 0, polygon);
        }
        return new Cell(x / area, y / area, 0, polygon);
    }
});
define("facetLabelPlacer", ["require", "exports", "common", "lib/polylabel", "structs/boundingbox", "facetCreator"], function (require, exports, common_6, polylabel_1, boundingbox_2, facetCreator_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.FacetLabelPlacer = void 0;
    class FacetLabelPlacer {
        /**
         *  Determines where to place the labels for each facet. This is done by calculating where
         *  in the polygon the largest circle can be contained, also called the pole of inaccessibility
         *  That's the spot where there will be the most room for the label.
         *  One tricky gotcha: neighbour facets can lay completely inside other facets and can overlap the label
         *  if only the outer border of the facet is taken in account. This is solved by adding the neighbours facet polygon that fall
         *  within the facet as additional polygon rings (why does everything look so easy to do yet never is under the hood :/)
         */
        static buildFacetLabelBounds(facetResult, onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                let count = 0;
                for (const f of facetResult.facets) {
                    if (f != null) {
                        const polyRings = [];
                        // get the border path from the segments (that can have been reduced compared to facet actual border path)
                        const borderPath = f.getFullPathFromBorderSegments(true);
                        // outer path must be first ring
                        polyRings.push(borderPath);
                        const onlyOuterRing = [borderPath];
                        // now add all the neighbours of the facet as "inner" rings,
                        // regardless if they are inner or not. These are seen as areas where the label
                        // cannot be placed
                        if (f.neighbourFacetsIsDirty) {
                            facetCreator_2.FacetCreator.buildFacetNeighbour(f, facetResult);
                        }
                        for (const neighbourIdx of f.neighbourFacets) {
                            const neighbourPath = facetResult.facets[neighbourIdx].getFullPathFromBorderSegments(true);
                            const fallsInside = FacetLabelPlacer.doesNeighbourFallInsideInCurrentFacet(neighbourPath, f, onlyOuterRing);
                            if (fallsInside) {
                                polyRings.push(neighbourPath);
                            }
                        }
                        const result = (0, polylabel_1.polylabel)(polyRings);
                        f.labelBounds = new boundingbox_2.BoundingBox();
                        // determine inner square within the circle
                        const innerPadding = 2 * Math.sqrt(2 * result.distance);
                        f.labelBounds.minX = result.pt.x - innerPadding;
                        f.labelBounds.maxX = result.pt.x + innerPadding;
                        f.labelBounds.minY = result.pt.y - innerPadding;
                        f.labelBounds.maxY = result.pt.y + innerPadding;
                        if (count % 100 === 0) {
                            yield (0, common_6.delay)(0);
                            if (onUpdate != null) {
                                onUpdate(f.id / facetResult.facets.length);
                            }
                        }
                    }
                    count++;
                }
                if (onUpdate != null) {
                    onUpdate(1);
                }
            });
        }
        /**
         *  Checks whether a neighbour border path is fully within the current facet border path
         */
        static doesNeighbourFallInsideInCurrentFacet(neighbourPath, f, onlyOuterRing) {
            let fallsInside = true;
            // fast test to see if the neighbour falls inside the bbox of the facet
            for (let i = 0; i < neighbourPath.length && fallsInside; i++) {
                if (neighbourPath[i].x >= f.bbox.minX && neighbourPath[i].x <= f.bbox.maxX &&
                    neighbourPath[i].y >= f.bbox.minY && neighbourPath[i].y <= f.bbox.maxY) {
                    // ok
                }
                else {
                    fallsInside = false;
                }
            }
            if (fallsInside) {
                // do a more fine grained but more expensive check to see if each of the points fall within the polygon
                for (let i = 0; i < neighbourPath.length && fallsInside; i++) {
                    const distance = (0, polylabel_1.pointToPolygonDist)(neighbourPath[i].x, neighbourPath[i].y, onlyOuterRing);
                    if (distance < 0) {
                        // falls outside
                        fallsInside = false;
                    }
                }
            }
            return fallsInside;
        }
    }
    exports.FacetLabelPlacer = FacetLabelPlacer;
});
/**
 * Module that manages the GUI when processing
 */
define("guiprocessmanager", ["require", "exports", "colorreductionmanagement", "common", "facetBorderSegmenter", "facetBorderTracer", "facetCreator", "facetLabelPlacer", "facetmanagement", "facetReducer", "gui", "structs/point"], function (require, exports, colorreductionmanagement_2, common_7, facetBorderSegmenter_1, facetBorderTracer_1, facetCreator_3, facetLabelPlacer_1, facetmanagement_4, facetReducer_1, gui_1, point_5) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.GUIProcessManager = exports.ProcessResult = void 0;
    class ProcessResult {
    }
    exports.ProcessResult = ProcessResult;
    /**
     *  Manages the GUI states & processes the image step by step
     */
    class GUIProcessManager {
        static process(settings, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                const c = document.getElementById("canvas");
                const ctx = c.getContext("2d");
                let imgData = ctx.getImageData(0, 0, c.width, c.height);
                if (settings.resizeImageIfTooLarge && (c.width > settings.resizeImageWidth || c.height > settings.resizeImageHeight)) {
                    let width = c.width;
                    let height = c.height;
                    if (width > settings.resizeImageWidth) {
                        const newWidth = settings.resizeImageWidth;
                        const newHeight = c.height / c.width * settings.resizeImageWidth;
                        width = newWidth;
                        height = newHeight;
                    }
                    if (height > settings.resizeImageHeight) {
                        const newHeight = settings.resizeImageHeight;
                        const newWidth = width / height * newHeight;
                        width = newWidth;
                        height = newHeight;
                    }
                    const tempCanvas = document.createElement("canvas");
                    tempCanvas.width = width;
                    tempCanvas.height = height;
                    tempCanvas.getContext("2d").drawImage(c, 0, 0, width, height);
                    c.width = width;
                    c.height = height;
                    ctx.drawImage(tempCanvas, 0, 0, width, height);
                    imgData = ctx.getImageData(0, 0, c.width, c.height);
                }
                // reset progress
                $(".status .progress .determinate").css("width", "0px");
                $(".status").removeClass("complete");
                const tabsOutput = M.Tabs.getInstance(document.getElementById("tabsOutput"));
                // k-means clustering
                const kmeansImgData = yield GUIProcessManager.processKmeansClustering(imgData, tabsOutput, ctx, settings, cancellationToken);
                let facetResult = new facetmanagement_4.FacetResult();
                let colormapResult = new colorreductionmanagement_2.ColorMapResult();
                // build color map
                colormapResult = colorreductionmanagement_2.ColorReducer.createColorMap(kmeansImgData);
                if (settings.narrowPixelStripCleanupRuns === 0) {
                    // facet building
                    facetResult = yield GUIProcessManager.processFacetBuilding(colormapResult, cancellationToken);
                    // facet reduction
                    yield GUIProcessManager.processFacetReduction(facetResult, tabsOutput, settings, colormapResult, cancellationToken);
                }
                else {
                    for (let run = 0; run < settings.narrowPixelStripCleanupRuns; run++) {
                        // clean up narrow pixel strips
                        yield colorreductionmanagement_2.ColorReducer.processNarrowPixelStripCleanup(colormapResult);
                        // facet building
                        facetResult = yield GUIProcessManager.processFacetBuilding(colormapResult, cancellationToken);
                        // facet reduction
                        yield GUIProcessManager.processFacetReduction(facetResult, tabsOutput, settings, colormapResult, cancellationToken);
                        // the colormapResult.imgColorIndices get updated as the facets are reduced, so just do a few runs of pixel cleanup
                    }
                }
                // facet border tracing
                yield GUIProcessManager.processFacetBorderTracing(tabsOutput, facetResult, cancellationToken);
                // facet border segmentation
                const cBorderSegment = yield GUIProcessManager.processFacetBorderSegmentation(facetResult, tabsOutput, settings, cancellationToken);
                // facet label placement
                yield GUIProcessManager.processFacetLabelPlacement(facetResult, cBorderSegment, tabsOutput, cancellationToken);
                // LACI: címkék rendezése, szűrése (csak lite)
                let colorPointCountMap = new Map();
                for (const f of facetResult.facets) {
                    if (!f) {
                        continue;
                    }
                    if (colorPointCountMap.has(f.color)) {
                        const oldPointCount = colorPointCountMap.get(f.color);
                        colorPointCountMap.set(f.color, oldPointCount || 0 + f.pointCount);
                    }
                    else {
                        colorPointCountMap.set(f.color, f.pointCount);
                    }
                }
                colorPointCountMap = new Map([...colorPointCountMap.entries()].sort((a, b) => b[1] - a[1]));
                const frequencyOrderMap = new Map();
                const rgbsByFrequency = [];
                let orderCounter = 1;
                for (const color of colorPointCountMap.keys()) {
                    frequencyOrderMap.set(color, orderCounter);
                    orderCounter++;
                }
                for (const f of facetResult.facets) {
                    if (!f) {
                        continue;
                    }
                    f.frequencyOrder = frequencyOrderMap.get(f.color);
                    const rgb = colormapResult.colorsByIndex[f.color];
                    rgbsByFrequency[f.frequencyOrder] = rgb;
                }
                // eddig
                // everything is now ready to generate the SVG, return the result
                const processResult = new ProcessResult();
                processResult.facetResult = facetResult;
                processResult.colorsByIndex = colormapResult.colorsByIndex;
                processResult.rgbsByFrequency = rgbsByFrequency;
                return processResult;
            });
        }
        static processKmeansClustering(imgData, tabsOutput, ctx, settings, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("K-means clustering");
                const cKmeans = document.getElementById("cKMeans");
                cKmeans.width = imgData.width;
                cKmeans.height = imgData.height;
                const ctxKmeans = cKmeans.getContext("2d");
                ctxKmeans.fillStyle = "white";
                ctxKmeans.fillRect(0, 0, cKmeans.width, cKmeans.height);
                const kmeansImgData = ctxKmeans.getImageData(0, 0, cKmeans.width, cKmeans.height);
                tabsOutput.select("kmeans-pane");
                $(".status.kMeans").addClass("active");
                yield colorreductionmanagement_2.ColorReducer.applyKMeansClustering(imgData, kmeansImgData, ctx, settings, (kmeans) => {
                    const progress = (100 - (kmeans.currentDeltaDistanceDifference > 100 ? 100 : kmeans.currentDeltaDistanceDifference)) / 100;
                    $("#statusKMeans").css("width", Math.round(progress * 100) + "%");
                    ctxKmeans.putImageData(kmeansImgData, 0, 0);
                    console.log(kmeans.currentDeltaDistanceDifference);
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                });
                $(".status").removeClass("active");
                $(".status.kMeans").addClass("complete");
                (0, gui_1.timeEnd)("K-means clustering");
                return kmeansImgData;
            });
        }
        static processFacetBuilding(colormapResult, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("Facet building");
                $(".status.facetBuilding").addClass("active");
                const facetResult = yield facetCreator_3.FacetCreator.getFacets(colormapResult.width, colormapResult.height, colormapResult.imgColorIndices, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    $("#statusFacetBuilding").css("width", Math.round(progress * 100) + "%");
                });
                $(".status").removeClass("active");
                $(".status.facetBuilding").addClass("complete");
                (0, gui_1.timeEnd)("Facet building");
                return facetResult;
            });
        }
        static processFacetReduction(facetResult, tabsOutput, settings, colormapResult, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("Facet reduction");
                const cReduction = document.getElementById("cReduction");
                cReduction.width = facetResult.width;
                cReduction.height = facetResult.height;
                const ctxReduction = cReduction.getContext("2d");
                ctxReduction.fillStyle = "white";
                ctxReduction.fillRect(0, 0, cReduction.width, cReduction.height);
                const reductionImgData = ctxReduction.getImageData(0, 0, cReduction.width, cReduction.height);
                tabsOutput.select("reduction-pane");
                $(".status.facetReduction").addClass("active");
                yield facetReducer_1.FacetReducer.reduceFacets(settings.removeFacetsSmallerThanNrOfPoints, settings.removeFacetsFromLargeToSmall, settings.maximumNumberOfFacets, colormapResult.colorsByIndex, facetResult, colormapResult.imgColorIndices, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    // update status & image
                    $("#statusFacetReduction").css("width", Math.round(progress * 100) + "%");
                    let idx = 0;
                    for (let j = 0; j < facetResult.height; j++) {
                        for (let i = 0; i < facetResult.width; i++) {
                            const facet = facetResult.facets[facetResult.facetMap.get(i, j)];
                            const rgb = colormapResult.colorsByIndex[facet.color];
                            reductionImgData.data[idx++] = rgb[0];
                            reductionImgData.data[idx++] = rgb[1];
                            reductionImgData.data[idx++] = rgb[2];
                            idx++;
                        }
                    }
                    ctxReduction.putImageData(reductionImgData, 0, 0);
                });
                $(".status").removeClass("active");
                $(".status.facetReduction").addClass("complete");
                (0, gui_1.timeEnd)("Facet reduction");
            });
        }
        static processFacetBorderTracing(tabsOutput, facetResult, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("Facet border tracing");
                tabsOutput.select("borderpath-pane");
                const cBorderPath = document.getElementById("cBorderPath");
                cBorderPath.width = facetResult.width;
                cBorderPath.height = facetResult.height;
                const ctxBorderPath = cBorderPath.getContext("2d");
                $(".status.facetBorderPath").addClass("active");
                yield facetBorderTracer_1.FacetBorderTracer.buildFacetBorderPaths(facetResult, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    // update status & image
                    $("#statusFacetBorderPath").css("width", Math.round(progress * 100) + "%");
                    ctxBorderPath.fillStyle = "white";
                    ctxBorderPath.fillRect(0, 0, cBorderPath.width, cBorderPath.height);
                    for (const f of facetResult.facets) {
                        if (f != null && f.borderPath != null) {
                            ctxBorderPath.beginPath();
                            ctxBorderPath.moveTo(f.borderPath[0].getWallX(), f.borderPath[0].getWallY());
                            for (let i = 1; i < f.borderPath.length; i++) {
                                ctxBorderPath.lineTo(f.borderPath[i].getWallX(), f.borderPath[i].getWallY());
                            }
                            ctxBorderPath.stroke();
                        }
                    }
                });
                $(".status").removeClass("active");
                $(".status.facetBorderPath").addClass("complete");
                (0, gui_1.timeEnd)("Facet border tracing");
            });
        }
        static processFacetBorderSegmentation(facetResult, tabsOutput, settings, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("Facet border segmentation");
                const cBorderSegment = document.getElementById("cBorderSegmentation");
                cBorderSegment.width = facetResult.width;
                cBorderSegment.height = facetResult.height;
                const ctxBorderSegment = cBorderSegment.getContext("2d");
                tabsOutput.select("bordersegmentation-pane");
                $(".status.facetBorderSegmentation").addClass("active");
                yield facetBorderSegmenter_1.FacetBorderSegmenter.buildFacetBorderSegments(facetResult, settings.nrOfTimesToHalveBorderSegments, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    // update status & image
                    $("#statusFacetBorderSegmentation").css("width", Math.round(progress * 100) + "%");
                    ctxBorderSegment.fillStyle = "white";
                    ctxBorderSegment.fillRect(0, 0, cBorderSegment.width, cBorderSegment.height);
                    for (const f of facetResult.facets) {
                        if (f != null && progress > f.id / facetResult.facets.length) {
                            ctxBorderSegment.beginPath();
                            const path = f.getFullPathFromBorderSegments(false);
                            ctxBorderSegment.moveTo(path[0].x, path[0].y);
                            for (let i = 1; i < path.length; i++) {
                                ctxBorderSegment.lineTo(path[i].x, path[i].y);
                            }
                            ctxBorderSegment.stroke();
                        }
                    }
                });
                $(".status").removeClass("active");
                $(".status.facetBorderSegmentation").addClass("complete");
                (0, gui_1.timeEnd)("Facet border segmentation");
                return cBorderSegment;
            });
        }
        static processFacetLabelPlacement(facetResult, cBorderSegment, tabsOutput, cancellationToken) {
            return __awaiter(this, void 0, void 0, function* () {
                (0, gui_1.time)("Facet label placement");
                const cLabelPlacement = document.getElementById("cLabelPlacement");
                cLabelPlacement.width = facetResult.width;
                cLabelPlacement.height = facetResult.height;
                const ctxLabelPlacement = cLabelPlacement.getContext("2d");
                ctxLabelPlacement.fillStyle = "white";
                ctxLabelPlacement.fillRect(0, 0, cBorderSegment.width, cBorderSegment.height);
                ctxLabelPlacement.drawImage(cBorderSegment, 0, 0);
                tabsOutput.select("labelplacement-pane");
                $(".status.facetLabelPlacement").addClass("active");
                yield facetLabelPlacer_1.FacetLabelPlacer.buildFacetLabelBounds(facetResult, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    // update status & image
                    $("#statusFacetLabelPlacement").css("width", Math.round(progress * 100) + "%");
                    for (const f of facetResult.facets) {
                        if (f != null && f.labelBounds != null) {
                            ctxLabelPlacement.fillStyle = "red";
                            ctxLabelPlacement.fillRect(f.labelBounds.minX, f.labelBounds.minY, f.labelBounds.width, f.labelBounds.height);
                        }
                    }
                });
                $(".status").removeClass("active");
                $(".status.facetLabelPlacement").addClass("complete");
                (0, gui_1.timeEnd)("Facet label placement");
            });
        }
        /**
         *  Creates a vector based SVG image of the facets with the given configuration
         */
        static createSVG(facetResult, colorsByIndex, sizeMultiplier, fill, stroke, addColorLabels, fontSize = 50, fontColor = "black", onUpdate = null) {
            return __awaiter(this, void 0, void 0, function* () {
                const xmlns = "http://www.w3.org/2000/svg";
                const svg = document.createElementNS(xmlns, "svg");
                svg.setAttribute("width", sizeMultiplier * facetResult.width + "");
                svg.setAttribute("height", sizeMultiplier * facetResult.height + "");
                let count = 0;
                for (const f of facetResult.facets) {
                    if (f != null && f.borderSegments.length > 0) {
                        let newpath = [];
                        const useSegments = true;
                        if (useSegments) {
                            newpath = f.getFullPathFromBorderSegments(false);
                            // shift from wall coordinates to pixel centers
                            /*for (const p of newpath) {
                                p.x+=0.5;
                                p.y+=0.5;
                            }*/
                        }
                        else {
                            for (let i = 0; i < f.borderPath.length; i++) {
                                newpath.push(new point_5.Point(f.borderPath[i].getWallX() + 0.5, f.borderPath[i].getWallY() + 0.5));
                            }
                        }
                        if (newpath[0].x !== newpath[newpath.length - 1].x || newpath[0].y !== newpath[newpath.length - 1].y) {
                            newpath.push(newpath[0]);
                        } // close loop if necessary
                        // Create a path in SVG's namespace
                        // using quadratic curve absolute positions
                        const svgPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
                        let data = "M ";
                        data += newpath[0].x * sizeMultiplier + " " + newpath[0].y * sizeMultiplier + " ";
                        for (let i = 1; i < newpath.length; i++) {
                            const midpointX = (newpath[i].x + newpath[i - 1].x) / 2;
                            const midpointY = (newpath[i].y + newpath[i - 1].y) / 2;
                            data += "Q " + (midpointX * sizeMultiplier) + " " + (midpointY * sizeMultiplier) + " " + (newpath[i].x * sizeMultiplier) + " " + (newpath[i].y * sizeMultiplier) + " ";
                            // data += "L " + (newpath[i].x * sizeMultiplier) + " " + (newpath[i].y * sizeMultiplier) + " ";
                        }
                        data += "Z";
                        svgPath.setAttribute("data-facetId", f.id + "");
                        // Set path's data
                        svgPath.setAttribute("d", data);
                        if (stroke) {
                            svgPath.style.stroke = "#000";
                        }
                        else {
                            // make the border the same color as the fill color if there is no border stroke
                            // to not have gaps in between facets
                            if (fill) {
                                svgPath.style.stroke = `rgb(${colorsByIndex[f.color][0]},${colorsByIndex[f.color][1]},${colorsByIndex[f.color][2]})`;
                            }
                        }
                        svgPath.style.strokeWidth = "0.5px"; // Set stroke width
                        if (fill) {
                            svgPath.style.fill = `rgb(${colorsByIndex[f.color][0]},${colorsByIndex[f.color][1]},${colorsByIndex[f.color][2]})`;
                        }
                        else {
                            svgPath.style.fill = "none";
                        }
                        svg.appendChild(svgPath);
                        /*  for (const seg of f.borderSegments) {
                              const svgSegPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
                              let segData = "M ";
                              const segPoints = seg.originalSegment.points;
                              segData += segPoints[0].x * sizeMultiplier + " " + segPoints[0].y * sizeMultiplier + " ";
                              for (let i: number = 1; i < segPoints.length; i++) {
                                  const midpointX = (segPoints[i].x + segPoints[i - 1].x) / 2;
                                  const midpointY = (segPoints[i].y + segPoints[i - 1].y) / 2;
                                  //data += "Q " + (midpointX * sizeMultiplier) + " " + (midpointY * sizeMultiplier) + " " + (newpath[i].x * sizeMultiplier) + " " + (newpath[i].y * sizeMultiplier) + " ";
                                  segData += "L " + (segPoints[i].x * sizeMultiplier) + " " + (segPoints[i].y * sizeMultiplier) + " ";
                              }
        
                              console.log("Facet " + f.id + ", segment " + segPoints[0].x + "," + segPoints[0].y + " -> " + segPoints[segPoints.length-1].x + "," +  segPoints[segPoints.length-1].y);
        
                              svgSegPath.setAttribute("data-segmentFacet", f.id + "");
                              // Set path's data
                              svgSegPath.setAttribute("d", segData);
                              svgSegPath.style.stroke = "#FF0";
                              svgSegPath.style.fill = "none";
                              svg.appendChild(svgSegPath);
                          }
                          */
                        // add the color labels if necessary. I mean, this is the whole idea behind the paint by numbers part
                        // so I don't know why you would hide them
                        if (addColorLabels) {
                            const txt = document.createElementNS(xmlns, "text");
                            txt.setAttribute("font-family", "Tahoma");
                            const nrOfDigits = (f.color + "").length;
                            txt.setAttribute("font-size", (fontSize / nrOfDigits) + "");
                            txt.setAttribute("dominant-baseline", "middle");
                            txt.setAttribute("text-anchor", "middle");
                            txt.setAttribute("fill", fontColor);
                            txt.textContent = f.frequencyOrder + "";
                            const subsvg = document.createElementNS(xmlns, "svg");
                            subsvg.setAttribute("width", f.labelBounds.width * sizeMultiplier + "");
                            subsvg.setAttribute("height", f.labelBounds.height * sizeMultiplier + "");
                            subsvg.setAttribute("overflow", "visible");
                            subsvg.setAttribute("viewBox", "-50 -50 100 100");
                            subsvg.setAttribute("preserveAspectRatio", "xMidYMid meet");
                            subsvg.appendChild(txt);
                            const g = document.createElementNS(xmlns, "g");
                            g.setAttribute("class", "label");
                            g.setAttribute("transform", "translate(" + f.labelBounds.minX * sizeMultiplier + "," + f.labelBounds.minY * sizeMultiplier + ")");
                            g.appendChild(subsvg);
                            svg.appendChild(g);
                        }
                        if (count % 100 === 0) {
                            yield (0, common_7.delay)(0);
                            if (onUpdate != null) {
                                onUpdate(f.id / facetResult.facets.length);
                            }
                        }
                    }
                    count++;
                }
                if (onUpdate != null) {
                    onUpdate(1);
                }
                return svg;
            });
        }
    }
    exports.GUIProcessManager = GUIProcessManager;
});
/**
 * Module that provides function the GUI uses and updates the DOM accordingly
 */
define("gui", ["require", "exports", "common", "guiprocessmanager", "settings"], function (require, exports, common_8, guiprocessmanager_1, settings_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.loadExample = exports.downloadSVG = exports.downloadPNG = exports.downloadPalettePng = exports.updateOutput = exports.process = exports.setUploadedFilename = exports.parseSettings = exports.log = exports.timeEnd = exports.time = void 0;
    let uploadedFilename = '';
    let processResult = null;
    let cancellationToken = new common_8.CancellationToken();
    const timers = {};
    function time(name) {
        console.time(name);
        timers[name] = new Date();
    }
    exports.time = time;
    function timeEnd(name) {
        console.timeEnd(name);
        const ms = new Date().getTime() - timers[name].getTime();
        log(name + ": " + ms + "ms");
        delete timers[name];
    }
    exports.timeEnd = timeEnd;
    function log(str) {
        $("#log").append("<br/><span>" + str + "</span>");
    }
    exports.log = log;
    function parseSettings() {
        const settings = new settings_2.Settings();
        if ($("#optColorSpaceRGB").prop("checked")) {
            settings.kMeansClusteringColorSpace = settings_2.ClusteringColorSpace.RGB;
        }
        else if ($("#optColorSpaceHSL").prop("checked")) {
            settings.kMeansClusteringColorSpace = settings_2.ClusteringColorSpace.HSL;
        }
        else if ($("#optColorSpaceRGB").prop("checked")) {
            settings.kMeansClusteringColorSpace = settings_2.ClusteringColorSpace.LAB;
        }
        if ($("#optFacetRemovalLargestToSmallest").prop("checked")) {
            settings.removeFacetsFromLargeToSmall = true;
        }
        else {
            settings.removeFacetsFromLargeToSmall = false;
        }
        settings.randomSeed = parseInt($("#txtRandomSeed").val() + "");
        settings.kMeansNrOfClusters = parseInt($("#txtNrOfClusters").val() + "");
        settings.kMeansMinDeltaDifference = parseFloat($("#txtClusterPrecision").val() + "");
        settings.removeFacetsSmallerThanNrOfPoints = parseInt($("#txtRemoveFacetsSmallerThan").val() + "");
        settings.maximumNumberOfFacets = parseInt($("#txtMaximumNumberOfFacets").val() + "");
        settings.nrOfTimesToHalveBorderSegments = parseInt($("#txtNrOfTimesToHalveBorderSegments").val() + "");
        settings.narrowPixelStripCleanupRuns = parseInt($("#txtNarrowPixelStripCleanupRuns").val() + "");
        settings.resizeImageIfTooLarge = $("#chkResizeImage").prop("checked");
        settings.resizeImageWidth = parseInt($("#txtResizeWidth").val() + "");
        settings.resizeImageHeight = parseInt($("#txtResizeHeight").val() + "");
        const restrictedColorLines = ($("#txtKMeansColorRestrictions").val() + "").split("\n");
        for (const line of restrictedColorLines) {
            const tline = line.trim();
            if (tline.indexOf("//") === 0) {
                // comment, skip
            }
            else {
                const rgbparts = tline.split(",");
                if (rgbparts.length === 3) {
                    let red = parseInt(rgbparts[0]);
                    let green = parseInt(rgbparts[1]);
                    let blue = parseInt(rgbparts[2]);
                    if (red < 0)
                        red = 0;
                    if (red > 255)
                        red = 255;
                    if (green < 0)
                        green = 0;
                    if (green > 255)
                        green = 255;
                    if (blue < 0)
                        blue = 0;
                    if (blue > 255)
                        blue = 255;
                    if (!isNaN(red) && !isNaN(green) && !isNaN(blue)) {
                        settings.kMeansColorRestrictions.push([red, green, blue]);
                    }
                }
            }
        }
        return settings;
    }
    exports.parseSettings = parseSettings;
    function setUploadedFilename(filename) {
        uploadedFilename = filename;
    }
    exports.setUploadedFilename = setUploadedFilename;
    function process() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const settings = parseSettings();
                // cancel old process & create new
                cancellationToken.isCancelled = true;
                cancellationToken = new common_8.CancellationToken();
                processResult = yield guiprocessmanager_1.GUIProcessManager.process(settings, cancellationToken);
                yield updateOutput();
                const tabsOutput = M.Tabs.getInstance(document.getElementById("tabsOutput"));
                tabsOutput.select("output-pane");
            }
            catch (e) {
                log("Error: " + e.message + " at " + e.stack);
            }
        });
    }
    exports.process = process;
    function updateOutput() {
        return __awaiter(this, void 0, void 0, function* () {
            if (processResult != null) {
                const showLabels = $("#chkShowLabels").prop("checked");
                const fill = $("#chkFillFacets").prop("checked");
                const stroke = $("#chkShowBorders").prop("checked");
                const sizeMultiplier = parseInt($("#txtSizeMultiplier").val() + "");
                const fontSize = parseInt($("#txtLabelFontSize").val() + "");
                const fontColor = $("#txtLabelFontColor").val() + "";
                $("#statusSVGGenerate").css("width", "0%");
                $(".status.SVGGenerate").removeClass("complete");
                $(".status.SVGGenerate").addClass("active");
                const svg = yield guiprocessmanager_1.GUIProcessManager.createSVG(processResult.facetResult, processResult.colorsByIndex, sizeMultiplier, fill, stroke, showLabels, fontSize, fontColor, (progress) => {
                    if (cancellationToken.isCancelled) {
                        throw new Error("Cancelled");
                    }
                    $("#statusSVGGenerate").css("width", Math.round(progress * 100) + "%");
                });
                console.log('updateOutput done 0');
                $("#svgContainer").empty().append(svg);
                console.log('updateOutput done 1');
                $("#palette").empty().append(createPaletteHtml(processResult.rgbsByFrequency));
                console.log('updateOutput done 2');
                $("#palette .color").tooltip();
                console.log('updateOutput done 3');
                $(".status").removeClass("active");
                console.log('updateOutput done 4');
                $(".status.SVGGenerate").addClass("complete");
                console.log('updateOutput done');
            }
        });
    }
    exports.updateOutput = updateOutput;
    function createPaletteHtml(rgbsByFrequency) {
        let html = "";
        // LACI: color megnövelve eggyel, hogy ne nulláról kezdődjön
        for (let c = 1; c < rgbsByFrequency.length; c++) {
            const style = "background-color: " + `rgb(${rgbsByFrequency[c][0]},${rgbsByFrequency[c][1]},${rgbsByFrequency[c][2]})`;
            html += `<div class="color" class="tooltipped" style="${style}" data-tooltip="${rgbsByFrequency[c][0]},${rgbsByFrequency[c][1]},${rgbsByFrequency[c][2]}">${c}</div>`;
        }
        return $(html);
    }
    function downloadPalettePng() {
        if (processResult == null) {
            return;
        }
        // LACI: colorsByIndex átírva rgbsByFrequency-re
        const rgbsByFrequency = processResult.rgbsByFrequency;
        const canvas = document.createElement("canvas");
        const nrOfItemsPerRow = 10;
        const nrRows = Math.ceil(rgbsByFrequency.length / nrOfItemsPerRow);
        const margin = 10;
        const cellWidth = 80;
        const cellHeight = 70;
        canvas.width = margin + nrOfItemsPerRow * (cellWidth + margin);
        canvas.height = margin + nrRows * (cellHeight + margin);
        const ctx = canvas.getContext("2d");
        ctx.translate(0.5, 0.5);
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        for (let i = 1; i < rgbsByFrequency.length; i++) {
            const color = rgbsByFrequency[i];
            const x = margin + (i % nrOfItemsPerRow) * (cellWidth + margin);
            const y = margin + Math.floor(i / nrOfItemsPerRow) * (cellHeight + margin);
            ctx.fillStyle = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;
            ctx.fillRect(x, y, cellWidth, cellHeight - 20);
            ctx.strokeStyle = "#888";
            ctx.strokeRect(x, y, cellWidth, cellHeight - 20);
            const nrText = i + "";
            ctx.fillStyle = "black";
            ctx.strokeStyle = "#CCC";
            ctx.font = "20px Tahoma";
            const nrTextSize = ctx.measureText(nrText);
            ctx.lineWidth = 2;
            ctx.strokeText(nrText, x + cellWidth / 2 - nrTextSize.width / 2, y + cellHeight / 2 - 5);
            ctx.fillText(nrText, x + cellWidth / 2 - nrTextSize.width / 2, y + cellHeight / 2 - 5);
            ctx.lineWidth = 1;
            ctx.font = "10px Tahoma";
            const rgbText = "RGB: " + Math.floor(color[0]) + "," + Math.floor(color[1]) + "," + Math.floor(color[2]);
            const rgbTextSize = ctx.measureText(rgbText);
            ctx.fillStyle = "black";
            ctx.fillText(rgbText, x + cellWidth / 2 - rgbTextSize.width / 2, y + cellHeight - 10);
        }
        const dataURL = canvas.toDataURL("image/png");
        const dl = document.createElement("a");
        document.body.appendChild(dl);
        dl.setAttribute("href", dataURL);
        const filename = determineFilename('paletta');
        dl.setAttribute("download", filename + ".png");
        dl.click();
    }
    exports.downloadPalettePng = downloadPalettePng;
    function determineFilenamePostfix() {
        const fill = $("#chkFillFacets").prop("checked");
        return fill ? 'szines' : 'korvonal';
    }
    function determineFilename(postfix) {
        if (postfix) {
            return uploadedFilename ? uploadedFilename + '-' + postfix : postfix;
        }
        else {
            return uploadedFilename ? uploadedFilename : 'output';
        }
    }
    function downloadPNG() {
        if ($("#svgContainer svg").length > 0) {
            const filenamePostfix = determineFilenamePostfix();
            const filename = determineFilename(filenamePostfix);
            saveSvgAsPng($("#svgContainer svg").get(0), filename + ".png");
        }
    }
    exports.downloadPNG = downloadPNG;
    function downloadSVG() {
        if ($("#svgContainer svg").length > 0) {
            const filenamePostfix = determineFilenamePostfix();
            const filename = determineFilename(filenamePostfix);
            const svgEl = $("#svgContainer svg").get(0);
            svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
            const svgData = svgEl.outerHTML;
            const preface = '<?xml version="1.0" standalone="no"?>\r\n';
            const svgBlob = new Blob([preface, svgData], { type: "image/svg+xml;charset=utf-8" });
            const svgUrl = URL.createObjectURL(svgBlob);
            const downloadLink = document.createElement("a");
            downloadLink.href = svgUrl;
            downloadLink.download = filename + ".svg";
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
            /*
            var svgAsXML = (new XMLSerializer).serializeToString(<any>$("#svgContainer svg").get(0));
            let dataURL = "data:image/svg+xml," + encodeURIComponent(svgAsXML);
            var dl = document.createElement("a");
            document.body.appendChild(dl);
            dl.setAttribute("href", dataURL);
            dl.setAttribute("download", "paintbynumbers.svg");
            dl.click();
            */
        }
    }
    exports.downloadSVG = downloadSVG;
    function loadExample(imgId) {
        // load image
        const img = document.getElementById(imgId);
        const c = document.getElementById("canvas");
        const ctx = c.getContext("2d");
        c.width = img.naturalWidth;
        c.height = img.naturalHeight;
        ctx.drawImage(img, 0, 0);
        setUploadedFilename(imgId);
    }
    exports.loadExample = loadExample;
});
define("lib/clipboard", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Clipboard = void 0;
    // From https://stackoverflow.com/a/35576409/694640
    /**
     * image pasting into canvas
     *
     * @param {string} canvas_id - canvas id
     * @param {boolean} autoresize - if canvas will be resized
     */
    class Clipboard {
        constructor(canvas_id, autoresize) {
            this.ctrl_pressed = false;
            this.command_pressed = false;
            this.paste_event_support = false;
            const _self = this;
            this.canvas = document.getElementById(canvas_id);
            this.ctx = this.canvas.getContext("2d");
            this.autoresize = autoresize;
            // handlers
            // document.addEventListener("keydown", function (e) {
            //     _self.on_keyboard_action(e);
            // }, false); // firefox fix
            // document.addEventListener("keyup", function (e) {
            //     _self.on_keyboardup_action(e);
            // }, false); // firefox fix
            document.addEventListener("paste", function (e) {
                _self.paste_auto(e);
            }, false); // official paste handler
            this.init();
        }
        // constructor - we ignore security checks here
        init() {
            this.pasteCatcher = document.createElement("div");
            this.pasteCatcher.setAttribute("id", "paste_ff");
            this.pasteCatcher.setAttribute("contenteditable", "");
            this.pasteCatcher.style.cssText = "opacity:0;position:fixed;top:0px;left:0px;width:10px;margin-left:-20px;";
            document.body.appendChild(this.pasteCatcher);
            const _self = this;
            // create an observer instance
            const observer = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (_self.paste_event_support === true || _self.ctrl_pressed === false || mutation.type !== "childList") {
                        // we already got data in paste_auto()
                        return true;
                    }
                    // if paste handle failed - capture pasted object manually
                    if (mutation.addedNodes.length === 1) {
                        if (mutation.addedNodes[0].src !== undefined) {
                            // image
                            _self.paste_createImage(mutation.addedNodes[0].src);
                        }
                        // register cleanup after some time.
                        setTimeout(function () {
                            _self.pasteCatcher.innerHTML = "";
                        }, 20);
                    }
                    return false;
                });
            });
            const target = document.getElementById("paste_ff");
            const config = { attributes: true, childList: true, characterData: true };
            observer.observe(target, config);
        }
        // default paste action
        paste_auto(e) {
            this.paste_event_support = false;
            if (this.pasteCatcher !== undefined) {
                this.pasteCatcher.innerHTML = "";
            }
            if (e.clipboardData) {
                const items = e.clipboardData.items;
                if (items) {
                    this.paste_event_support = true;
                    // access data directly
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].type.indexOf("image") !== -1) {
                            // image
                            const blob = items[i].getAsFile();
                            const URLObj = window.URL || window.webkitURL;
                            const source = URLObj.createObjectURL(blob);
                            this.paste_createImage(source);
                            e.preventDefault();
                            return false;
                        }
                    }
                }
                else {
                    // wait for DOMSubtreeModified event
                    // https://bugzilla.mozilla.org/show_bug.cgi?id=891247
                }
            }
            return true;
        }
        // on keyboard press
        on_keyboard_action(event) {
            const k = event.keyCode;
            // ctrl
            if (k === 17 || event.metaKey || event.ctrlKey) {
                if (this.ctrl_pressed === false) {
                    this.ctrl_pressed = true;
                }
            }
            // v
            if (k === 86) {
                if (document.activeElement !== undefined && document.activeElement.type === "text") {
                    // let user paste into some input
                    return false;
                }
                if (this.ctrl_pressed === true && this.pasteCatcher !== undefined) {
                    this.pasteCatcher.focus();
                }
            }
            return true;
        }
        // on keyboard release
        on_keyboardup_action(event) {
            // ctrl
            if (event.ctrlKey === false && this.ctrl_pressed === true) {
                this.ctrl_pressed = false;
            }
            else if (event.metaKey === false && this.command_pressed === true) {
                this.command_pressed = false;
                this.ctrl_pressed = false;
            }
        }
        // draw pasted image to canvas
        paste_createImage(source) {
            const pastedImage = new Image();
            const self = this;
            pastedImage.onload = function () {
                if (self.autoresize === true) {
                    // resize
                    self.canvas.width = pastedImage.width;
                    self.canvas.height = pastedImage.height;
                }
                else {
                    // clear canvas
                    self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height);
                }
                self.ctx.drawImage(pastedImage, 0, 0);
            };
            pastedImage.src = source;
        }
    }
    exports.Clipboard = Clipboard;
});
define("main", ["require", "exports", "gui", "lib/clipboard"], function (require, exports, gui_2, clipboard_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    $(document).ready(function () {
        $(".tabs").tabs();
        $(".tooltipped").tooltip();
        const clip = new clipboard_1.Clipboard("canvas", true);
        $("#file").change(function (ev) {
            const files = $("#file").get(0).files;
            if (files !== null && files.length > 0) {
                const reader = new FileReader();
                reader.onloadend = function () {
                    const img = document.createElement("img");
                    img.onload = () => {
                        const c = document.getElementById("canvas");
                        const ctx = c.getContext("2d");
                        c.width = img.naturalWidth;
                        c.height = img.naturalHeight;
                        ctx.drawImage(img, 0, 0);
                        const filenameArray = files[0].name.split('.');
                        filenameArray.pop();
                        const filename = filenameArray.join('.');
                        (0, gui_2.setUploadedFilename)(filename);
                    };
                    img.onerror = () => {
                        alert("Unable to load image");
                    };
                    img.src = reader.result;
                };
                reader.readAsDataURL(files[0]);
            }
        });
        (0, gui_2.loadExample)("imgSmall");
        $("#btnProcess").click(function () {
            return __awaiter(this, void 0, void 0, function* () {
                try {
                    yield (0, gui_2.process)();
                    console.log('process done');
                }
                catch (err) {
                    alert("Error: " + err);
                }
            });
        });
        $("#chkShowLabels, #chkFillFacets, #chkShowBorders, #txtSizeMultiplier, #txtLabelFontSize, #txtLabelFontColor").change(() => __awaiter(this, void 0, void 0, function* () {
            yield (0, gui_2.updateOutput)();
        }));
        $("#btnDownloadSVG").click(function () {
            (0, gui_2.downloadSVG)();
        });
        $("#btnDownloadPNG").click(function () {
            (0, gui_2.downloadPNG)();
        });
        $("#btnDownloadPalettePNG").click(function () {
            (0, gui_2.downloadPalettePng)();
        });
        $("#lnkTrivial").click(() => { (0, gui_2.loadExample)("imgTrivial"); return false; });
        $("#lnkSmall").click(() => { (0, gui_2.loadExample)("imgSmall"); return false; });
        $("#lnkMedium").click(() => { (0, gui_2.loadExample)("imgMedium"); return false; });
    });
});
